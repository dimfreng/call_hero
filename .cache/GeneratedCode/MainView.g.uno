public partial class AppName: Fuse.Controls.Text
{
    static AppName()
    {
    }
    public AppName()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        this.FontSize = 35f;
        this.Font = global::MainView.seasrn;
    }
}
public partial class MainView: Fuse.App
{
    public sealed class Fuse_Controls_PageControl_Fuse_Node_Active_Property: Uno.UX.Property<Fuse.Node>
    {
        Fuse.Controls.PageControl _obj;
        public Fuse_Controls_PageControl_Fuse_Node_Active_Property(Fuse.Controls.PageControl obj) { _obj = obj; }
        protected override Fuse.Node OnGet() { return _obj.Active; }
        protected override void OnSet(Fuse.Node v, object origin) { _obj.Active = v; }
    }
    public sealed class Fuse_Reactive_Each_object_Items_Property: Uno.UX.Property<object>
    {
        Fuse.Reactive.Each _obj;
        public Fuse_Reactive_Each_object_Items_Property(Fuse.Reactive.Each obj) { _obj = obj; }
        protected override object OnGet() { return _obj.Items; }
        protected override void OnSet(object v, object origin) { _obj.Items = v; }
    }
    public sealed class Fuse_Controls_TextControl_string_Value_Property: Uno.UX.Property<string>
    {
        Fuse.Controls.TextControl _obj;
        public Fuse_Controls_TextControl_string_Value_Property(Fuse.Controls.TextControl obj) { _obj = obj; }
        protected override string OnGet() { return _obj.Value; }
        protected override void OnSet(string v, object origin) { _obj.SetValue(v, origin); }
        protected override void OnAddListener(Uno.UX.ValueChangedHandler<string> listener) { _obj.ValueChanged += listener; }
        protected override void OnRemoveListener(Uno.UX.ValueChangedHandler<string> listener) { _obj.ValueChanged -= listener; }
    }
    public partial class Factory: Uno.UX.IFactory
    {
        internal readonly MainView __parent;
        public Factory(MainView parent)
        {
            __parent = parent;
        }
        MainView.Fuse_Controls_TextControl_string_Value_Property temp_Value_inst;
        internal Fuse.Reactive.EventBinding temp_eb0;
        static Factory()
        {
        }
        public object New()
        {
            var self = new Fuse.Controls.Rectangle();
            var temp = new Fuse.Controls.Text();
            temp_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp);
            var temp1 = new Fuse.Reactive.DataBinding<string>(temp_Value_inst, "name");
            var temp2 = new Fuse.Drawing.StaticSolidColor(float4(0f, 0.4f, 0.6352941f, 1f));
            var temp_eb0 = new Fuse.Reactive.EventBinding("selectedCategory");
            self.CornerRadius = float4(5f, 5f, 5f, 5f);
            self.Width = 230f;
            self.Height = 55f;
            self.Margin = float4(0f, 0f, 0f, 10f);
            global::Fuse.Gestures.Clicked.AddHandler(self, temp_eb0.OnEvent);
            temp.FontSize = 20f;
            temp.TextColor = float4(1f, 1f, 1f, 1f);
            temp.Alignment = Fuse.Elements.Alignment.Center;
            temp.Behaviors.Add(temp1);
            self.Fill = temp2;
            self.Children.Add(temp);
            self.Behaviors.Add(temp_eb0);
            return self;
        }
    }
    public partial class Factory1: Uno.UX.IFactory
    {
        internal readonly MainView __parent;
        public Factory1(MainView parent)
        {
            __parent = parent;
        }
        MainView.Fuse_Controls_TextControl_string_Value_Property temp_Value_inst;
        MainView.Fuse_Controls_TextControl_string_Value_Property temp1_Value_inst;
        MainView.Fuse_Controls_TextControl_string_Value_Property temp2_Value_inst;
        internal Fuse.Reactive.EventBinding temp_eb2;
        static Factory1()
        {
        }
        public object New()
        {
            var self = new Fuse.Controls.Rectangle();
            var temp = new Fuse.Controls.Text();
            temp_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp);
            var temp1 = new Fuse.Controls.Text();
            temp1_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp1);
            var temp2 = new Fuse.Controls.Text();
            temp2_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp2);
            var temp3 = new Fuse.Controls.Grid();
            var temp4 = new Fuse.Controls.StackPanel();
            var temp5 = new Fuse.Controls.ScrollView();
            var temp6 = new Fuse.Reactive.DataBinding<string>(temp_Value_inst, "name");
            var temp7 = new Fuse.Controls.ScrollView();
            var temp8 = new Fuse.Reactive.DataBinding<string>(temp1_Value_inst, "address");
            var temp9 = new Fuse.Controls.ScrollView();
            var temp10 = new Fuse.Reactive.DataBinding<string>(temp2_Value_inst, "number");
            var temp11 = new Fuse.Controls.StackPanel();
            var temp12 = new Fuse.Controls.Rectangle();
            var temp13 = new Fuse.Drawing.ImageFill();
            var temp_eb2 = new Fuse.Reactive.EventBinding("callNumber");
            var temp14 = new Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
            self.Width = 300f;
            self.Height = 80f;
            self.Margin = float4(0f, 0f, 0f, 10f);
            self.Opacity = 0.7f;
            temp3.RowCount = 1;
            temp3.ColumnCount = 1;
            temp3.Children.Add(temp4);
            temp3.Children.Add(temp11);
            temp4.Width = 180f;
            temp4.Margin = float4(50f, 5f, -5f, 7f);
            global::Fuse.Controls.Grid.SetRow(temp4, 0);
            global::Fuse.Controls.Grid.SetColumn(temp4, 0);
            temp4.Children.Add(temp5);
            temp4.Children.Add(temp7);
            temp4.Children.Add(temp9);
            temp5.AllowedScrollDirections = Fuse.Gestures.ScrollDirections.Horizontal;
            temp5.Content = temp;
            temp.FontSize = 20f;
            temp.Behaviors.Add(temp6);
            temp7.AllowedScrollDirections = Fuse.Gestures.ScrollDirections.Horizontal;
            temp7.Content = temp1;
            temp1.FontSize = 18f;
            temp1.TextColor = float4(0.4666667f, 0.4666667f, 0.4666667f, 1f);
            temp1.Behaviors.Add(temp8);
            temp9.AllowedScrollDirections = Fuse.Gestures.ScrollDirections.Horizontal;
            temp9.Content = temp2;
            temp2.FontSize = 18f;
            temp2.TextColor = float4(0f, 0f, 0f, 1f);
            temp2.Behaviors.Add(temp10);
            temp11.Width = 10f;
            global::Fuse.Controls.Grid.SetRow(temp11, 0);
            global::Fuse.Controls.Grid.SetColumn(temp11, 1);
            temp11.Children.Add(temp12);
            temp12.Width = 50f;
            temp12.Height = 50f;
            temp12.Alignment = Fuse.Elements.Alignment.Center;
            temp12.Margin = float4(30f, 15f, 0f, 0f);
            global::Fuse.Gestures.Clicked.AddHandler(temp12, temp_eb2.OnEvent);
            temp12.Fills.Add(temp13);
            temp12.Behaviors.Add(temp_eb2);
            temp13.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../Assets/Phone-icon.png"));
            self.Fill = temp14;
            self.Children.Add(temp3);
            return self;
        }
    }
    MainView.Fuse_Reactive_Each_object_Items_Property temp_Items_inst;
    MainView.Fuse_Controls_TextControl_string_Value_Property temp1_Value_inst;
    MainView.Fuse_Reactive_Each_object_Items_Property temp2_Items_inst;
    MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property temp3_Active_inst;
    [global::Uno.UX.UXGlobalResource("seasrn")] public static readonly Fuse.Font seasrn;
    internal Fuse.Controls.Page Page1;
    internal Fuse.Controls.Grid loggedOutView;
    internal Fuse.Controls.Page Page2;
    internal Fuse.Reactive.EventBinding temp_eb1;
    static MainView()
    {
        seasrn = new Fuse.Font(new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../Assets/fonts/SEASRN__.ttf")));
        global::Uno.UX.Resource.SetGlobalKey(seasrn, "seasrn");
    }
    public MainView()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        var temp = new Fuse.Reactive.Each();
        temp_Items_inst = new MainView.Fuse_Reactive_Each_object_Items_Property(temp);
        var temp1 = new Fuse.Controls.Text();
        temp1_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp1);
        var temp2 = new Fuse.Reactive.Each();
        temp2_Items_inst = new MainView.Fuse_Reactive_Each_object_Items_Property(temp2);
        var temp3 = new Fuse.Controls.PageControl();
        temp3_Active_inst = new MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property(temp3);
        var temp4 = new Fuse.Reactive.JavaScript();
        Page1 = new Fuse.Controls.Page();
        loggedOutView = new Fuse.Controls.Grid();
        var temp5 = new Fuse.Controls.Rectangle();
        var temp6 = new Fuse.Drawing.ImageFill();
        var temp7 = new Fuse.Drawing.ImageFill();
        var temp8 = new Fuse.Controls.Grid();
        var temp9 = new AppName();
        var temp10 = new Fuse.Controls.Circle();
        var temp11 = new Fuse.Drawing.ImageFill();
        var temp12 = new AppName();
        var temp13 = new Fuse.Controls.Panel();
        var temp14 = new Fuse.Controls.Text();
        var temp15 = new Fuse.Controls.Rectangle();
        var temp16 = new Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        var temp17 = new Fuse.Controls.ScrollView();
        var temp18 = new Fuse.Controls.StackPanel();
        var temp19 = new Factory(this);
        var temp20 = new Fuse.Reactive.DataBinding<object>(temp_Items_inst, "categories");
        Page2 = new Fuse.Controls.Page();
        var temp21 = new Fuse.Controls.Grid();
        var temp22 = new Fuse.Controls.Rectangle();
        var temp23 = new Fuse.Drawing.ImageFill();
        var temp24 = new Fuse.Drawing.ImageFill();
        var temp25 = new Fuse.Controls.Panel();
        var temp26 = new Fuse.Controls.Rectangle();
        var temp27 = new Fuse.Drawing.StaticSolidColor(float4(0f, 0f, 0f, 1f));
        var temp28 = new Fuse.Controls.Rectangle();
        var temp29 = new Fuse.Controls.Image();
        temp_eb1 = new Fuse.Reactive.EventBinding("backButton");
        var temp30 = new Fuse.Reactive.DataBinding<string>(temp1_Value_inst, "selected");
        var temp31 = new Fuse.Controls.ScrollView();
        var temp32 = new Fuse.Controls.StackPanel();
        var temp33 = new Factory1(this);
        var temp34 = new Fuse.Reactive.DataBinding<object>(temp2_Items_inst, "contacts");
        var temp35 = new Fuse.Reactive.DataBinding<Fuse.Node>(temp3_Active_inst, "currentPage");
        temp4.LineNumber = 4;
        temp4.FileName = "MainView.ux";
        temp4.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../MainView.js"));
        temp3.Children.Add(Page1);
        temp3.Children.Add(Page2);
        temp3.Behaviors.Add(temp35);
        Page1.Name = "Page1";
        Page1.Children.Add(loggedOutView);
        loggedOutView.RowData = "1*,0.5*,2.5*";
        loggedOutView.Name = "loggedOutView";
        loggedOutView.Children.Add(temp5);
        loggedOutView.Children.Add(temp8);
        loggedOutView.Children.Add(temp13);
        loggedOutView.Children.Add(temp17);
        temp5.Layer = Fuse.Layer.Background;
        temp5.Fills.Add(temp6);
        temp5.Fills.Add(temp7);
        temp6.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../Assets/phone.jpg"));
        temp7.Opacity = 0.2f;
        temp7.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../Assets/overlay.jpg"));
        temp8.RowCount = 1;
        temp8.ColumnCount = 3;
        temp8.Margin = float4(0f, 20f, 0f, 0f);
        temp8.Padding = float4(10f, 0f, 10f, 0f);
        global::Fuse.Controls.Grid.SetRow(temp8, 0);
        temp8.Children.Add(temp9);
        temp8.Children.Add(temp10);
        temp8.Children.Add(temp12);
        temp9.Value = "Call";
        temp9.TextColor = float4(1f, 1f, 1f, 1f);
        temp9.Margin = float4(15f, 45f, 0f, 0f);
        global::Fuse.Controls.Grid.SetColumn(temp9, 0);
        temp10.Width = 75f;
        temp10.Height = 75f;
        global::Fuse.Controls.Grid.SetColumn(temp10, 1);
        temp10.Fills.Add(temp11);
        temp11.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../Assets/CH-Icon.jpg"));
        temp12.Value = "Hero";
        temp12.TextColor = float4(1f, 1f, 1f, 1f);
        temp12.Margin = float4(5f, 45f, 0f, 0f);
        global::Fuse.Controls.Grid.SetColumn(temp12, 2);
        temp13.Margin = float4(0f, 0f, 0f, 15f);
        temp13.Children.Add(temp14);
        temp13.Children.Add(temp15);
        temp14.Value = "Available Categories";
        temp14.FontSize = 30f;
        temp14.Alignment = Fuse.Elements.Alignment.Center;
        global::Fuse.Controls.Grid.SetRow(temp14, 1);
        temp15.Opacity = 0.5f;
        temp15.Layer = Fuse.Layer.Background;
        temp15.Fill = temp16;
        temp17.Margin = float4(0f, 0f, 0f, 10f);
        global::Fuse.Controls.Grid.SetRow(temp17, 2);
        temp17.Content = temp18;
        temp18.Behaviors.Add(temp20);
        temp18.Behaviors.Add(temp);
        temp.Factories.Add(temp19);
        Page2.Name = "Page2";
        Page2.Children.Add(temp21);
        temp21.RowData = "0.4*,2.5*";
        temp21.Children.Add(temp22);
        temp21.Children.Add(temp25);
        temp21.Children.Add(temp31);
        temp22.Layer = Fuse.Layer.Background;
        temp22.Fills.Add(temp23);
        temp22.Fills.Add(temp24);
        temp23.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../Assets/background.jpg"));
        temp24.Opacity = 0.4f;
        temp24.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../Assets/overlay.jpg"));
        global::Fuse.Controls.Grid.SetRow(temp25, 0);
        temp25.Children.Add(temp26);
        temp25.Children.Add(temp28);
        temp25.Children.Add(temp1);
        temp26.Opacity = 0.5f;
        temp26.Layer = Fuse.Layer.Background;
        temp26.Fill = temp27;
        temp28.Width = 25f;
        temp28.Height = 25f;
        temp28.Alignment = Fuse.Elements.Alignment.Left;
        temp28.Margin = float4(25f, 20f, 0f, 0f);
        global::Fuse.Gestures.Clicked.AddHandler(temp28, temp_eb1.OnEvent);
        temp28.Children.Add(temp29);
        temp28.Behaviors.Add(temp_eb1);
        temp29.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../Assets/arrow.png"));
        temp1.FontSize = 24f;
        temp1.TextColor = float4(1f, 1f, 1f, 1f);
        temp1.Alignment = Fuse.Elements.Alignment.Center;
        temp1.Margin = float4(0f, 20f, 0f, 0f);
        temp1.Behaviors.Add(temp30);
        temp31.Margin = float4(0f, 10f, 0f, 10f);
        global::Fuse.Controls.Grid.SetRow(temp31, 1);
        temp31.Content = temp32;
        temp32.Behaviors.Add(temp34);
        temp32.Behaviors.Add(temp2);
        temp2.Factories.Add(temp33);
        this.RootNode = temp3;
        this.Theme = Fuse.BasicTheme.BasicTheme.Singleton;
        this.Behaviors.Add(temp4);
    }
}
