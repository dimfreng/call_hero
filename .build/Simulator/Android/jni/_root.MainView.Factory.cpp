// This file was generated based on 'C:\Users\Kevin\Desktop\callhero\CallHero\.build\Simulator\Android\Cache\GeneratedCode\MainView.g.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.MainView.Factory.h>
#include <_root.MainView.Fuse_Controls_TextControl_string_Value_Property.h>
#include <_root.MainView.h>
#include <Fuse.Behavior.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.Controls.Rectangle.h>
#include <Fuse.Controls.Shape.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Drawing.Brush.h>
#include <Fuse.Drawing.StaticSolidColor.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Gestures.Clicked.h>
#include <Fuse.Gestures.ClickedHandler.h>
#include <Fuse.Node.h>
#include <Fuse.Reactive.DataBinding-1.h>
#include <Fuse.Reactive.EventBinding.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Float.h>
#include <Uno.Float4.h>
#include <Uno.String.h>
#include <Uno.UX.Property-1.h>
static uString* STRINGS[2];
static uType* TYPES[12];

namespace g{

// public partial sealed class MainView.Factory :41
// {
// static Factory() :50
static void MainView__Factory__cctor__fn(uType* __type)
{
}

MainView__Factory_type* MainView__Factory_typeof()
{
    static uSStrong<MainView__Factory_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.FieldCount = 2;
    options.InterfaceCount = 1;
    options.ObjectSize = sizeof(MainView__Factory);
    options.TypeSize = sizeof(MainView__Factory_type);
    type = (MainView__Factory_type*)uClassType::New("MainView.Factory", options);
    type->fp_cctor_ = MainView__Factory__cctor__fn;
    type->interface0.fp_New1 = (void(*)(uObject*, uObject**))MainView__Factory__New1_fn;
    ::STRINGS[0] = uString::Const("name");
    ::STRINGS[1] = uString::Const("selectedCategory");
    ::TYPES[0] = ::g::Fuse::Controls::TextControl_typeof();
    ::TYPES[1] = ::g::Fuse::Reactive::DataBinding_typeof()->MakeType(::g::Uno::String_typeof());
    ::TYPES[2] = ::g::Uno::UX::Property_typeof()->MakeType(::g::Uno::String_typeof());
    ::TYPES[3] = ::g::Fuse::Controls::Rectangle_typeof();
    ::TYPES[4] = ::g::Fuse::Elements::Element_typeof();
    ::TYPES[5] = ::g::Fuse::Node_typeof();
    ::TYPES[6] = ::g::Fuse::Gestures::ClickedHandler_typeof();
    ::TYPES[7] = ::g::Fuse::Behavior_typeof();
    ::TYPES[8] = ::g::Fuse::Controls::Shape_typeof();
    ::TYPES[9] = ::g::Fuse::Drawing::Brush_typeof();
    ::TYPES[10] = ::g::Fuse::Controls::Panel_typeof();
    ::TYPES[11] = uObject_typeof();
    type->SetInterfaces(
        ::g::Uno::UX::IFactory_typeof(), offsetof(MainView__Factory_type, interface0));
    type->SetFields(0,
        ::g::MainView_typeof(), offsetof(::g::MainView__Factory, __parent1), 0,
        ::g::MainView__Fuse_Controls_TextControl_string_Value_Property_typeof(), offsetof(::g::MainView__Factory, temp_Value_inst), 0);
    type->Reflection.SetFunctions(2,
        new uFunction("New", NULL, (void*)MainView__Factory__New1_fn, 0, false, uObject_typeof(), 0),
        new uFunction(".ctor", NULL, (void*)MainView__Factory__New2_fn, 0, true, MainView__Factory_typeof(), 1, ::g::MainView_typeof()));
    return type;
}

// public Factory(MainView parent) :44
void MainView__Factory__ctor__fn(MainView__Factory* __this, ::g::MainView* parent)
{
    __this->ctor_(parent);
}

// public object New() :53
void MainView__Factory__New1_fn(MainView__Factory* __this, uObject** __retval)
{
    *__retval = __this->New1();
}

// public Factory New(MainView parent) :44
void MainView__Factory__New2_fn(::g::MainView* parent, MainView__Factory** __retval)
{
    *__retval = MainView__Factory::New2(parent);
}

// public Factory(MainView parent) [instance] :44
void MainView__Factory::ctor_(::g::MainView* parent)
{
    __parent1 = parent;
}

// public object New() [instance] :53
uObject* MainView__Factory::New1()
{
    ::g::Fuse::Controls::Rectangle* self = ::g::Fuse::Controls::Rectangle::New2();
    ::g::Fuse::Controls::Text* temp = ::g::Fuse::Controls::Text::New2();
    temp_Value_inst = ::g::MainView__Fuse_Controls_TextControl_string_Value_Property::New1(temp);
    ::g::Fuse::Reactive::DataBinding* temp1 = (::g::Fuse::Reactive::DataBinding*)::g::Fuse::Reactive::DataBinding::New1(::TYPES[1/*Fuse.Reactive.DataBinding<string>*/], temp_Value_inst, ::STRINGS[0/*"name"*/]);
    ::g::Fuse::Drawing::StaticSolidColor* temp2 = ::g::Fuse::Drawing::StaticSolidColor::New1(::g::Uno::Float4__New2(0.0f, 0.4f, 0.6352941f, 1.0f));
    ::g::Fuse::Reactive::EventBinding* temp_eb01 = ::g::Fuse::Reactive::EventBinding::New1(::STRINGS[1/*"selectedCat...*/]);
    self->CornerRadius(::g::Uno::Float4__New2(5.0f, 5.0f, 5.0f, 5.0f));
    self->Width(230.0f);
    self->Height(55.0f);
    self->Margin(::g::Uno::Float4__New2(0.0f, 0.0f, 0.0f, 10.0f));
    ::g::Fuse::Gestures::Clicked::AddHandler(self, uDelegate::New(::TYPES[6/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, temp_eb01));
    temp->FontSize(20.0f);
    temp->TextColor(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    temp->Alignment(10);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Behaviors()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[7/*Fuse.Behavior*/])), temp1);
    self->Fill(temp2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(self->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[5/*Fuse.Node*/])), temp);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(self->Behaviors()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[7/*Fuse.Behavior*/])), temp_eb01);
    return self;
}

// public Factory New(MainView parent) [static] :44
MainView__Factory* MainView__Factory::New2(::g::MainView* parent)
{
    MainView__Factory* obj1 = (MainView__Factory*)uNew(MainView__Factory_typeof());
    obj1->ctor_(parent);
    return obj1;
}
// }

} // ::g
