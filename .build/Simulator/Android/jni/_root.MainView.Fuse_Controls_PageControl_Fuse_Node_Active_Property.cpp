// This file was generated based on 'C:\Users\Kevin\Desktop\callhero\CallHero\.build\Simulator\Android\Cache\GeneratedCode\MainView.g.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property.h>
#include <Fuse.Controls.PageControl.h>
#include <Uno.Object.h>
static uType* TYPES[1];

namespace g{

// public sealed class MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property :18
// {
::g::Uno::UX::Property_type* MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property_typeof()
{
    static uSStrong< ::g::Uno::UX::Property_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.FieldCount = 3;
    options.ObjectSize = sizeof(MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property);
    options.TypeSize = sizeof(::g::Uno::UX::Property_type);
    type = (::g::Uno::UX::Property_type*)uClassType::New("MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property", options);
    type->SetBase(::g::Uno::UX::Property_typeof()->MakeType(::g::Fuse::Node_typeof()));
    type->fp_OnGet = (void(*)(::g::Uno::UX::Property*, uTRef))MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property__OnGet_fn;
    type->fp_OnSet = (void(*)(::g::Uno::UX::Property*, void*, uObject*))MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property__OnSet_fn;
    ::TYPES[0] = ::g::Fuse::Controls::PageControl_typeof();
    type->SetFields(2,
        ::g::Fuse::Controls::PageControl_typeof(), offsetof(::g::MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property, _obj), 0);
    type->Reflection.SetFunctions(1,
        new uFunction(".ctor", NULL, (void*)MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property__New1_fn, 0, true, MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property_typeof(), 1, ::g::Fuse::Controls::PageControl_typeof()));
    return type;
}

// public Fuse_Controls_PageControl_Fuse_Node_Active_Property(Fuse.Controls.PageControl obj) :21
void MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property__ctor_1_fn(MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property* __this, ::g::Fuse::Controls::PageControl* obj)
{
    __this->ctor_1(obj);
}

// public Fuse_Controls_PageControl_Fuse_Node_Active_Property New(Fuse.Controls.PageControl obj) :21
void MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property__New1_fn(::g::Fuse::Controls::PageControl* obj, MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property** __retval)
{
    *__retval = MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property::New1(obj);
}

// protected override sealed Fuse.Node OnGet() :22
void MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property__OnGet_fn(MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property* __this, ::g::Fuse::Node** __retval)
{
    return *__retval = uPtr(__this->_obj)->Active(), void();
}

// protected override sealed void OnSet(Fuse.Node v, object origin) :23
void MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property__OnSet_fn(MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property* __this, ::g::Fuse::Node* v, uObject* origin)
{
    uPtr(__this->_obj)->Active(v);
}

// public Fuse_Controls_PageControl_Fuse_Node_Active_Property(Fuse.Controls.PageControl obj) [instance] :21
void MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property::ctor_1(::g::Fuse::Controls::PageControl* obj)
{
    ctor_();
    _obj = obj;
}

// public Fuse_Controls_PageControl_Fuse_Node_Active_Property New(Fuse.Controls.PageControl obj) [static] :21
MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property* MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property::New1(::g::Fuse::Controls::PageControl* obj)
{
    MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property* obj1 = (MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property*)uNew(MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property_typeof());
    obj1->ctor_1(obj);
    return obj1;
}
// }

} // ::g
