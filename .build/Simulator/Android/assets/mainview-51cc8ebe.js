  var Observable = require("FuseJS/Observable");
  var phone = require('FuseJS/Phone');
      var currentPage = Observable("Page1");
      var contacts = Observable();
      var selected = Observable();
      var categories = [
      {name:'FastFoods',
  		contacts:[
  		{name:"Calda Pizza",address:"Lapasan",number:"09233012555"},
  		{name:"Jollibee Express Delivery",address:"IFI Bulua",number:"8555595"},
      {name:"Jollibee Express Delivery",address:"Limketkai Gateway",number:"8563000"},
      {name:"Jollibee Express Delivery",address:"Puerto",number:"8552824"},
      {name:"Jollibee Express Delivery",address:"Vamenta",number:"8584747"},
      {name:"Jollibee Express Delivery",address:"Xavier",number:"727786"},
      {name:"McDonald’s",address:"Limketkai",number:"8563693"},
      {name:"McDonald’s",address:"Ororama Supercenter",number:"271423"},
      {name:"More Bites Pizza",address:"Tiano cor. del Pilar Sts",number:"09183313001"},
      {name:"Pizza de Oro",address:"",number:"09154430588"},
      {name:"Pizza Hut ",address:"SM City ",number:"9111111"},
      {name:"Pizza Uno",address:"RER Ph. 1, Kauswagan Highway",number:"8801943"},
      {name:"Shakey’s Pizza",address:"Limketkai Mall",number:"8567054"},
      {name:"Sy Bee Tin",address:"Tomas Saco St.",number:"09154652458"},
      {name:"Yellow Cab Pizza",address:"Limketkai [24-hr]",number:"8559222"},
      {name:"Yellow Cab Pizza",address:"SM City",number:"8592020"},
      {name:"Loraine’s Portico",address:"Masterson Ave.",number:"09173223899"},
      {name:"More Bites Pizza",address:"Tiano cor. del Pilar Sts.",number:"09183313001"},
      {name:"More Bites Pizza",address:"Tiano cor. del Pilar Sts.",number:"09994236898"},
      {name:"Greenwich",address:"Gaisano Mall",number:"8545555"},
      {name:"Greenwich",address:"Limketkai Center",number:"8542222"},
      {name:"Greenwich",address:"G/F, SM City Cagayan de Oro",number:"8592900"},
      {name:"Alberto's Pizza",address:"Mercado Compound Rd, Cagayan de Oro, Misamis ",number:"8501940"},
      {name:"Alberto's Pizza",address:"Toribio Chavez Street, Cagayan de Oro, 9000 Misamis ",number:"8511769"},
  		{name:"Chowking ",address:"GUSA",number:"8551141"},
      {name:"Chowking ",address:"LIMKETKAI",number:"8541111"},
      {name:"KFC",address:"Entrance 2, G/F Limketkai Mall, Cagayan de Oro, ",number:"8561366"},
      {name:"Alberto's Pizza",address:"Marfori Compound (at Pabayo St.), 9000 Cagayan de Oro",number:"+639237351551"},
      {name:"Alberto's Pizza",address:"Marfori Compound (at Pabayo St.), 9000 Cagayan de Oro",number:"+639158819311"},
      {name:"KFC",address:"G/F SM City Cagayan de Oro",number:"8691142"}
  		]},
      {name:'Resort Hotels',
  		contacts:[
  		{name:"Chali Beach Resort",address:"Old Cugman Road, Cugman",number:"8555941"},
  		{name:"Coco Bay Resort",address:"National Highway, Baloy",number:"8552702"},
      {name:"Country Village Hotel",address:"Villarin Street, Carmen",number:"8583004"},
      {name:"Gardens of Malasag Eco Tourism Village",address:"Malasag Hill",number:"09209826759"},
      {name:"Harbor Lights ",address:"Old Gusa Road, Gusa",number:"8556060"},
      {name:"Marco Hotel",address:"Alwana Business Park, Cugman",number:"8552201"},
      {name:"N Hotel",address:"National Highway, Kauswagan",number:"8801924"},
      {name:"Ridge View Chalets",address:"Xavier Estates and Country Club",number:"8587946"},
      {name:"Apple Tree Resort and Hotel",address:"Opol, Misamis Oriental",number:"735563"}
  		]},
      {name:'Hotels',
      contacts:[
      {name:"Koresco Hotel",address:"Pueblo de Oro Golf Estate, Lumbia",number:"8592299"},
      {name:"Pryce Plaza Hotel",address:"Carmen Hill",number:"8584536"},
      {name:"Seda Centrio",address:"CM Recto corner Corrales Avenue",number:"3238888"},
      {name:"BudgeTel",address:"Corrales Extension",number:"726643"},
      {name:"Chananthon Bed and Breakfast",address:"CM Recto Avenue",number:"8568189"},
      {name:"Discovery Hotel",address:"Florentino Street",number:"8563896"},
      {name:"Golden Pension House",address:"Golden Glow North, Pedro Oloy Roa Avenue",number:"8591259"},
      {name:"GV Hotel",address:"Corrales Extension",number:"3231547"},
      {name:"Mallberry Suites Business Hotel",address:"Florentino Street",number:"8549999"},
      {name:"Metro Hotel",address:"Capt. Roa Extension",number:"852 1417"},
      {name:"New Estrella Townhouse",address:"J. Pacana Street ",number:"8521441"},
      {name:"Pearlmont Inn",address:"Limketkai Drive",number:"8562653"},
      {name:"Prawn House Restaurant and Suites",address:"Regatta Square, Pueblo de Oro",number:"8519014"},
      {name:"South Winds Hotel",address:"Capt. Roa Extension",number:"727623"},
      {name:"Stonestown Suites",address:"Fr. Masterson Street, Upper Carmen",number:"8583854"},
      {name:"Travelers Pod",address:"Ground Floor, Gateway Tower 1",number:"8518988"},
      {name:"Amarea Travel Lodge",address:"Aguinaldo Street",number:"+639064670330"},
      {name:"CDO Tourist Inn",address:"Tiano Bros. Street",number:"+639353629638"},
      {name:"Cagayan Park View Hotel",address:"Tirso Neri Street",number:"8571197"},
      {name:"D' Morvie Suites  Yacapin Branch",address:"Yacapin Street",number:"8569080"},
      {name:"D' Morvie Suites  Lapasan Branch",address:"Lapasan Highway",number:"856 7197"},
      {name:"D' Morvie Suites  Capistrano Branch",address:"Yacapin and Capistrano Streets",number:"852 2015"},
      {name:"Executive Pension",address:"Mabini Street",number:"8564360"},
      {name:"G Suites",address:"Yacapin Street",number:"+639267621876"},
      {name:"Hotel Ramon",address:"Burgos corner T. Neri Streets",number:"8574804"},
      {name:"Kyross Inn",address:"Mabini Street",number:"728832"},
      {name:"MASS SPECC Hostel",address:"Tiano Bros. Street",number:"728145"},
      {name:"Rich Manor Pension House",address:"Corrales corner Ebarle Streets",number:"+639274669846"},
      {name:"Vines Pension",address:"Aguinaldo corner Yacapin Streets",number:"8572158"},
      {name:"Wills Place",address:"A. Velez Street",number:"+639176320650"},
      {name:"Win Min Transient Inn",address:"Market City (formerly Agora), Lapasan",number:"8568043"},
      {name:"YMCA",address:"J. Pacana Street",number:"8569624"}
      ]},
      {name:'Hospitals',
  		contacts:[
  		{name:"CAGAYAN DE ORO MATERNITY CHILDREN'S HOSPITAL AND PUERICULTURE CENTER",address:"Rizal-Gaerlan Streets, Cagaya de Oro City",number:"8586772"},
      {name:"CAGAYAN DE ORO MEDICAL CENTER",address:"Nacabalan-Tiano Streets, Cagayan de Oro City",number:"722256"},
      {name:"CAPITOL UNIVERSITY MEDICAL CENTER",address:"Gusa, Cagayan de Oro City",number:"723215"},
      {name:"COMMUNITY HEALTH CARE CENTER GERMAN'S DOCTOR HOSPITAL",address:"Mortola-Hayes Streets, Cagayan de Oro City",number:"725045"},
      {name:"DOCTOR'S SABAL HOSPITAL INC.",address:"A. Velez Street, Cagayan de Oro City",number:"723179"},
      {name:"J.R. BORJA MEMORIAL GENERAL HOSPITAL",address:"Carmen, Cagayan de Oro City",number:"723528"},
      {name:"MADONNA AND CHILD HOSPITAL",address:"J.V. Seriña St., Carmen Cagayan de Oro City",number:"8584105"},
      {name:"MARIA REYNA-XAVIER UNIVERSITY HOSPITAL, INC.",address:"Camama-anan, Cagayan de Oro City",number:"8571767"},
      {name:"NORTHERN MINDANAO MEDICAL CENTER",address:"Capitol Compound, Cagayan de Oro City",number:"8564147"},
      {name:"POLYMEDIC MEDICAL PLAZA",address:"National Highway, Kauswagan, Cagayan de Oro City",number:"8585858"},
      {name:"POLYMEDIC GENERAL HOSPITAL",address:"Apolinar Street, Cagayan de Oro City",number:"8564185"},
      {name:"PUERTO COMMUNITY HOSPITAL",address:"Puerto, Cagayan De Oro City",number:"8555088"}
  		]},
      {name:'Restaurants',
      contacts:[
      {name:"Adolfo’s Ribs & Jazz ",address:" Country Village Hotel",number:"8583004"},
      {name:"Ambrosia Restaurant",address:" Hotel Koresco",number:"8592299"},
      {name:"Anabel’s Restaurant",address:"Xavier Estates Country Club",number:"8587946"},
      {name:"Bigby’s Café",address:"Rosario Strip, Limketkai Center",number:"8575511"},
      {name:"Bigby’s Café",address:"SM City",number:"8591358"},
      {name:"Barkadahan Grill",address:"Divisoria",number:"8568484"},
      {name:"Mexitalian Cuisine ",address:"Xavier’s Square, Masterson Ave.",number:"09064915390"},
      {name:"Mexitalian Cuisine",address:"Xavier’s Square, Masterson Ave.",number:"09291768444"},
      {name:"Panagatan",address:"Opol, Misamis Oriental",number:"09228586071"},
      {name:"Prawn House Restaurant",address:"Regatta Square, Pueblo de Oro",number:"3095992"},
      {name:"Chick ‘n Bowl",address:"Patag Road, Carmen (beside SSS)",number:"09173034399"},
      {name:"Chick ‘n Bowl",address:"Patag Road, Carmen (beside SSS)",number:"09228766841"},
      {name:"Cucina de Oro/Chicken Ati-atihan",address:"Limketkai Drive",number:"8569168"}
      ]},
      {name:'Police Stations',
  		contacts:[
  		{name:"Police Regional Office 10",address:"Camp 1Lt Vicente G. Alagar",number:"8563183"},
  		{name:"Police Office Station 1",address:"Cogon Market, Cogon, Cagayan De Oro City",number:"8573178"},
      {name:"Divisoria Police Precint",address:"Burgos St",number:"8573173"},
      {name:"Macasandig Police Precinct",address:"Jupiter St",number:"3105579"},
      {name:"Cagayan de Oro City Police Office Station 7",address:"Bulua, Cagayan de Oro City, Misamis Oriental",number:"8583093"},
      {name:"Cugman Police Precinct",address:"Claro M. Recto Ave",number:"733222"},
      {name:"Bulua Police Precinct",address:"Iligan-Cagayan de Oro-Butuan Rd",number:"3105577"},
      {name:"Lumbia Police Precinct",address:"",number:"3105578"},
      {name:"Police Precinct 1",address:"Amphitheater, Divisoria",number:"09068568257"},
      {name:"Police Precinct 2",address:"Cogon Market ",number:"09177233129"},
      {name:"Police Precinct 3",address:"IBT Terminal, Agora",number:"09069329955"},
      {name:"Police Precinct 4",address:"Carmen Market",number:"09474435645"},
      {name:"Police Precinct 5",address:"Julio Pacana St., Puntod",number:"09278844074"},
      {name:"Police Precinct 6",address:"Puerto",number:"09426433621"},
      {name:"Police Precinct 7",address:"Bulua",number:"09482782864"},
      {name:"Police Precinct 8",address:"Lumbia",number:"09264571575"},
      {name:"Police Precinct 9",address:"Macasandig",number:"09167410690"},
      {name:"Police Precinct 10",address:"Cugman",number:"09168321350"}
  		]},
      {name:'Fire Stations',
  		contacts:[
  		{name:"Office of the District Fire Marshal",address:"Capt V. Roa St., Cagayan de Oro City ",number:"725827"},
  		{name:"Cagayan de Oro City Fire District",address:"Capt V. Roa St., Cagayan de Oro City",number:"727280"},
      {name:"Bulua Fire Station",address:"Bulua, Cagayan de Oro City",number:"738943"},
      {name:"Carmen Fire Station",address:"Carmen, Cagayan de Oro City",number:"720195"},
      {name:"Kauswagan Fire Station",address:"Kauswagan, Cagayan de Oro City",number:"3508005"},
      {name:"Lapasan Fire Station",address:"Lapasan, Cagayan de Oro City",number:"8565466"},
      {name:"Macabalan Fire Station",address:"Macabalan, Cagayan de Oro City",number:"8560164"},
      {name:"Macasandig Fire Sub-Station",address:"Macasandig, Cagayan de Oro City",number:"3091945"},
      {name:"Puerto Fire Station",address:"Puerto, Cagayan de Oro City",number:"740111"},
      {name:"Bugo Fire Su-bStation",address:"Bugo, Cagayan de Oro City",number:"3093227"},
      {name:"Tablon Fire Sub-Station",address:"Tablon, Cagayan de Oro City",number:"8551464"}
  		]}
      ];

      function selectedCategory(args) {
          currentPage.value = "Page2";
      
          contacts.clear();
          args.data.contacts.sort(compare).forEach(function(entry) {
            contacts.add(entry);
          }, this);
          selected.value = args.data.name;
		     
      }

      function compare(a,b) {
      if (a.name < b.name)
        return -1;
      else if (a.name > b.name)
        return 1;
      else 
        return 0;
    }

      function backButton(){
        currentPage.value = "Page1";
        contacts.clear();
      }

      function callNumber(args){
      	phone.call(args.data.number);  
      }

      module.exports = {
          selectedCategory: selectedCategory,
          callNumber: callNumber,
          currentPage: currentPage,
          categories: categories,
          contacts: contacts,
          backButton: backButton,
          selected: selected
      };