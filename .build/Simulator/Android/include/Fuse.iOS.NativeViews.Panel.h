// This file was generated based on 'C:\ProgramData\Uno\Packages\Fuse.iOS\0.19.3\NativeViews\$.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Controls.Panel.h>
#include <Fuse.iOS.NativeViews.ParentNativeView-1.h>
namespace g{namespace Fuse{namespace iOS{namespace NativeViews{struct Panel;}}}}

namespace g{
namespace Fuse{
namespace iOS{
namespace NativeViews{

// internal sealed extern class Panel :667
// {
::g::Fuse::Behavior_type* Panel_typeof();
void Panel__ctor_3_fn(Panel* __this);
void Panel__New1_fn(Panel** __retval);

struct Panel : ::g::Fuse::iOS::NativeViews::ParentNativeView
{
    void ctor_3();
    static Panel* New1();
};
// }

}}}} // ::g::Fuse::iOS::NativeViews
