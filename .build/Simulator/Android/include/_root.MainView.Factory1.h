// This file was generated based on 'C:\Users\Kevin\Desktop\callhero\CallHero\.build\Simulator\Android\Cache\GeneratedCode\MainView.g.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Object.h>
#include <Uno.UX.IFactory.h>
namespace g{struct MainView;}
namespace g{struct MainView__Factory1;}
namespace g{struct MainView__Fuse_Controls_TextControl_string_Value_Property;}

namespace g{

// public partial sealed class MainView.Factory1 :76
// {
struct MainView__Factory1_type : uType
{
    ::g::Uno::UX::IFactory interface0;
};

MainView__Factory1_type* MainView__Factory1_typeof();
void MainView__Factory1__ctor__fn(MainView__Factory1* __this, ::g::MainView* parent);
void MainView__Factory1__New1_fn(MainView__Factory1* __this, uObject** __retval);
void MainView__Factory1__New2_fn(::g::MainView* parent, MainView__Factory1** __retval);

struct MainView__Factory1 : uObject
{
    uStrong< ::g::MainView*> __parent1;
    uStrong< ::g::MainView__Fuse_Controls_TextControl_string_Value_Property*> temp_Value_inst;
    uStrong< ::g::MainView__Fuse_Controls_TextControl_string_Value_Property*> temp1_Value_inst;
    uStrong< ::g::MainView__Fuse_Controls_TextControl_string_Value_Property*> temp2_Value_inst;

    void ctor_(::g::MainView* parent);
    uObject* New1();
    static MainView__Factory1* New2(::g::MainView* parent);
};
// }

} // ::g
