// This file was generated based on 'C:\ProgramData\Uno\Packages\Fuse.Controls\0.19.3\.upk\meta'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.h>
namespace g{namespace Uno{namespace Runtime{namespace Implementation{namespace ShaderBackends{namespace OpenGL{struct GLProgram;}}}}}}
namespace g{struct Fuse_Controls_bundle;}

namespace g{

// public static generated class Fuse_Controls_bundle :0
// {
uClassType* Fuse_Controls_bundle_typeof();

struct Fuse_Controls_bundle : uObject
{
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> ImageElementDraw08020a1a_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& ImageElementDraw08020a1a() { return Fuse_Controls_bundle_typeof()->Init(), ImageElementDraw08020a1a_; }
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> SolidRectangle5950675f_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& SolidRectangle5950675f() { return Fuse_Controls_bundle_typeof()->Init(), SolidRectangle5950675f_; }
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> SolidRectangle5951675f_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& SolidRectangle5951675f() { return Fuse_Controls_bundle_typeof()->Init(), SolidRectangle5951675f_; }
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> SolidRectangle5953675f_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& SolidRectangle5953675f() { return Fuse_Controls_bundle_typeof()->Init(), SolidRectangle5953675f_; }
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> Viewport2da28930_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& Viewport2da28930() { return Fuse_Controls_bundle_typeof()->Init(), Viewport2da28930_; }
};
// }

} // ::g
