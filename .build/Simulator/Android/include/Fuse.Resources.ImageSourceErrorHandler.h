// This file was generated based on 'C:\ProgramData\Uno\Packages\Fuse.Elements\0.19.3\Resources\$.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Fuse{
namespace Resources{

// public delegate void ImageSourceErrorHandler(object sender, Fuse.Resources.ImageSourceErrorArgs args) :345
uDelegateType* ImageSourceErrorHandler_typeof();

}}} // ::g::Fuse::Resources
