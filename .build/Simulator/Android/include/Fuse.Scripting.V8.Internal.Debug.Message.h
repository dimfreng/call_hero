// This file was generated based on 'C:\ProgramData\Uno\Packages\Fuse.Scripting.V8\0.19.3\$.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <include/v8-debug.h>
#include <Uno.Object.h>

namespace g{
namespace Fuse{
namespace Scripting{
namespace V8{
namespace Internal{

// public struct Debug.Message :856
// {
uStructType* Debug__Message_typeof();
void Debug__Message__GetJSON_fn(::v8::Debug::Message** __this, ::v8::Local< ::v8::String>* __retval);

struct Debug__Message
{
    static ::v8::Local< ::v8::String> GetJSON(::v8::Debug::Message* __this);
};
// }

}}}}} // ::g::Fuse::Scripting::V8::Internal
