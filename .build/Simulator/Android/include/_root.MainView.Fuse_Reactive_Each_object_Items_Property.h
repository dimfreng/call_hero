// This file was generated based on 'C:\Users\Kevin\Desktop\callhero\CallHero\.build\Simulator\Android\Cache\GeneratedCode\MainView.g.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Object.h>
#include <Uno.UX.Property-1.h>
namespace g{namespace Fuse{namespace Reactive{struct Each;}}}
namespace g{struct MainView__Fuse_Reactive_Each_object_Items_Property;}

namespace g{

// public sealed class MainView.Fuse_Reactive_Each_object_Items_Property :25
// {
::g::Uno::UX::Property_type* MainView__Fuse_Reactive_Each_object_Items_Property_typeof();
void MainView__Fuse_Reactive_Each_object_Items_Property__ctor_1_fn(MainView__Fuse_Reactive_Each_object_Items_Property* __this, ::g::Fuse::Reactive::Each* obj);
void MainView__Fuse_Reactive_Each_object_Items_Property__New1_fn(::g::Fuse::Reactive::Each* obj, MainView__Fuse_Reactive_Each_object_Items_Property** __retval);
void MainView__Fuse_Reactive_Each_object_Items_Property__OnGet_fn(MainView__Fuse_Reactive_Each_object_Items_Property* __this, uObject** __retval);
void MainView__Fuse_Reactive_Each_object_Items_Property__OnSet_fn(MainView__Fuse_Reactive_Each_object_Items_Property* __this, uObject* v, uObject* origin);

struct MainView__Fuse_Reactive_Each_object_Items_Property : ::g::Uno::UX::Property
{
    uStrong< ::g::Fuse::Reactive::Each*> _obj;

    void ctor_1(::g::Fuse::Reactive::Each* obj);
    static MainView__Fuse_Reactive_Each_object_Items_Property* New1(::g::Fuse::Reactive::Each* obj);
};
// }

} // ::g
