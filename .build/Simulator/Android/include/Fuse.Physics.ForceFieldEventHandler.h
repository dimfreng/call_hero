// This file was generated based on 'C:\ProgramData\Uno\Packages\Fuse.Physics\0.19.3\$.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Fuse{
namespace Physics{

// public delegate void ForceFieldEventHandler(object sender, Fuse.Physics.ForceFieldEventArgs args) :379
uDelegateType* ForceFieldEventHandler_typeof();

}}} // ::g::Fuse::Physics
