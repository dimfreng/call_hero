// This file was generated based on 'C:\ProgramData\Uno\Packages\Fuse.iOS\0.19.3\.upk\meta'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.h>
namespace g{namespace Uno{namespace Runtime{namespace Implementation{namespace ShaderBackends{namespace OpenGL{struct GLProgram;}}}}}}
namespace g{struct Fuse_iOS_bundle;}

namespace g{

// public static generated class Fuse_iOS_bundle :0
// {
uClassType* Fuse_iOS_bundle_typeof();

struct Fuse_iOS_bundle : uObject
{
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> Blitterb638ee0c_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& Blitterb638ee0c() { return Fuse_iOS_bundle_typeof()->Init(), Blitterb638ee0c_; }
};
// }

} // ::g
