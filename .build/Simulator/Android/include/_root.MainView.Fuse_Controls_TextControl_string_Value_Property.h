// This file was generated based on 'C:\Users\Kevin\Desktop\callhero\CallHero\.build\Simulator\Android\Cache\GeneratedCode\MainView.g.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.String.h>
#include <Uno.UX.Property-1.h>
namespace g{namespace Fuse{namespace Controls{struct TextControl;}}}
namespace g{struct MainView__Fuse_Controls_TextControl_string_Value_Property;}

namespace g{

// public sealed class MainView.Fuse_Controls_TextControl_string_Value_Property :32
// {
::g::Uno::UX::Property_type* MainView__Fuse_Controls_TextControl_string_Value_Property_typeof();
void MainView__Fuse_Controls_TextControl_string_Value_Property__ctor_1_fn(MainView__Fuse_Controls_TextControl_string_Value_Property* __this, ::g::Fuse::Controls::TextControl* obj);
void MainView__Fuse_Controls_TextControl_string_Value_Property__New1_fn(::g::Fuse::Controls::TextControl* obj, MainView__Fuse_Controls_TextControl_string_Value_Property** __retval);
void MainView__Fuse_Controls_TextControl_string_Value_Property__OnAddListener_fn(MainView__Fuse_Controls_TextControl_string_Value_Property* __this, uDelegate* listener);
void MainView__Fuse_Controls_TextControl_string_Value_Property__OnGet_fn(MainView__Fuse_Controls_TextControl_string_Value_Property* __this, uString** __retval);
void MainView__Fuse_Controls_TextControl_string_Value_Property__OnRemoveListener_fn(MainView__Fuse_Controls_TextControl_string_Value_Property* __this, uDelegate* listener);
void MainView__Fuse_Controls_TextControl_string_Value_Property__OnSet_fn(MainView__Fuse_Controls_TextControl_string_Value_Property* __this, uString* v, uObject* origin);

struct MainView__Fuse_Controls_TextControl_string_Value_Property : ::g::Uno::UX::Property
{
    uStrong< ::g::Fuse::Controls::TextControl*> _obj;

    void ctor_1(::g::Fuse::Controls::TextControl* obj);
    static MainView__Fuse_Controls_TextControl_string_Value_Property* New1(::g::Fuse::Controls::TextControl* obj);
};
// }

} // ::g
