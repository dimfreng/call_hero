public partial class AppName: Fuse.Controls.Text
{
    static AppName()
    {
    }
    public AppName()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        this.FontSize = 25f;
        this.Font = global::MainView.seasrn;
    }
}
public partial class MainView: Fuse.App
{
    public sealed class Fuse_Controls_PageControl_Fuse_Node_Active_Property: Uno.UX.Property<Fuse.Node>
    {
        Fuse.Controls.PageControl _obj;
        public Fuse_Controls_PageControl_Fuse_Node_Active_Property(Fuse.Controls.PageControl obj) { _obj = obj; }
        protected override Fuse.Node OnGet() { return _obj.Active; }
        protected override void OnSet(Fuse.Node v, object origin) { _obj.Active = v; }
    }
    public sealed class Fuse_Reactive_Each_object_Items_Property: Uno.UX.Property<object>
    {
        Fuse.Reactive.Each _obj;
        public Fuse_Reactive_Each_object_Items_Property(Fuse.Reactive.Each obj) { _obj = obj; }
        protected override object OnGet() { return _obj.Items; }
        protected override void OnSet(object v, object origin) { _obj.Items = v; }
    }
    public sealed class Fuse_Controls_TextControl_string_Value_Property: Uno.UX.Property<string>
    {
        Fuse.Controls.TextControl _obj;
        public Fuse_Controls_TextControl_string_Value_Property(Fuse.Controls.TextControl obj) { _obj = obj; }
        protected override string OnGet() { return _obj.Value; }
        protected override void OnSet(string v, object origin) { _obj.SetValue(v, origin); }
        protected override void OnAddListener(Uno.UX.ValueChangedHandler<string> listener) { _obj.ValueChanged += listener; }
        protected override void OnRemoveListener(Uno.UX.ValueChangedHandler<string> listener) { _obj.ValueChanged -= listener; }
    }
    public partial class Factory: Uno.UX.IFactory
    {
        internal readonly MainView __parent;
        public Factory(MainView parent)
        {
            __parent = parent;
        }
        MainView.Fuse_Controls_TextControl_string_Value_Property temp_Value_inst;
        internal Fuse.Reactive.EventBinding temp_eb0;
        static Factory()
        {
        }
        public object New()
        {
            var self = new Fuse.Controls.Rectangle();
            var temp = new Fuse.Controls.Text();
            temp_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp);
            var temp1 = new Fuse.Reactive.DataBinding<string>(temp_Value_inst, "name");
            var temp2 = new Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
            var temp_eb0 = new Fuse.Reactive.EventBinding("clickHandler(contacts)");
            self.CornerRadius = float4(5f, 5f, 5f, 5f);
            self.Width = 150f;
            self.Height = 35f;
            self.Margin = float4(0f, 0f, 0f, 10f);
            self.Opacity = 0.7f;
            global::Fuse.Gestures.Clicked.AddHandler(self, temp_eb0.OnEvent);
            temp.TextColor = float4(0f, 0f, 0f, 1f);
            temp.Alignment = Fuse.Elements.Alignment.Center;
            temp.Behaviors.Add(temp1);
            self.Fill = temp2;
            self.Children.Add(temp);
            self.Behaviors.Add(temp_eb0);
            return self;
        }
    }
    MainView.Fuse_Reactive_Each_object_Items_Property temp_Items_inst;
    MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property temp1_Active_inst;
    [global::Uno.UX.UXGlobalResource("seasrn")] public static readonly Fuse.Font seasrn;
    internal Fuse.Controls.Page page1;
    internal Fuse.Controls.Grid loggedOutView;
    internal Fuse.Controls.Page Page2;
    static MainView()
    {
        seasrn = new Fuse.Font(new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/fonts/SEASRN__.ttf")));
        global::Uno.UX.Resource.SetGlobalKey(seasrn, "seasrn");
    }
    public MainView()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        var temp = new Fuse.Reactive.Each();
        temp_Items_inst = new MainView.Fuse_Reactive_Each_object_Items_Property(temp);
        var temp1 = new Fuse.Controls.PageControl();
        temp1_Active_inst = new MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property(temp1);
        var temp2 = new Fuse.Reactive.JavaScript();
        page1 = new Fuse.Controls.Page();
        loggedOutView = new Fuse.Controls.Grid();
        var temp3 = new Fuse.Controls.Rectangle();
        var temp4 = new Fuse.Drawing.ImageFill();
        var temp5 = new Fuse.Drawing.ImageFill();
        var temp6 = new Fuse.Controls.Grid();
        var temp7 = new AppName();
        var temp8 = new Fuse.Controls.Circle();
        var temp9 = new Fuse.Drawing.ImageFill();
        var temp10 = new AppName();
        var temp11 = new Fuse.Controls.Text();
        var temp12 = new Fuse.Controls.ScrollView();
        var temp13 = new Fuse.Controls.StackPanel();
        var temp14 = new Factory(this);
        var temp15 = new Fuse.Reactive.DataBinding<object>(temp_Items_inst, "categories");
        Page2 = new Fuse.Controls.Page();
        var temp16 = new Fuse.Controls.Rectangle();
        var temp17 = new Fuse.Drawing.ImageFill();
        var temp18 = new Fuse.Drawing.ImageFill();
        var temp19 = new Fuse.Reactive.DataBinding<Fuse.Node>(temp1_Active_inst, "currentPage");
        temp2.LineNumber = 4;
        temp2.FileName = "MainView.ux";
        temp2.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../MainView.js"));
        temp1.Children.Add(page1);
        temp1.Children.Add(Page2);
        temp1.Behaviors.Add(temp19);
        page1.Name = "page1";
        page1.Children.Add(loggedOutView);
        loggedOutView.RowData = "1*,0.5*,2.5*";
        loggedOutView.Padding = float4(10f, 0f, 10f, 0f);
        loggedOutView.Name = "loggedOutView";
        loggedOutView.Children.Add(temp3);
        loggedOutView.Children.Add(temp6);
        loggedOutView.Children.Add(temp11);
        loggedOutView.Children.Add(temp12);
        temp3.Layer = Fuse.Layer.Background;
        temp3.Fills.Add(temp4);
        temp3.Fills.Add(temp5);
        temp4.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/phone.jpg"));
        temp5.Opacity = 0.3f;
        temp5.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/overlay.jpg"));
        temp6.RowCount = 1;
        temp6.ColumnCount = 3;
        global::Fuse.Controls.Grid.SetRow(temp6, 0);
        temp6.Children.Add(temp7);
        temp6.Children.Add(temp8);
        temp6.Children.Add(temp10);
        temp7.Value = "Call";
        temp7.TextColor = float4(1f, 1f, 1f, 1f);
        temp7.Margin = float4(0f, 35f, 0f, 0f);
        global::Fuse.Controls.Grid.SetColumn(temp7, 0);
        temp8.Width = 75f;
        temp8.Height = 75f;
        global::Fuse.Controls.Grid.SetColumn(temp8, 1);
        temp8.Fills.Add(temp9);
        temp9.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/CH-Icon.jpg"));
        temp10.Value = "Hero";
        temp10.Margin = float4(5f, 35f, 0f, 0f);
        global::Fuse.Controls.Grid.SetColumn(temp10, 2);
        temp11.Value = "Available Categories";
        temp11.FontSize = 20f;
        temp11.Alignment = Fuse.Elements.Alignment.Center;
        global::Fuse.Controls.Grid.SetRow(temp11, 1);
        temp12.Margin = float4(0f, 0f, 0f, 10f);
        global::Fuse.Controls.Grid.SetRow(temp12, 2);
        temp12.Content = temp13;
        temp13.Behaviors.Add(temp15);
        temp13.Behaviors.Add(temp);
        temp.Factories.Add(temp14);
        Page2.Name = "Page2";
        Page2.Children.Add(temp16);
        temp16.Layer = Fuse.Layer.Background;
        temp16.Fills.Add(temp17);
        temp16.Fills.Add(temp18);
        temp17.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/background.jpg"));
        temp18.Opacity = 0.3f;
        temp18.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/overlay.jpg"));
        this.RootNode = temp1;
        this.Theme = Fuse.BasicTheme.BasicTheme.Singleton;
        this.Behaviors.Add(temp2);
    }
}
