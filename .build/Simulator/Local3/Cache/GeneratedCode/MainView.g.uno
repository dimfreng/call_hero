public partial class TaskItem: Fuse.Controls.DockPanel
{
    MainView.Fuse_Controls_Image_Fuse_Resources_ImageSource_Source_Property temp_Source_inst;
    MainView.Fuse_Controls_TextControl_string_Value_Property temp1_Value_inst;
    MainView.Fuse_Controls_TextControl_string_Value_Property temp2_Value_inst;
    static TaskItem()
    {
    }
    public TaskItem()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        var temp = new Fuse.Controls.Image();
        temp_Source_inst = new MainView.Fuse_Controls_Image_Fuse_Resources_ImageSource_Source_Property(temp);
        var temp1 = new Fuse.Controls.Text();
        temp1_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp1);
        var temp2 = new Fuse.Controls.Text();
        temp2_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp2);
        var temp3 = new Fuse.Reactive.DataToResourceBinding<Fuse.Resources.ImageSource>(temp_Source_inst, "image");
        var temp4 = new Fuse.Controls.Grid();
        var temp5 = new Fuse.Reactive.DataBinding<string>(temp1_Value_inst, "text");
        var temp6 = new Fuse.Reactive.DataBinding<string>(temp2_Value_inst, "timeSlot");
        var temp7 = new Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        this.Height = 80f;
        this.Margin = float4(1f, 1f, 1f, 1f);
        this.Padding = float4(15f, 20f, 15f, 20f);
        temp.Width = 50f;
        temp.Height = 50f;
        global::Fuse.Controls.DockPanel.SetDock(temp, Fuse.Layouts.Dock.Left);
        temp.Behaviors.Add(temp3);
        temp4.RowCount = 2;
        temp4.Margin = float4(40f, 0f, 40f, 0f);
        temp4.Children.Add(temp1);
        temp4.Children.Add(temp2);
        temp1.Behaviors.Add(temp5);
        temp2.TextColor = float4(0.6f, 0.6f, 0.6f, 1f);
        temp2.Behaviors.Add(temp6);
        this.Background = temp7;
        this.Children.Add(temp);
        this.Children.Add(temp4);
    }
}
public partial class MainView: Fuse.App
{
    public sealed class Fuse_Controls_Image_Fuse_Resources_ImageSource_Source_Property: Uno.UX.Property<Fuse.Resources.ImageSource>
    {
        Fuse.Controls.Image _obj;
        public Fuse_Controls_Image_Fuse_Resources_ImageSource_Source_Property(Fuse.Controls.Image obj) { _obj = obj; }
        protected override Fuse.Resources.ImageSource OnGet() { return _obj.Source; }
        protected override void OnSet(Fuse.Resources.ImageSource v, object origin) { _obj.Source = v; }
    }
    public sealed class Fuse_Controls_TextControl_string_Value_Property: Uno.UX.Property<string>
    {
        Fuse.Controls.TextControl _obj;
        public Fuse_Controls_TextControl_string_Value_Property(Fuse.Controls.TextControl obj) { _obj = obj; }
        protected override string OnGet() { return _obj.Value; }
        protected override void OnSet(string v, object origin) { _obj.SetValue(v, origin); }
        protected override void OnAddListener(Uno.UX.ValueChangedHandler<string> listener) { _obj.ValueChanged += listener; }
        protected override void OnRemoveListener(Uno.UX.ValueChangedHandler<string> listener) { _obj.ValueChanged -= listener; }
    }
    public sealed class Fuse_Reactive_Each_object_Items_Property: Uno.UX.Property<object>
    {
        Fuse.Reactive.Each _obj;
        public Fuse_Reactive_Each_object_Items_Property(Fuse.Reactive.Each obj) { _obj = obj; }
        protected override object OnGet() { return _obj.Items; }
        protected override void OnSet(object v, object origin) { _obj.Items = v; }
    }
    public sealed class Fuse_Animations_Animator_double_Delay_Property: Uno.UX.Property<double>
    {
        Fuse.Animations.Animator _obj;
        public Fuse_Animations_Animator_double_Delay_Property(Fuse.Animations.Animator obj) { _obj = obj; }
        protected override double OnGet() { return _obj.Delay; }
        protected override void OnSet(double v, object origin) { _obj.Delay = v; }
    }
    public sealed class Fuse_Elements_Element_float_Opacity_Property: Uno.UX.Property<float>
    {
        Fuse.Elements.Element _obj;
        public Fuse_Elements_Element_float_Opacity_Property(Fuse.Elements.Element obj) { _obj = obj; }
        protected override float OnGet() { return _obj.Opacity; }
        protected override void OnSet(float v, object origin) { _obj.Opacity = v; }
    }
    public sealed class Fuse_Scaling_float_Factor_Property: Uno.UX.Property<float>
    {
        Fuse.Scaling _obj;
        public Fuse_Scaling_float_Factor_Property(Fuse.Scaling obj) { _obj = obj; }
        protected override float OnGet() { return _obj.Factor; }
        protected override void OnSet(float v, object origin) { _obj.Factor = v; }
    }
    public sealed class Fuse_Triggers_WhileBool_bool_Value_Property: Uno.UX.Property<bool>
    {
        Fuse.Triggers.WhileBool _obj;
        public Fuse_Triggers_WhileBool_bool_Value_Property(Fuse.Triggers.WhileBool obj) { _obj = obj; }
        protected override bool OnGet() { return _obj.Value; }
        protected override void OnSet(bool v, object origin) { _obj.Value = v; }
    }
    public sealed class Fuse_Translation_float_Y_Property: Uno.UX.Property<float>
    {
        Fuse.Translation _obj;
        public Fuse_Translation_float_Y_Property(Fuse.Translation obj) { _obj = obj; }
        protected override float OnGet() { return _obj.Y; }
        protected override void OnSet(float v, object origin) { _obj.Y = v; }
    }
    public sealed class Fuse_Translation_float_X_Property: Uno.UX.Property<float>
    {
        Fuse.Translation _obj;
        public Fuse_Translation_float_X_Property(Fuse.Translation obj) { _obj = obj; }
        protected override float OnGet() { return _obj.X; }
        protected override void OnSet(float v, object origin) { _obj.X = v; }
    }
    public sealed class Fuse_Elements_Element_float_Width_Property: Uno.UX.Property<float>
    {
        Fuse.Elements.Element _obj;
        public Fuse_Elements_Element_float_Width_Property(Fuse.Elements.Element obj) { _obj = obj; }
        protected override float OnGet() { return _obj.Width; }
        protected override void OnSet(float v, object origin) { _obj.Width = v; }
    }
    public sealed class Fuse_Controls_Circle_float_LengthAngleDegrees_Property: Uno.UX.Property<float>
    {
        Fuse.Controls.Circle _obj;
        public Fuse_Controls_Circle_float_LengthAngleDegrees_Property(Fuse.Controls.Circle obj) { _obj = obj; }
        protected override float OnGet() { return _obj.LengthAngleDegrees; }
        protected override void OnSet(float v, object origin) { _obj.LengthAngleDegrees = v; }
    }
    public sealed class Fuse_Node_bool_IsEnabled_Property: Uno.UX.Property<bool>
    {
        Fuse.Node _obj;
        public Fuse_Node_bool_IsEnabled_Property(Fuse.Node obj) { _obj = obj; }
        protected override bool OnGet() { return _obj.IsEnabled; }
        protected override void OnSet(bool v, object origin) { _obj.IsEnabled = v; }
    }
    public partial class Template: Uno.UX.Template<Fuse.Controls.Text>
    {
        internal readonly MainView __parent;
        public Template(MainView parent)
        {
            __parent = parent;
        }
        static Template()
        {
        }
        protected override void OnApply(Fuse.Controls.Text self)
        {
            Fuse.Controls.TextControl.FontSizeProperty.SetStyle(self, 16f);
            Fuse.Elements.Element.AlignmentProperty.SetStyle(self, Fuse.Elements.Alignment.Center);
        }
    }
    public partial class Factory: Uno.UX.IFactory
    {
        internal readonly MainView __parent;
        public Factory(MainView parent)
        {
            __parent = parent;
        }
        MainView.Fuse_Translation_float_Y_Property trans_Y_inst;
        MainView.Fuse_Scaling_float_Factor_Property scaling_Factor_inst;
        MainView.Fuse_Animations_Animator_double_Delay_Property temp_Delay_inst;
        MainView.Fuse_Animations_Animator_double_Delay_Property temp1_Delay_inst;
        internal Fuse.Triggers.WhileTrue animateItem;
        internal Fuse.Translation trans;
        internal Fuse.Scaling scaling;
        static Factory()
        {
        }
        public object New()
        {
            var self = new TaskItem();
            var trans = new Fuse.Translation();
            trans_Y_inst = new MainView.Fuse_Translation_float_Y_Property(trans);
            var scaling = new Fuse.Scaling();
            scaling_Factor_inst = new MainView.Fuse_Scaling_float_Factor_Property(scaling);
            var temp = new Fuse.Animations.Change<float>(trans_Y_inst);
            temp_Delay_inst = new MainView.Fuse_Animations_Animator_double_Delay_Property(temp);
            var temp1 = new Fuse.Animations.Change<float>(scaling_Factor_inst);
            temp1_Delay_inst = new MainView.Fuse_Animations_Animator_double_Delay_Property(temp1);
            var temp2 = new Fuse.Triggers.OnUserEvent();
            var temp3 = new Fuse.Controls.Toggle();
            var animateItem = new Fuse.Triggers.WhileTrue();
            var temp4 = new Fuse.Reactive.DataBinding<double>(temp_Delay_inst, "delay");
            var temp5 = new Fuse.Reactive.DataBinding<double>(temp1_Delay_inst, "delay");
            temp2.Name = "ToggleLoggedIn";
            temp2.Actions.Add(temp3);
            temp3.Target = animateItem;
            animateItem.Animators.Add(temp);
            animateItem.Animators.Add(temp1);
            temp.Value = 0f;
            temp.Easing = Fuse.Animations.Easing.QuadraticInOut;
            temp.Duration = 0.8;
            temp1.Value = 1f;
            temp1.Easing = Fuse.Animations.Easing.QuadraticInOut;
            temp1.Duration = 0.6;
            trans.Y = -0.5f;
            trans.RelativeTo = Fuse.TranslationModes.Size;
            scaling.Factor = 1.2f;
            self.Behaviors.Add(temp4);
            self.Behaviors.Add(temp5);
            self.Behaviors.Add(temp2);
            self.Behaviors.Add(animateItem);
            self.Transforms.Add(trans);
            self.Transforms.Add(scaling);
            return self;
        }
    }
    MainView.Fuse_Elements_Element_float_Opacity_Property plusButton_Opacity_inst;
    MainView.Fuse_Scaling_float_Factor_Property plusButtonScaling_Factor_inst;
    MainView.Fuse_Triggers_WhileBool_bool_Value_Property scalePlusButton_Value_inst;
    MainView.Fuse_Elements_Element_float_Opacity_Property cross_Opacity_inst;
    MainView.Fuse_Translation_float_Y_Property plusButtonTranslation_Y_inst;
    MainView.Fuse_Translation_float_X_Property plusButtonTranslation_X_inst;
    MainView.Fuse_Reactive_Each_object_Items_Property temp_Items_inst;
    MainView.Fuse_Triggers_WhileBool_bool_Value_Property changeWidth_Value_inst;
    MainView.Fuse_Triggers_WhileBool_bool_Value_Property loadCircle_Value_inst;
    MainView.Fuse_Triggers_WhileBool_bool_Value_Property scaleAndFade_Value_inst;
    MainView.Fuse_Triggers_WhileBool_bool_Value_Property showLoggedIn_Value_inst;
    MainView.Fuse_Elements_Element_float_Width_Property rectNormalScale_Width_inst;
    MainView.Fuse_Elements_Element_float_Opacity_Property text_Opacity_inst;
    MainView.Fuse_Elements_Element_float_Opacity_Property loadingCircle_Opacity_inst;
    MainView.Fuse_Controls_Circle_float_LengthAngleDegrees_Property loadingCircle_LengthAngleDegrees_inst;
    MainView.Fuse_Elements_Element_float_Opacity_Property loadingCirclePanel_Opacity_inst;
    MainView.Fuse_Elements_Element_float_Opacity_Property loadingButton_Opacity_inst;
    MainView.Fuse_Elements_Element_float_Opacity_Property rectNormalScale_Opacity_inst;
    MainView.Fuse_Scaling_float_Factor_Property loginButtonScaling_Factor_inst;
    MainView.Fuse_Elements_Element_float_Opacity_Property loggedInView_Opacity_inst;
    MainView.Fuse_Elements_Element_float_Opacity_Property goodMorningText_Opacity_inst;
    MainView.Fuse_Elements_Element_float_Opacity_Property monthPanel_Opacity_inst;
    MainView.Fuse_Translation_float_Y_Property weekTranslation_Y_inst;
    MainView.Fuse_Scaling_float_Factor_Property weekScaling_Factor_inst;
    MainView.Fuse_Scaling_float_Factor_Property headerScaling_Factor_inst;
    MainView.Fuse_Triggers_WhileBool_bool_Value_Property showPlusButton_Value_inst;
    MainView.Fuse_Elements_Element_float_Opacity_Property profile_Opacity_inst;
    MainView.Fuse_Scaling_float_Factor_Property profileScaling_Factor_inst;
    MainView.Fuse_Node_bool_IsEnabled_Property loggedInView_IsEnabled_inst;
    internal Fuse.Controls.Panel loggedInView;
    internal Fuse.Controls.Circle plusButton;
    internal Fuse.Controls.Panel cross;
    internal Fuse.Translation plusButtonTranslation;
    internal Fuse.Scaling plusButtonScaling;
    internal Fuse.Triggers.WhileTrue showPlusButton;
    internal Fuse.Triggers.WhileTrue scalePlusButton;
    internal Fuse.Translation weekTranslation;
    internal Fuse.Scaling weekScaling;
    internal Fuse.Scaling headerScaling;
    internal Fuse.Controls.Text goodMorningText;
    internal Fuse.Controls.Circle profile;
    internal Fuse.Scaling profileScaling;
    internal Fuse.Controls.Grid monthPanel;
    internal Fuse.Triggers.WhileTrue loading;
    internal Fuse.Triggers.WhileTrue changeWidth;
    internal Fuse.Triggers.WhileTrue loadCircle;
    internal Fuse.Triggers.WhileTrue scaleAndFade;
    internal Fuse.Triggers.WhileTrue showLoggedIn;
    internal Fuse.Controls.Grid loggedOutView;
    internal Fuse.Controls.Text text;
    internal Fuse.Controls.Panel loadingCirclePanel;
    internal Fuse.Controls.Circle loadingCircle;
    internal Fuse.Controls.Rectangle rectNormalScale;
    internal Fuse.Controls.Panel loadingButton;
    internal Fuse.Scaling loginButtonScaling;
    static MainView()
    {
    }
    public MainView()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        var temp1 = new Fuse.Resources.FileImageSource();
        var temp2 = new Fuse.Resources.FileImageSource();
        var temp3 = new Fuse.Resources.FileImageSource();
        plusButton = new Fuse.Controls.Circle();
        plusButton_Opacity_inst = new MainView.Fuse_Elements_Element_float_Opacity_Property(plusButton);
        plusButtonScaling = new Fuse.Scaling();
        plusButtonScaling_Factor_inst = new MainView.Fuse_Scaling_float_Factor_Property(plusButtonScaling);
        scalePlusButton = new Fuse.Triggers.WhileTrue();
        scalePlusButton_Value_inst = new MainView.Fuse_Triggers_WhileBool_bool_Value_Property(scalePlusButton);
        cross = new Fuse.Controls.Panel();
        cross_Opacity_inst = new MainView.Fuse_Elements_Element_float_Opacity_Property(cross);
        plusButtonTranslation = new Fuse.Translation();
        plusButtonTranslation_Y_inst = new MainView.Fuse_Translation_float_Y_Property(plusButtonTranslation);
        plusButtonTranslation_X_inst = new MainView.Fuse_Translation_float_X_Property(plusButtonTranslation);
        var temp = new Fuse.Reactive.Each();
        temp_Items_inst = new MainView.Fuse_Reactive_Each_object_Items_Property(temp);
        changeWidth = new Fuse.Triggers.WhileTrue();
        changeWidth_Value_inst = new MainView.Fuse_Triggers_WhileBool_bool_Value_Property(changeWidth);
        loadCircle = new Fuse.Triggers.WhileTrue();
        loadCircle_Value_inst = new MainView.Fuse_Triggers_WhileBool_bool_Value_Property(loadCircle);
        scaleAndFade = new Fuse.Triggers.WhileTrue();
        scaleAndFade_Value_inst = new MainView.Fuse_Triggers_WhileBool_bool_Value_Property(scaleAndFade);
        showLoggedIn = new Fuse.Triggers.WhileTrue();
        showLoggedIn_Value_inst = new MainView.Fuse_Triggers_WhileBool_bool_Value_Property(showLoggedIn);
        rectNormalScale = new Fuse.Controls.Rectangle();
        rectNormalScale_Width_inst = new MainView.Fuse_Elements_Element_float_Width_Property(rectNormalScale);
        text = new Fuse.Controls.Text();
        text_Opacity_inst = new MainView.Fuse_Elements_Element_float_Opacity_Property(text);
        loadingCircle = new Fuse.Controls.Circle();
        loadingCircle_Opacity_inst = new MainView.Fuse_Elements_Element_float_Opacity_Property(loadingCircle);
        loadingCircle_LengthAngleDegrees_inst = new MainView.Fuse_Controls_Circle_float_LengthAngleDegrees_Property(loadingCircle);
        loadingCirclePanel = new Fuse.Controls.Panel();
        loadingCirclePanel_Opacity_inst = new MainView.Fuse_Elements_Element_float_Opacity_Property(loadingCirclePanel);
        loadingButton = new Fuse.Controls.Panel();
        loadingButton_Opacity_inst = new MainView.Fuse_Elements_Element_float_Opacity_Property(loadingButton);
        rectNormalScale_Opacity_inst = new MainView.Fuse_Elements_Element_float_Opacity_Property(rectNormalScale);
        loginButtonScaling = new Fuse.Scaling();
        loginButtonScaling_Factor_inst = new MainView.Fuse_Scaling_float_Factor_Property(loginButtonScaling);
        loggedInView = new Fuse.Controls.Panel();
        loggedInView_Opacity_inst = new MainView.Fuse_Elements_Element_float_Opacity_Property(loggedInView);
        goodMorningText = new Fuse.Controls.Text();
        goodMorningText_Opacity_inst = new MainView.Fuse_Elements_Element_float_Opacity_Property(goodMorningText);
        monthPanel = new Fuse.Controls.Grid();
        monthPanel_Opacity_inst = new MainView.Fuse_Elements_Element_float_Opacity_Property(monthPanel);
        weekTranslation = new Fuse.Translation();
        weekTranslation_Y_inst = new MainView.Fuse_Translation_float_Y_Property(weekTranslation);
        weekScaling = new Fuse.Scaling();
        weekScaling_Factor_inst = new MainView.Fuse_Scaling_float_Factor_Property(weekScaling);
        headerScaling = new Fuse.Scaling();
        headerScaling_Factor_inst = new MainView.Fuse_Scaling_float_Factor_Property(headerScaling);
        showPlusButton = new Fuse.Triggers.WhileTrue();
        showPlusButton_Value_inst = new MainView.Fuse_Triggers_WhileBool_bool_Value_Property(showPlusButton);
        profile = new Fuse.Controls.Circle();
        profile_Opacity_inst = new MainView.Fuse_Elements_Element_float_Opacity_Property(profile);
        profileScaling = new Fuse.Scaling();
        profileScaling_Factor_inst = new MainView.Fuse_Scaling_float_Factor_Property(profileScaling);
        loggedInView_IsEnabled_inst = new MainView.Fuse_Node_bool_IsEnabled_Property(loggedInView);
        var temp4 = new Fuse.Controls.Panel();
        var temp5 = new Fuse.iOS.StatusBarConfig();
        var temp6 = new Uno.UX.Resource("home1", temp1);
        var temp7 = new Uno.UX.Resource("home2", temp2);
        var temp8 = new Uno.UX.Resource("home3", temp3);
        var temp9 = new Fuse.UserEvent("ToggleLoggedIn");
        var temp10 = new Fuse.Controls.Panel();
        var temp11 = new Fuse.Reactive.JavaScript();
        var temp12 = new Fuse.Controls.Rectangle();
        var temp13 = new Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        var temp14 = new Fuse.Controls.Rectangle();
        var temp15 = new Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        var temp16 = new Fuse.Animations.Change<float>(plusButton_Opacity_inst);
        var temp17 = new Fuse.Animations.Change<float>(plusButtonScaling_Factor_inst);
        var temp18 = new Fuse.Gestures.Clicked();
        var temp19 = new Fuse.Controls.Toggle();
        var temp20 = new Fuse.Triggers.Actions.RaiseUserEvent();
        var temp21 = new Fuse.Animations.Change<bool>(scalePlusButton_Value_inst);
        var temp22 = new Fuse.Animations.Nothing();
        var temp23 = new Fuse.Animations.Change<float>(cross_Opacity_inst);
        var temp24 = new Fuse.Animations.Change<float>(plusButtonScaling_Factor_inst);
        var temp25 = new Fuse.Animations.Change<float>(plusButtonTranslation_Y_inst);
        var temp26 = new Fuse.Animations.Change<float>(plusButtonTranslation_X_inst);
        var temp27 = new Fuse.Drawing.StaticSolidColor(float4(1f, 0.2f, 0.4f, 1f));
        var temp28 = new Fuse.Controls.Grid();
        var temp29 = new Fuse.Controls.DockPanel();
        var temp30 = new Fuse.Controls.StackPanel();
        var temp31 = new Factory(this);
        var temp32 = new Fuse.Reactive.DataBinding<object>(temp_Items_inst, "items");
        var temp33 = new Fuse.Controls.Grid();
        var temp34 = new Fuse.Style();
        var temp35 = new Template(this) { Cascade = true, AffectSubtypes = true };
        var temp36 = new Fuse.Controls.Text();
        var temp37 = new Fuse.Controls.Text();
        var temp38 = new Fuse.Controls.Text();
        var temp39 = new Fuse.Controls.Text();
        var temp40 = new Fuse.Controls.Text();
        var temp41 = new Fuse.Controls.Text();
        var temp42 = new Fuse.Controls.Text();
        var temp43 = new Fuse.Controls.Text();
        var temp44 = new Fuse.Controls.Text();
        var temp45 = new Fuse.Controls.Text();
        var temp46 = new Fuse.Controls.Text();
        var temp47 = new Fuse.Controls.Text();
        var temp48 = new Fuse.Controls.Text();
        var temp49 = new Fuse.Controls.Text();
        var temp50 = new Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        var temp51 = new Fuse.Drawing.StaticSolidColor(float4(0.8666667f, 0.8666667f, 0.8666667f, 1f));
        var temp52 = new Fuse.Controls.Grid();
        var temp53 = new Fuse.Controls.Image();
        var temp54 = new Fuse.Drawing.ImageFill();
        var temp55 = new Fuse.Controls.Circle();
        var temp56 = new Fuse.Controls.Text();
        var temp57 = new Fuse.Drawing.StaticSolidColor(float4(0f, 1f, 1f, 1f));
        var temp58 = new Fuse.Controls.Image();
        var temp59 = new Fuse.Controls.Text();
        var temp60 = new Fuse.Controls.Image();
        var temp61 = new Fuse.Rotation();
        loading = new Fuse.Triggers.WhileTrue();
        var temp62 = new Fuse.Animations.Change<bool>(changeWidth_Value_inst);
        var temp63 = new Fuse.Animations.Change<bool>(loadCircle_Value_inst);
        var temp64 = new Fuse.Animations.Change<bool>(scaleAndFade_Value_inst);
        var temp65 = new Fuse.Animations.Change<bool>(showLoggedIn_Value_inst);
        var temp66 = new Fuse.Animations.Change<float>(rectNormalScale_Width_inst);
        var temp67 = new Fuse.Animations.Change<float>(text_Opacity_inst);
        var temp68 = new Fuse.Animations.Change<float>(loadingCircle_Opacity_inst);
        var temp69 = new Fuse.Animations.Spin();
        var temp70 = new Fuse.Animations.Cycle<float>(loadingCircle_LengthAngleDegrees_inst);
        var temp71 = new Fuse.Animations.Change<float>(loadingCirclePanel_Opacity_inst);
        var temp72 = new Fuse.Animations.Change<float>(loadingButton_Opacity_inst);
        var temp73 = new Fuse.Animations.Change<float>(rectNormalScale_Opacity_inst);
        var temp74 = new Fuse.Animations.Change<float>(loginButtonScaling_Factor_inst);
        var temp75 = new Fuse.Animations.Change<float>(loggedInView_Opacity_inst);
        var temp76 = new Fuse.Animations.Change<float>(goodMorningText_Opacity_inst);
        var temp77 = new Fuse.Animations.Change<float>(monthPanel_Opacity_inst);
        var temp78 = new Fuse.Animations.Change<float>(weekTranslation_Y_inst);
        var temp79 = new Fuse.Animations.Change<float>(weekScaling_Factor_inst);
        var temp80 = new Fuse.Animations.Change<float>(headerScaling_Factor_inst);
        var temp81 = new Fuse.Animations.Change<bool>(showPlusButton_Value_inst);
        var temp82 = new Fuse.Triggers.Actions.RaiseUserEvent();
        var temp83 = new Fuse.Animations.Change<float>(profile_Opacity_inst);
        var temp84 = new Fuse.Animations.Change<float>(profileScaling_Factor_inst);
        var temp85 = new Fuse.Animations.Change<bool>(loggedInView_IsEnabled_inst);
        loggedOutView = new Fuse.Controls.Grid();
        var temp86 = new Fuse.Controls.Rectangle();
        var temp87 = new Fuse.Drawing.ImageFill();
        var temp88 = new Fuse.Controls.Panel();
        var temp89 = new Fuse.Drawing.Stroke();
        var temp90 = new Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        var temp91 = new Fuse.Gestures.Clicked();
        var temp92 = new Fuse.Controls.Toggle();
        var temp93 = new Fuse.Drawing.StaticSolidColor(float4(1f, 0.2f, 0.4f, 1f));
        var temp94 = new Fuse.Controls.Circle();
        var temp95 = new Fuse.Drawing.StaticSolidColor(float4(1f, 0.2f, 0.4f, 1f));
        var temp96 = new Fuse.Controls.Image();
        var temp97 = new Fuse.Controls.Grid();
        var temp98 = new Fuse.Controls.Image();
        var temp99 = new Fuse.Controls.TextEdit();
        var temp100 = new Fuse.Controls.Rectangle();
        var temp101 = new Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        var temp102 = new Fuse.Controls.Image();
        var temp103 = new Fuse.Controls.TextEdit();
        var temp104 = new Fuse.Controls.Rectangle();
        var temp105 = new Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        temp4.Children.Add(temp10);
        temp4.Behaviors.Add(temp5);
        temp4.Behaviors.Add(temp9);
        temp4.Resources.Add(temp6);
        temp4.Resources.Add(temp7);
        temp4.Resources.Add(temp8);
        temp5.Style = Uno.Platform.iOS.StatusBarStyle.Light;
        temp1.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/avatar1.png"));
        temp2.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/avatar2.png"));
        temp3.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/avatar3.png"));
        temp10.Children.Add(loggedInView);
        temp10.Children.Add(loggedOutView);
        temp10.Behaviors.Add(temp11);
        temp10.Behaviors.Add(loading);
        temp10.Behaviors.Add(changeWidth);
        temp10.Behaviors.Add(loadCircle);
        temp10.Behaviors.Add(scaleAndFade);
        temp10.Behaviors.Add(showLoggedIn);
        temp11.LineNumber = 31;
        temp11.FileName = "MainView.ux";
        temp11.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../MainView.js"));
        loggedInView.Opacity = 0f;
        loggedInView.Name = "loggedInView";
        loggedInView.IsEnabled = false;
        loggedInView.Children.Add(plusButton);
        loggedInView.Children.Add(temp28);
        plusButton.Width = 60f;
        plusButton.Height = 60f;
        plusButton.Alignment = Fuse.Elements.Alignment.BottomRight;
        plusButton.Margin = float4(20f, 20f, 20f, 20f);
        plusButton.Opacity = 0f;
        plusButton.Name = "plusButton";
        plusButton.Fill = temp27;
        plusButton.Children.Add(cross);
        plusButton.Behaviors.Add(showPlusButton);
        plusButton.Behaviors.Add(temp18);
        plusButton.Behaviors.Add(scalePlusButton);
        plusButton.Transforms.Add(plusButtonTranslation);
        plusButton.Transforms.Add(plusButtonScaling);
        cross.Name = "cross";
        cross.Children.Add(temp12);
        cross.Children.Add(temp14);
        Fuse.Elements.Element.WidthProperty.Set(temp12, 30f, global::Fuse.Elements.SizeUnit.Percent);
        temp12.Height = 2f;
        temp12.Fill = temp13;
        temp14.Width = 2f;
        Fuse.Elements.Element.HeightProperty.Set(temp14, 30f, global::Fuse.Elements.SizeUnit.Percent);
        temp14.Fill = temp15;
        plusButtonTranslation.RelativeTo = Fuse.TranslationModes.ParentSize;
        plusButtonScaling.Factor = 0.1f;
        showPlusButton.Animators.Add(temp16);
        showPlusButton.Animators.Add(temp17);
        temp16.Value = 1f;
        temp16.Duration = 0.1;
        temp16.DurationBack = 0;
        temp17.Value = 1f;
        temp17.Easing = Fuse.Animations.Easing.CircularInOut;
        temp17.Duration = 0.4;
        temp17.DurationBack = 0;
        temp18.Animators.Add(temp21);
        temp18.Animators.Add(temp22);
        temp18.Actions.Add(temp19);
        temp18.Actions.Add(temp20);
        temp19.Delay = 0.2f;
        temp19.Target = loading;
        temp20.Name = "ToggleLoggedIn";
        temp20.Delay = 0.3f;
        temp21.Value = true;
        temp22.Duration = 0.8;
        scalePlusButton.Animators.Add(temp23);
        scalePlusButton.Animators.Add(temp24);
        scalePlusButton.Animators.Add(temp25);
        scalePlusButton.Animators.Add(temp26);
        temp23.Value = 0f;
        temp23.Duration = 0.1;
        temp24.Value = 15f;
        temp24.Easing = Fuse.Animations.Easing.CircularInOut;
        temp24.Duration = 0.6;
        temp25.Value = -0.5f;
        temp25.Easing = Fuse.Animations.Easing.CircularInOut;
        temp25.Duration = 0.6;
        temp25.DurationBack = 0;
        temp26.Value = -0.5f;
        temp26.Easing = Fuse.Animations.Easing.CircularInOut;
        temp26.Duration = 0.6;
        temp26.DurationBack = 0;
        temp28.RowCount = 2;
        temp28.Children.Add(temp29);
        temp28.Children.Add(temp52);
        global::Fuse.Controls.Grid.SetRow(temp29, 1);
        temp29.Background = temp51;
        temp29.Children.Add(temp30);
        temp29.Children.Add(temp33);
        temp30.Behaviors.Add(temp32);
        temp30.Behaviors.Add(temp);
        temp.Factories.Add(temp31);
        temp33.RowCount = 2;
        temp33.ColumnCount = 7;
        temp33.Height = 100f;
        temp33.Margin = float4(1f, 1f, 1f, 1f);
        temp33.Padding = float4(15f, 15f, 15f, 15f);
        global::Fuse.Controls.DockPanel.SetDock(temp33, Fuse.Layouts.Dock.Top);
        temp33.Background = temp50;
        temp33.Children.Add(temp36);
        temp33.Children.Add(temp37);
        temp33.Children.Add(temp38);
        temp33.Children.Add(temp39);
        temp33.Children.Add(temp40);
        temp33.Children.Add(temp41);
        temp33.Children.Add(temp42);
        temp33.Children.Add(temp43);
        temp33.Children.Add(temp44);
        temp33.Children.Add(temp45);
        temp33.Children.Add(temp46);
        temp33.Children.Add(temp47);
        temp33.Children.Add(temp48);
        temp33.Children.Add(temp49);
        temp33.Styles.Add(temp34);
        temp33.Transforms.Add(weekTranslation);
        temp33.Transforms.Add(weekScaling);
        temp34.Templates.Add(temp35);
        temp36.Value = "SUN";
        temp37.Value = "MON";
        temp38.Value = "TUE";
        temp39.Value = "WED";
        temp40.Value = "THU";
        temp41.Value = "FRI";
        temp42.Value = "SAT";
        temp43.Value = "7";
        temp44.Value = "8";
        temp45.Value = "9";
        temp46.Value = "10";
        temp47.Value = "11";
        temp48.Value = "12";
        temp49.Value = "13";
        weekTranslation.Y = -0.5f;
        weekTranslation.RelativeTo = Fuse.TranslationModes.Size;
        weekScaling.Factor = 1.2f;
        temp52.Rows = "1*,auto,100";
        global::Fuse.Controls.Grid.SetRow(temp52, 0);
        temp52.Children.Add(temp53);
        temp52.Children.Add(goodMorningText);
        temp52.Children.Add(profile);
        temp52.Children.Add(monthPanel);
        temp53.Color = float4(0.8666667f, 0.8666667f, 0.8666667f, 1f);
        temp53.StretchMode = Fuse.Elements.StretchMode.UniformToFill;
        temp53.Layer = Fuse.Layer.Background;
        temp53.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/headerhome.png"));
        temp53.Transforms.Add(headerScaling);
        headerScaling.Factor = 1.4f;
        goodMorningText.Value = "Good Morning!";
        goodMorningText.FontSize = 32f;
        goodMorningText.TextColor = float4(1f, 1f, 1f, 1f);
        goodMorningText.Alignment = Fuse.Elements.Alignment.Center;
        goodMorningText.Opacity = 0f;
        goodMorningText.Name = "goodMorningText";
        profile.Width = 100f;
        profile.Height = 100f;
        profile.Opacity = 0f;
        profile.Name = "profile";
        profile.Fills.Add(temp54);
        profile.Children.Add(temp55);
        profile.Transforms.Add(profileScaling);
        temp54.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/avatarhome.png"));
        temp55.Width = 25f;
        temp55.Height = 25f;
        temp55.Alignment = Fuse.Elements.Alignment.TopRight;
        temp55.Margin = float4(5f, 5f, 5f, 5f);
        temp55.Fill = temp57;
        temp55.Children.Add(temp56);
        temp56.Value = "3";
        temp56.FontSize = 10f;
        temp56.TextColor = float4(1f, 1f, 1f, 1f);
        temp56.Alignment = Fuse.Elements.Alignment.Center;
        profileScaling.Factor = 0.1f;
        monthPanel.ColumnCount = 3;
        monthPanel.Padding = float4(20f, 0f, 20f, 0f);
        monthPanel.Opacity = 0f;
        monthPanel.Name = "monthPanel";
        monthPanel.Children.Add(temp58);
        monthPanel.Children.Add(temp59);
        monthPanel.Children.Add(temp60);
        temp58.Width = 20f;
        temp58.Height = 20f;
        temp58.Alignment = Fuse.Elements.Alignment.Left;
        temp58.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/arrow.png"));
        temp59.Value = "FEBRUARY";
        temp59.TextColor = float4(1f, 1f, 1f, 1f);
        temp59.Alignment = Fuse.Elements.Alignment.Center;
        temp60.Width = 20f;
        temp60.Height = 20f;
        temp60.Alignment = Fuse.Elements.Alignment.Right;
        temp60.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/arrow.png"));
        temp60.Transforms.Add(temp61);
        temp61.Degrees = 180f;
        loading.Animators.Add(temp62);
        loading.Animators.Add(temp63);
        loading.Animators.Add(temp64);
        loading.Animators.Add(temp65);
        temp62.Value = true;
        temp62.DelayBack = 0;
        temp63.Value = true;
        temp63.DelayBack = 0;
        temp64.Value = true;
        temp64.DelayBack = 0;
        temp64.Delay = 2.5;
        temp65.Value = true;
        temp65.Delay = 2.9;
        changeWidth.Animators.Add(temp66);
        temp66.Value = 60f;
        temp66.Easing = Fuse.Animations.Easing.CircularInOut;
        temp66.Duration = 0.5;
        temp66.DurationBack = 0;
        loadCircle.Animators.Add(temp67);
        loadCircle.Animators.Add(temp68);
        loadCircle.Animators.Add(temp69);
        loadCircle.Animators.Add(temp70);
        temp67.Value = 0f;
        temp67.Duration = 0.2;
        temp67.DurationBack = 0;
        temp68.Value = 1f;
        temp68.Duration = 0.3;
        temp68.DurationBack = 0;
        temp68.DelayBack = 0;
        temp68.Delay = 0.2;
        temp69.Frequency = 2;
        temp69.Target = loadingCircle;
        temp70.Low = 30f;
        temp70.High = 300f;
        temp70.Frequency = 0.7;
        scaleAndFade.Animators.Add(temp71);
        scaleAndFade.Animators.Add(temp72);
        scaleAndFade.Animators.Add(temp73);
        scaleAndFade.Animators.Add(temp74);
        temp71.Value = 0f;
        temp71.Duration = 0.1;
        temp72.Value = 1f;
        temp72.Duration = 0.01;
        temp73.Value = 0f;
        temp73.Duration = 0.01;
        temp74.Value = 1f;
        temp74.Easing = Fuse.Animations.Easing.ExponentialInOut;
        temp74.Duration = 0.7;
        temp74.DurationBack = 0;
        temp74.Delay = 0.01;
        showLoggedIn.Animators.Add(temp75);
        showLoggedIn.Animators.Add(temp76);
        showLoggedIn.Animators.Add(temp77);
        showLoggedIn.Animators.Add(temp78);
        showLoggedIn.Animators.Add(temp79);
        showLoggedIn.Animators.Add(temp80);
        showLoggedIn.Animators.Add(temp81);
        showLoggedIn.Animators.Add(temp83);
        showLoggedIn.Animators.Add(temp84);
        showLoggedIn.Animators.Add(temp85);
        showLoggedIn.Actions.Add(temp82);
        temp75.Value = 1f;
        temp75.Easing = Fuse.Animations.Easing.CubicInOut;
        temp75.Duration = 0.65;
        temp75.DurationBack = 0.35;
        temp75.DelayBack = 0.3;
        temp75.Delay = 0.1;
        temp76.Value = 1f;
        temp76.Duration = 0.3;
        temp76.Delay = 0.3;
        temp77.Value = 1f;
        temp77.Duration = 0.3;
        temp77.Delay = 0.3;
        temp78.Value = 0f;
        temp78.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp78.Duration = 0.8;
        temp79.Value = 1f;
        temp79.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp79.Duration = 0.8;
        temp80.Value = 1f;
        temp80.Easing = Fuse.Animations.Easing.CircularInOut;
        temp80.Duration = 0.6;
        temp81.Value = true;
        temp82.Name = "ToggleLoggedIn";
        temp83.Value = 1f;
        temp83.Duration = 0.1;
        temp83.DurationBack = 0.2;
        temp83.DelayBack = 0;
        temp83.Delay = 0.7;
        temp84.Value = 1f;
        temp84.Easing = Fuse.Animations.Easing.CircularInOut;
        temp84.Duration = 0.4;
        temp84.Delay = 0.7;
        temp85.Value = true;
        loggedOutView.RowData = "1.5*,auto,1*";
        loggedOutView.Padding = float4(40f, 0f, 40f, 0f);
        loggedOutView.Name = "loggedOutView";
        loggedOutView.Children.Add(temp86);
        loggedOutView.Children.Add(temp88);
        loggedOutView.Children.Add(temp96);
        loggedOutView.Children.Add(temp97);
        temp86.Layer = Fuse.Layer.Background;
        temp86.Fills.Add(temp87);
        temp87.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/bglogin.png"));
        temp88.Width = 150f;
        temp88.Height = 60f;
        global::Fuse.Controls.Grid.SetRow(temp88, 2);
        temp88.Children.Add(text);
        temp88.Children.Add(loadingCirclePanel);
        temp88.Children.Add(rectNormalScale);
        temp88.Children.Add(loadingButton);
        temp88.Behaviors.Add(temp91);
        text.Value = "Sign in";
        text.FontSize = 18f;
        text.TextColor = float4(1f, 1f, 1f, 1f);
        text.Alignment = Fuse.Elements.Alignment.Center;
        text.Name = "text";
        loadingCirclePanel.Name = "loadingCirclePanel";
        loadingCirclePanel.Children.Add(loadingCircle);
        loadingCircle.StartAngleDegrees = 0f;
        loadingCircle.LengthAngleDegrees = 90f;
        Fuse.Elements.Element.WidthProperty.Set(loadingCircle, 70f, global::Fuse.Elements.SizeUnit.Percent);
        Fuse.Elements.Element.HeightProperty.Set(loadingCircle, 70f, global::Fuse.Elements.SizeUnit.Percent);
        loadingCircle.Opacity = 0f;
        loadingCircle.Name = "loadingCircle";
        loadingCircle.Strokes.Add(temp89);
        temp89.Width = 1f;
        temp89.Brush = temp90;
        temp91.Actions.Add(temp92);
        temp92.Target = loading;
        rectNormalScale.CornerRadius = float4(30f, 30f, 30f, 30f);
        rectNormalScale.Width = 300f;
        rectNormalScale.Height = 60f;
        rectNormalScale.Name = "rectNormalScale";
        rectNormalScale.Fill = temp93;
        loadingButton.Width = 1320f;
        loadingButton.Height = 1320f;
        loadingButton.Alignment = Fuse.Elements.Alignment.Center;
        loadingButton.Opacity = 0f;
        loadingButton.Name = "loadingButton";
        loadingButton.Children.Add(temp94);
        loadingButton.Transforms.Add(loginButtonScaling);
        temp94.Fill = temp95;
        loginButtonScaling.Factor = 0.04545f;
        temp96.Margin = float4(40f, 40f, 40f, 40f);
        global::Fuse.Controls.Grid.SetRow(temp96, 0);
        temp96.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/marklogin.png"));
        temp97.RowCount = 2;
        temp97.Columns = "auto,1*";
        temp97.Height = 160f;
        temp97.Padding = float4(0f, 20f, 0f, 20f);
        global::Fuse.Controls.Grid.SetRow(temp97, 1);
        temp97.Children.Add(temp98);
        temp97.Children.Add(temp99);
        temp97.Children.Add(temp100);
        temp97.Children.Add(temp102);
        temp97.Children.Add(temp103);
        temp97.Children.Add(temp104);
        temp98.Width = 20f;
        temp98.Height = 20f;
        temp98.Alignment = Fuse.Elements.Alignment.Left;
        temp98.Margin = float4(10f, 0f, 30f, 0f);
        global::Fuse.Controls.Grid.SetRow(temp98, 0);
        global::Fuse.Controls.Grid.SetColumn(temp98, 0);
        temp98.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/user_icon.png"));
        temp99.Value = "Username";
        temp99.TextColor = float4(1f, 1f, 1f, 1f);
        temp99.Alignment = Fuse.Elements.Alignment.CenterLeft;
        global::Fuse.Controls.Grid.SetRow(temp99, 0);
        global::Fuse.Controls.Grid.SetColumn(temp99, 1);
        temp100.Height = 1f;
        temp100.Alignment = Fuse.Elements.Alignment.Bottom;
        global::Fuse.Controls.Grid.SetRow(temp100, 0);
        global::Fuse.Controls.Grid.SetColumn(temp100, 0);
        global::Fuse.Controls.Grid.SetColumnSpan(temp100, 2);
        temp100.Fill = temp101;
        temp102.Width = 20f;
        temp102.Height = 20f;
        temp102.Alignment = Fuse.Elements.Alignment.Left;
        temp102.Margin = float4(10f, 0f, 30f, 0f);
        global::Fuse.Controls.Grid.SetRow(temp102, 1);
        global::Fuse.Controls.Grid.SetColumn(temp102, 0);
        temp102.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/password_icon.png"));
        temp103.Value = "Password";
        temp103.TextColor = float4(1f, 1f, 1f, 1f);
        temp103.Alignment = Fuse.Elements.Alignment.CenterLeft;
        global::Fuse.Controls.Grid.SetRow(temp103, 1);
        global::Fuse.Controls.Grid.SetColumn(temp103, 1);
        temp104.Height = 1f;
        temp104.Alignment = Fuse.Elements.Alignment.Bottom;
        global::Fuse.Controls.Grid.SetRow(temp104, 1);
        global::Fuse.Controls.Grid.SetColumn(temp104, 0);
        global::Fuse.Controls.Grid.SetColumnSpan(temp104, 2);
        temp104.Fill = temp105;
        this.RootNode = temp4;
        this.Theme = Fuse.BasicTheme.BasicTheme.Singleton;
    }
}
