public partial class AppName: Fuse.Controls.Text
{
    static AppName()
    {
    }
    public AppName()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        this.FontSize = 25f;
        this.Font = global::MainView.seasrn;
    }
}
public partial class MainView: Fuse.App
{
    public sealed class Fuse_Controls_PageControl_Fuse_Node_Active_Property: Uno.UX.Property<Fuse.Node>
    {
        Fuse.Controls.PageControl _obj;
        public Fuse_Controls_PageControl_Fuse_Node_Active_Property(Fuse.Controls.PageControl obj) { _obj = obj; }
        protected override Fuse.Node OnGet() { return _obj.Active; }
        protected override void OnSet(Fuse.Node v, object origin) { _obj.Active = v; }
    }
    public sealed class Fuse_Reactive_Each_object_Items_Property: Uno.UX.Property<object>
    {
        Fuse.Reactive.Each _obj;
        public Fuse_Reactive_Each_object_Items_Property(Fuse.Reactive.Each obj) { _obj = obj; }
        protected override object OnGet() { return _obj.Items; }
        protected override void OnSet(object v, object origin) { _obj.Items = v; }
    }
    public sealed class Fuse_Controls_TextControl_string_Value_Property: Uno.UX.Property<string>
    {
        Fuse.Controls.TextControl _obj;
        public Fuse_Controls_TextControl_string_Value_Property(Fuse.Controls.TextControl obj) { _obj = obj; }
        protected override string OnGet() { return _obj.Value; }
        protected override void OnSet(string v, object origin) { _obj.SetValue(v, origin); }
        protected override void OnAddListener(Uno.UX.ValueChangedHandler<string> listener) { _obj.ValueChanged += listener; }
        protected override void OnRemoveListener(Uno.UX.ValueChangedHandler<string> listener) { _obj.ValueChanged -= listener; }
    }
    public partial class Factory: Uno.UX.IFactory
    {
        internal readonly MainView __parent;
        public Factory(MainView parent)
        {
            __parent = parent;
        }
        MainView.Fuse_Controls_TextControl_string_Value_Property temp_Value_inst;
        internal Fuse.Reactive.EventBinding temp_eb0;
        static Factory()
        {
        }
        public object New()
        {
            var self = new Fuse.Controls.Rectangle();
            var temp = new Fuse.Controls.Text();
            temp_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp);
            var temp1 = new Fuse.Reactive.DataBinding<string>(temp_Value_inst, "name");
            var temp2 = new Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
            var temp_eb0 = new Fuse.Reactive.EventBinding("clickHandler");
            self.CornerRadius = float4(5f, 5f, 5f, 5f);
            self.Width = 150f;
            self.Height = 35f;
            self.Margin = float4(0f, 0f, 0f, 10f);
            self.Opacity = 0.7f;
            global::Fuse.Gestures.Clicked.AddHandler(self, temp_eb0.OnEvent);
            temp.TextColor = float4(0f, 0f, 0f, 1f);
            temp.Alignment = Fuse.Elements.Alignment.Center;
            temp.Behaviors.Add(temp1);
            self.Fill = temp2;
            self.Children.Add(temp);
            self.Behaviors.Add(temp_eb0);
            return self;
        }
    }
    public partial class Factory1: Uno.UX.IFactory
    {
        internal readonly MainView __parent;
        public Factory1(MainView parent)
        {
            __parent = parent;
        }
        MainView.Fuse_Controls_TextControl_string_Value_Property temp_Value_inst;
        MainView.Fuse_Controls_TextControl_string_Value_Property temp1_Value_inst;
        MainView.Fuse_Controls_TextControl_string_Value_Property temp2_Value_inst;
        internal Fuse.Reactive.EventBinding temp_eb1;
        static Factory1()
        {
        }
        public object New()
        {
            var self = new Fuse.Controls.Rectangle();
            var temp = new Fuse.Controls.Text();
            temp_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp);
            var temp1 = new Fuse.Controls.Text();
            temp1_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp1);
            var temp2 = new Fuse.Controls.Text();
            temp2_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp2);
            var temp3 = new Fuse.Controls.StackPanel();
            var temp4 = new Fuse.Reactive.DataBinding<string>(temp_Value_inst, "name");
            var temp5 = new Fuse.Reactive.DataBinding<string>(temp1_Value_inst, "address");
            var temp6 = new Fuse.Reactive.DataBinding<string>(temp2_Value_inst, "number");
            var temp7 = new Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
            var temp_eb1 = new Fuse.Reactive.EventBinding("clickHandler");
            self.CornerRadius = float4(5f, 5f, 5f, 5f);
            self.Width = 150f;
            self.Height = 35f;
            self.Margin = float4(0f, 0f, 0f, 10f);
            self.Opacity = 0.7f;
            global::Fuse.Gestures.Clicked.AddHandler(self, temp_eb1.OnEvent);
            temp3.Margin = float4(7f, 3f, 7f, 7f);
            temp3.Children.Add(temp);
            temp3.Children.Add(temp1);
            temp3.Children.Add(temp2);
            temp.FontSize = 16f;
            temp.Behaviors.Add(temp4);
            temp1.FontSize = 16f;
            temp1.TextColor = float4(0.6f, 0.6f, 0.6f, 1f);
            temp1.Behaviors.Add(temp5);
            temp2.TextWrapping = Fuse.Elements.TextWrapping.Wrap;
            temp2.FontSize = 14f;
            temp2.TextColor = float4(0f, 0f, 0f, 1f);
            temp2.Behaviors.Add(temp6);
            self.Fill = temp7;
            self.Children.Add(temp3);
            self.Behaviors.Add(temp_eb1);
            return self;
        }
    }
    MainView.Fuse_Reactive_Each_object_Items_Property temp_Items_inst;
    MainView.Fuse_Reactive_Each_object_Items_Property temp1_Items_inst;
    MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property temp2_Active_inst;
    [global::Uno.UX.UXGlobalResource("seasrn")] public static readonly Fuse.Font seasrn;
    internal Fuse.Controls.Page page1;
    internal Fuse.Controls.Grid loggedOutView;
    internal Fuse.Controls.Page Page2;
    static MainView()
    {
        seasrn = new Fuse.Font(new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/fonts/SEASRN__.ttf")));
        global::Uno.UX.Resource.SetGlobalKey(seasrn, "seasrn");
    }
    public MainView()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        var temp = new Fuse.Reactive.Each();
        temp_Items_inst = new MainView.Fuse_Reactive_Each_object_Items_Property(temp);
        var temp1 = new Fuse.Reactive.Each();
        temp1_Items_inst = new MainView.Fuse_Reactive_Each_object_Items_Property(temp1);
        var temp2 = new Fuse.Controls.PageControl();
        temp2_Active_inst = new MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property(temp2);
        var temp3 = new Fuse.Reactive.JavaScript();
        page1 = new Fuse.Controls.Page();
        loggedOutView = new Fuse.Controls.Grid();
        var temp4 = new Fuse.Controls.Rectangle();
        var temp5 = new Fuse.Drawing.ImageFill();
        var temp6 = new Fuse.Drawing.ImageFill();
        var temp7 = new Fuse.Controls.Grid();
        var temp8 = new AppName();
        var temp9 = new Fuse.Controls.Circle();
        var temp10 = new Fuse.Drawing.ImageFill();
        var temp11 = new AppName();
        var temp12 = new Fuse.Controls.Text();
        var temp13 = new Fuse.Controls.ScrollView();
        var temp14 = new Fuse.Controls.StackPanel();
        var temp15 = new Factory(this);
        var temp16 = new Fuse.Reactive.DataBinding<object>(temp_Items_inst, "categories");
        Page2 = new Fuse.Controls.Page();
        var temp17 = new Fuse.Controls.Grid();
        var temp18 = new Fuse.Controls.Rectangle();
        var temp19 = new Fuse.Drawing.ImageFill();
        var temp20 = new Fuse.Drawing.ImageFill();
        var temp21 = new Fuse.Controls.ScrollView();
        var temp22 = new Fuse.Controls.StackPanel();
        var temp23 = new Factory1(this);
        var temp24 = new Fuse.Reactive.DataBinding<object>(temp1_Items_inst, "contacts");
        var temp25 = new Fuse.Reactive.DataBinding<Fuse.Node>(temp2_Active_inst, "currentPage");
        temp3.LineNumber = 4;
        temp3.FileName = "MainView.ux";
        temp3.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../MainView.js"));
        temp2.Children.Add(page1);
        temp2.Children.Add(Page2);
        temp2.Behaviors.Add(temp25);
        page1.Name = "page1";
        page1.Children.Add(loggedOutView);
        loggedOutView.RowData = "1*,0.5*,2.5*";
        loggedOutView.Padding = float4(10f, 0f, 10f, 0f);
        loggedOutView.Name = "loggedOutView";
        loggedOutView.Children.Add(temp4);
        loggedOutView.Children.Add(temp7);
        loggedOutView.Children.Add(temp12);
        loggedOutView.Children.Add(temp13);
        temp4.Layer = Fuse.Layer.Background;
        temp4.Fills.Add(temp5);
        temp4.Fills.Add(temp6);
        temp5.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/phone.jpg"));
        temp6.Opacity = 0.3f;
        temp6.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/overlay.jpg"));
        temp7.RowCount = 1;
        temp7.ColumnCount = 3;
        global::Fuse.Controls.Grid.SetRow(temp7, 0);
        temp7.Children.Add(temp8);
        temp7.Children.Add(temp9);
        temp7.Children.Add(temp11);
        temp8.Value = "Call";
        temp8.TextColor = float4(1f, 1f, 1f, 1f);
        temp8.Margin = float4(0f, 35f, 0f, 0f);
        global::Fuse.Controls.Grid.SetColumn(temp8, 0);
        temp9.Width = 75f;
        temp9.Height = 75f;
        global::Fuse.Controls.Grid.SetColumn(temp9, 1);
        temp9.Fills.Add(temp10);
        temp10.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/CH-Icon.jpg"));
        temp11.Value = "Hero";
        temp11.Margin = float4(5f, 35f, 0f, 0f);
        global::Fuse.Controls.Grid.SetColumn(temp11, 2);
        temp12.Value = "Available Categories";
        temp12.FontSize = 20f;
        temp12.Alignment = Fuse.Elements.Alignment.Center;
        global::Fuse.Controls.Grid.SetRow(temp12, 1);
        temp13.Margin = float4(0f, 0f, 0f, 10f);
        global::Fuse.Controls.Grid.SetRow(temp13, 2);
        temp13.Content = temp14;
        temp14.Behaviors.Add(temp16);
        temp14.Behaviors.Add(temp);
        temp.Factories.Add(temp15);
        Page2.Name = "Page2";
        Page2.Children.Add(temp17);
        temp17.RowData = "0.5*,2.5*,0.5*";
        temp17.Children.Add(temp18);
        temp17.Children.Add(temp21);
        temp18.Layer = Fuse.Layer.Background;
        temp18.Fills.Add(temp19);
        temp18.Fills.Add(temp20);
        temp19.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/background.jpg"));
        temp20.Opacity = 0.4f;
        temp20.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/overlay.jpg"));
        temp21.Margin = float4(0f, 0f, 0f, 10f);
        global::Fuse.Controls.Grid.SetRow(temp21, 1);
        temp21.Content = temp22;
        temp22.Behaviors.Add(temp24);
        temp22.Behaviors.Add(temp1);
        temp1.Factories.Add(temp23);
        this.RootNode = temp2;
        this.Theme = Fuse.BasicTheme.BasicTheme.Singleton;
        this.Behaviors.Add(temp3);
    }
}
