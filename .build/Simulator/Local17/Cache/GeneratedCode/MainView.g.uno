public partial class MainView: Fuse.App
{
    public sealed class Fuse_Controls_PageControl_Fuse_Node_Active_Property: Uno.UX.Property<Fuse.Node>
    {
        Fuse.Controls.PageControl _obj;
        public Fuse_Controls_PageControl_Fuse_Node_Active_Property(Fuse.Controls.PageControl obj) { _obj = obj; }
        protected override Fuse.Node OnGet() { return _obj.Active; }
        protected override void OnSet(Fuse.Node v, object origin) { _obj.Active = v; }
    }
    public sealed class Fuse_Reactive_Each_object_Items_Property: Uno.UX.Property<object>
    {
        Fuse.Reactive.Each _obj;
        public Fuse_Reactive_Each_object_Items_Property(Fuse.Reactive.Each obj) { _obj = obj; }
        protected override object OnGet() { return _obj.Items; }
        protected override void OnSet(object v, object origin) { _obj.Items = v; }
    }
    public sealed class Fuse_Controls_Button_string_Text_Property: Uno.UX.Property<string>
    {
        Fuse.Controls.Button _obj;
        public Fuse_Controls_Button_string_Text_Property(Fuse.Controls.Button obj) { _obj = obj; }
        protected override string OnGet() { return _obj.Text; }
        protected override void OnSet(string v, object origin) { _obj.SetText(v, origin); }
        protected override void OnAddListener(Uno.UX.ValueChangedHandler<string> listener) { _obj.TextChanged += listener; }
        protected override void OnRemoveListener(Uno.UX.ValueChangedHandler<string> listener) { _obj.TextChanged -= listener; }
    }
    public partial class Factory: Uno.UX.IFactory
    {
        internal readonly MainView __parent;
        public Factory(MainView parent)
        {
            __parent = parent;
        }
        MainView.Fuse_Controls_Button_string_Text_Property self_Text_inst;
        static Factory()
        {
        }
        public object New()
        {
            var self = new Fuse.Controls.Button();
            self_Text_inst = new MainView.Fuse_Controls_Button_string_Text_Property(self);
            var temp = new Fuse.Reactive.DataBinding<string>(self_Text_inst, "name");
            self.Behaviors.Add(temp);
            return self;
        }
    }
    MainView.Fuse_Reactive_Each_object_Items_Property temp_Items_inst;
    MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property temp1_Active_inst;
    internal Fuse.Controls.Page page1;
    internal Fuse.Controls.Grid loggedOutView;
    internal Fuse.Reactive.EventBinding temp_eb0;
    internal Fuse.Controls.Page Page2;
    static MainView()
    {
    }
    public MainView()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        var temp = new Fuse.Reactive.Each();
        temp_Items_inst = new MainView.Fuse_Reactive_Each_object_Items_Property(temp);
        var temp1 = new Fuse.Controls.PageControl();
        temp1_Active_inst = new MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property(temp1);
        var temp2 = new Fuse.Reactive.JavaScript();
        page1 = new Fuse.Controls.Page();
        loggedOutView = new Fuse.Controls.Grid();
        var temp3 = new Fuse.Controls.Rectangle();
        var temp4 = new Fuse.Drawing.ImageFill();
        var temp5 = new Fuse.Controls.Grid();
        var temp6 = new Fuse.Controls.Text();
        var temp7 = new Fuse.Controls.Circle();
        var temp8 = new Fuse.Controls.Text();
        var temp9 = new Fuse.Drawing.ImageFill();
        temp_eb0 = new Fuse.Reactive.EventBinding("clickHandler");
        var temp10 = new Fuse.Controls.Text();
        var temp11 = new Fuse.Controls.ScrollView();
        var temp12 = new Fuse.Controls.StackPanel();
        var temp13 = new Factory(this);
        var temp14 = new Fuse.Reactive.DataBinding<object>(temp_Items_inst, "categories");
        Page2 = new Fuse.Controls.Page();
        var temp15 = new Fuse.Controls.Rectangle();
        var temp16 = new Fuse.Drawing.ImageFill();
        var temp17 = new Fuse.Reactive.DataBinding<Fuse.Node>(temp1_Active_inst, "currentPage");
        temp2.LineNumber = 4;
        temp2.FileName = "MainView.ux";
        temp2.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../MainView.js"));
        temp1.Children.Add(page1);
        temp1.Children.Add(Page2);
        temp1.Behaviors.Add(temp17);
        page1.Name = "page1";
        page1.Children.Add(loggedOutView);
        loggedOutView.RowData = "1*,0.5*,2.5*";
        loggedOutView.Padding = float4(40f, 0f, 40f, 0f);
        loggedOutView.Name = "loggedOutView";
        loggedOutView.Children.Add(temp3);
        loggedOutView.Children.Add(temp5);
        loggedOutView.Children.Add(temp10);
        loggedOutView.Children.Add(temp11);
        temp3.Layer = Fuse.Layer.Background;
        temp3.Fills.Add(temp4);
        temp4.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/phone.jpg"));
        temp5.RowCount = 1;
        temp5.ColumnCount = 3;
        global::Fuse.Controls.Grid.SetRow(temp5, 0);
        temp5.Children.Add(temp6);
        temp5.Children.Add(temp7);
        temp6.Value = "Call";
        global::Fuse.Controls.Grid.SetColumn(temp6, 0);
        temp7.Width = 75f;
        temp7.Height = 75f;
        global::Fuse.Controls.Grid.SetColumn(temp7, 1);
        global::Fuse.Gestures.Clicked.AddHandler(temp7, temp_eb0.OnEvent);
        temp7.Fills.Add(temp9);
        temp7.Children.Add(temp8);
        temp7.Behaviors.Add(temp_eb0);
        temp8.Value = "Hero";
        global::Fuse.Controls.Grid.SetColumn(temp8, 2);
        temp9.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/CH-Icon.jpg"));
        temp10.Value = "Available Categories";
        temp10.Alignment = Fuse.Elements.Alignment.Center;
        global::Fuse.Controls.Grid.SetRow(temp10, 1);
        temp11.Margin = float4(0f, 0f, 0f, 10f);
        global::Fuse.Controls.Grid.SetRow(temp11, 2);
        temp11.Content = temp12;
        temp12.Behaviors.Add(temp14);
        temp12.Behaviors.Add(temp);
        temp.Factories.Add(temp13);
        Page2.Name = "Page2";
        Page2.Children.Add(temp15);
        temp15.Layer = Fuse.Layer.Background;
        temp15.Fills.Add(temp16);
        temp16.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/background.jpg"));
        this.RootNode = temp1;
        this.Theme = Fuse.BasicTheme.BasicTheme.Singleton;
        this.Behaviors.Add(temp2);
    }
}
