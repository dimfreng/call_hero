public partial class MainView: Fuse.App
{
    public sealed class Fuse_Controls_Rectangle_float4_CornerRadius_Property: Uno.UX.Property<float4>
    {
        Fuse.Controls.Rectangle _obj;
        public Fuse_Controls_Rectangle_float4_CornerRadius_Property(Fuse.Controls.Rectangle obj) { _obj = obj; }
        protected override float4 OnGet() { return _obj.CornerRadius; }
        protected override void OnSet(float4 v, object origin) { _obj.CornerRadius = v; }
    }
    public partial class Template: Uno.UX.Template<Fuse.Controls.Rectangle>
    {
        internal readonly MainView __parent;
        public Template(MainView parent)
        {
            __parent = parent;
        }
        static Template()
        {
        }
        protected override void OnApply(Fuse.Controls.Rectangle self)
        {
            Fuse.Elements.Element.WidthProperty.SetStyle(self, 100f);
            Fuse.Elements.Element.HeightProperty.SetStyle(self, 100f);
            Fuse.Elements.Element.MarginProperty.SetStyle(self, float4(10f, 10f, 10f, 10f));
            Fuse.Controls.Shape.FillProperty.SetStyle(self, Fuse.Drawing.Brushes.White);
        }
    }
    MainView.Fuse_Controls_Rectangle_float4_CornerRadius_Property fillRectB_CornerRadius_inst;
    internal Fuse.Controls.Rectangle fillRectB;
    static MainView()
    {
    }
    public MainView()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        fillRectB = new Fuse.Controls.Rectangle();
        fillRectB_CornerRadius_inst = new MainView.Fuse_Controls_Rectangle_float4_CornerRadius_Property(fillRectB);
        var temp = new Fuse.Controls.Panel();
        var temp1 = new Fuse.Controls.StackPanel();
        var temp2 = new Fuse.Style();
        var temp3 = new Template(this) { Cascade = true, AffectSubtypes = true };
        var temp4 = new Fuse.Controls.Rectangle();
        var temp5 = new Fuse.Gestures.Tapped();
        var temp6 = new Fuse.Animations.Scale();
        var temp7 = new Fuse.Animations.Move();
        var temp8 = new Fuse.Gestures.WhilePressed();
        var temp9 = new Fuse.Animations.Rotate();
        var temp10 = new Fuse.Animations.Change<float4>(fillRectB_CornerRadius_inst);
        var temp11 = new Fuse.Drawing.StaticSolidColor(float4(1f, 0.5568628f, 0.7058824f, 1f));
        temp.Background = temp11;
        temp.Children.Add(temp1);
        temp1.Orientation = Fuse.Layouts.Orientation.Horizontal;
        temp1.Alignment = Fuse.Elements.Alignment.Center;
        temp1.Children.Add(temp4);
        temp1.Children.Add(fillRectB);
        temp1.Styles.Add(temp2);
        temp2.Templates.Add(temp3);
        temp4.CornerRadius = float4(5f, 5f, 5f, 5f);
        temp4.Behaviors.Add(temp5);
        temp5.Animators.Add(temp6);
        temp5.Animators.Add(temp7);
        temp6.Factor = 0.8f;
        temp6.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp6.Duration = 0.3;
        temp7.Y = 0.3f;
        temp7.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp7.Duration = 0.3;
        temp7.RelativeTo = Fuse.TranslationModes.Size;
        fillRectB.CornerRadius = float4(50f, 50f, 50f, 50f);
        fillRectB.Name = "fillRectB";
        fillRectB.Behaviors.Add(temp8);
        temp8.Animators.Add(temp9);
        temp8.Animators.Add(temp10);
        temp9.Degrees = 180f;
        temp9.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp9.Duration = 0.5;
        temp10.Value = float4(5f, 5f, 5f, 5f);
        temp10.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp10.Duration = 0.5;
        this.RootNode = temp;
        this.Theme = Fuse.BasicTheme.BasicTheme.Singleton;
    }
}
