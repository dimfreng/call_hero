public partial class MainView: Fuse.App
{
    internal Fuse.Controls.Grid loggedOutView;
    static MainView()
    {
    }
    public MainView()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        loggedOutView = new Fuse.Controls.Grid();
        var temp = new Fuse.Controls.Image();
        loggedOutView.RowData = "1.5*,auto,1*";
        loggedOutView.Padding = float4(40f, 0f, 40f, 0f);
        loggedOutView.Name = "loggedOutView";
        loggedOutView.Children.Add(temp);
        temp.Margin = float4(40f, 40f, 40f, 40f);
        global::Fuse.Controls.Grid.SetRow(temp, 0);
        temp.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/marklogin.png"));
        this.RootNode = loggedOutView;
        this.Theme = Fuse.BasicTheme.BasicTheme.Singleton;
    }
}
