public partial class MainView: Fuse.App
{
    public sealed class Fuse_Controls_PageControl_Fuse_Node_Active_Property: Uno.UX.Property<Fuse.Node>
    {
        Fuse.Controls.PageControl _obj;
        public Fuse_Controls_PageControl_Fuse_Node_Active_Property(Fuse.Controls.PageControl obj) { _obj = obj; }
        protected override Fuse.Node OnGet() { return _obj.Active; }
        protected override void OnSet(Fuse.Node v, object origin) { _obj.Active = v; }
    }
    MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property temp_Active_inst;
    internal Fuse.Controls.Page page1;
    internal Fuse.Controls.Grid loggedOutView;
    internal Fuse.Reactive.EventBinding temp_eb0;
    internal Fuse.Controls.Page Page2;
    static MainView()
    {
    }
    public MainView()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        var temp = new Fuse.Controls.PageControl();
        temp_Active_inst = new MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property(temp);
        var temp1 = new Fuse.Reactive.JavaScript();
        page1 = new Fuse.Controls.Page();
        loggedOutView = new Fuse.Controls.Grid();
        var temp2 = new Fuse.Controls.Rectangle();
        var temp3 = new Fuse.Drawing.ImageFill();
        var temp4 = new Fuse.Controls.Image();
        temp_eb0 = new Fuse.Reactive.EventBinding("clickHandler");
        Page2 = new Fuse.Controls.Page();
        var temp5 = new Fuse.Reactive.DataBinding<Fuse.Node>(temp_Active_inst, "currentPage");
        temp1.LineNumber = 3;
        temp1.FileName = "MainView.ux";
        temp1.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../MainView.js"));
        temp.Children.Add(page1);
        temp.Children.Add(Page2);
        temp.Behaviors.Add(temp5);
        page1.Name = "page1";
        page1.Children.Add(loggedOutView);
        loggedOutView.RowData = "1.5*,auto,1*";
        loggedOutView.Padding = float4(40f, 0f, 40f, 0f);
        loggedOutView.Name = "loggedOutView";
        loggedOutView.Children.Add(temp2);
        loggedOutView.Children.Add(temp4);
        temp2.Layer = Fuse.Layer.Background;
        temp2.Fills.Add(temp3);
        temp3.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/bglogin.png"));
        temp4.Margin = float4(40f, 40f, 40f, 40f);
        global::Fuse.Controls.Grid.SetRow(temp4, 0);
        global::Fuse.Gestures.Clicked.AddHandler(temp4, temp_eb0.OnEvent);
        temp4.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/marklogin.png"));
        temp4.Behaviors.Add(temp_eb0);
        Page2.Name = "Page2";
        Page2.Background = Fuse.Drawing.Brushes.Blue;
        this.RootNode = temp;
        this.Theme = Fuse.BasicTheme.BasicTheme.Singleton;
        this.Behaviors.Add(temp1);
    }
}
