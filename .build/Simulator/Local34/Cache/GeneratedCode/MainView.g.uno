public partial class AppName: Fuse.Controls.Text
{
    static AppName()
    {
    }
    public AppName()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        this.FontSize = 25f;
        this.Font = global::MainView.seasrn;
    }
}
public partial class MainView: Fuse.App
{
    public sealed class Fuse_Controls_PageControl_Fuse_Node_Active_Property: Uno.UX.Property<Fuse.Node>
    {
        Fuse.Controls.PageControl _obj;
        public Fuse_Controls_PageControl_Fuse_Node_Active_Property(Fuse.Controls.PageControl obj) { _obj = obj; }
        protected override Fuse.Node OnGet() { return _obj.Active; }
        protected override void OnSet(Fuse.Node v, object origin) { _obj.Active = v; }
    }
    public sealed class Fuse_Reactive_Each_object_Items_Property: Uno.UX.Property<object>
    {
        Fuse.Reactive.Each _obj;
        public Fuse_Reactive_Each_object_Items_Property(Fuse.Reactive.Each obj) { _obj = obj; }
        protected override object OnGet() { return _obj.Items; }
        protected override void OnSet(object v, object origin) { _obj.Items = v; }
    }
    public sealed class Fuse_Controls_TextControl_string_Value_Property: Uno.UX.Property<string>
    {
        Fuse.Controls.TextControl _obj;
        public Fuse_Controls_TextControl_string_Value_Property(Fuse.Controls.TextControl obj) { _obj = obj; }
        protected override string OnGet() { return _obj.Value; }
        protected override void OnSet(string v, object origin) { _obj.SetValue(v, origin); }
        protected override void OnAddListener(Uno.UX.ValueChangedHandler<string> listener) { _obj.ValueChanged += listener; }
        protected override void OnRemoveListener(Uno.UX.ValueChangedHandler<string> listener) { _obj.ValueChanged -= listener; }
    }
    public partial class Factory: Uno.UX.IFactory
    {
        internal readonly MainView __parent;
        public Factory(MainView parent)
        {
            __parent = parent;
        }
        MainView.Fuse_Controls_TextControl_string_Value_Property temp_Value_inst;
        internal Fuse.Reactive.EventBinding temp_eb0;
        static Factory()
        {
        }
        public object New()
        {
            var self = new Fuse.Controls.Rectangle();
            var temp = new Fuse.Controls.Text();
            temp_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp);
            var temp1 = new Fuse.Reactive.DataBinding<string>(temp_Value_inst, "name");
            var temp2 = new Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
            var temp_eb0 = new Fuse.Reactive.EventBinding("selectedCategory");
            self.CornerRadius = float4(5f, 5f, 5f, 5f);
            self.Width = 150f;
            self.Height = 35f;
            self.Margin = float4(0f, 0f, 0f, 10f);
            self.Opacity = 0.7f;
            global::Fuse.Gestures.Clicked.AddHandler(self, temp_eb0.OnEvent);
            temp.TextColor = float4(0f, 0f, 0f, 1f);
            temp.Alignment = Fuse.Elements.Alignment.Center;
            temp.Behaviors.Add(temp1);
            self.Fill = temp2;
            self.Children.Add(temp);
            self.Behaviors.Add(temp_eb0);
            return self;
        }
    }
    public partial class Factory1: Uno.UX.IFactory
    {
        internal readonly MainView __parent;
        public Factory1(MainView parent)
        {
            __parent = parent;
        }
        MainView.Fuse_Controls_TextControl_string_Value_Property temp_Value_inst;
        MainView.Fuse_Controls_TextControl_string_Value_Property temp1_Value_inst;
        MainView.Fuse_Controls_TextControl_string_Value_Property temp2_Value_inst;
        internal Fuse.Reactive.EventBinding temp_eb2;
        static Factory1()
        {
        }
        public object New()
        {
            var self = new Fuse.Controls.Rectangle();
            var temp = new Fuse.Controls.Text();
            temp_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp);
            var temp1 = new Fuse.Controls.Text();
            temp1_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp1);
            var temp2 = new Fuse.Controls.Text();
            temp2_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp2);
            var temp3 = new Fuse.Controls.Grid();
            var temp4 = new Fuse.Controls.StackPanel();
            var temp5 = new Fuse.Controls.ScrollView();
            var temp6 = new Fuse.Reactive.DataBinding<string>(temp_Value_inst, "name");
            var temp7 = new Fuse.Controls.ScrollView();
            var temp8 = new Fuse.Reactive.DataBinding<string>(temp1_Value_inst, "address");
            var temp9 = new Fuse.Controls.ScrollView();
            var temp10 = new Fuse.Reactive.DataBinding<string>(temp2_Value_inst, "number");
            var temp11 = new Fuse.Controls.Rectangle();
            var temp12 = new Fuse.Drawing.ImageFill();
            var temp13 = new Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
            var temp_eb2 = new Fuse.Reactive.EventBinding("callNumber");
            self.Width = 220f;
            self.Height = 60f;
            self.Margin = float4(0f, 0f, 0f, 10f);
            self.Opacity = 0.7f;
            global::Fuse.Gestures.Clicked.AddHandler(self, temp_eb2.OnEvent);
            temp3.RowCount = 1;
            temp3.ColumnCount = 2;
            temp3.Children.Add(temp4);
            temp3.Children.Add(temp11);
            temp4.Width = 150f;
            temp4.Margin = float4(10f, 3f, 0f, 7f);
            global::Fuse.Controls.Grid.SetRow(temp4, 0);
            global::Fuse.Controls.Grid.SetColumn(temp4, 0);
            temp4.Children.Add(temp5);
            temp4.Children.Add(temp7);
            temp4.Children.Add(temp9);
            temp5.AllowedScrollDirections = Fuse.Gestures.ScrollDirections.Horizontal;
            temp5.Content = temp;
            temp.FontSize = 16f;
            temp.Behaviors.Add(temp6);
            temp7.AllowedScrollDirections = Fuse.Gestures.ScrollDirections.Horizontal;
            temp7.Content = temp1;
            temp1.FontSize = 16f;
            temp1.TextColor = float4(0.6f, 0.6f, 0.6f, 1f);
            temp1.Behaviors.Add(temp8);
            temp9.AllowedScrollDirections = Fuse.Gestures.ScrollDirections.Horizontal;
            temp9.Content = temp2;
            temp2.FontSize = 14f;
            temp2.TextColor = float4(0f, 0f, 0f, 1f);
            temp2.Behaviors.Add(temp10);
            temp11.Width = 40f;
            temp11.Height = 40f;
            temp11.Margin = float4(30f, 0f, 0f, 0f);
            global::Fuse.Controls.Grid.SetRow(temp11, 0);
            global::Fuse.Controls.Grid.SetColumn(temp11, 1);
            temp11.Fills.Add(temp12);
            temp12.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/Phone-icon.png"));
            self.Fill = temp13;
            self.Children.Add(temp3);
            self.Behaviors.Add(temp_eb2);
            return self;
        }
    }
    MainView.Fuse_Reactive_Each_object_Items_Property temp_Items_inst;
    MainView.Fuse_Controls_TextControl_string_Value_Property temp1_Value_inst;
    MainView.Fuse_Reactive_Each_object_Items_Property temp2_Items_inst;
    MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property temp3_Active_inst;
    [global::Uno.UX.UXGlobalResource("seasrn")] public static readonly Fuse.Font seasrn;
    internal Fuse.Controls.Page Page1;
    internal Fuse.Controls.Grid loggedOutView;
    internal Fuse.Controls.Page Page2;
    internal Fuse.Reactive.EventBinding temp_eb1;
    static MainView()
    {
        seasrn = new Fuse.Font(new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/fonts/SEASRN__.ttf")));
        global::Uno.UX.Resource.SetGlobalKey(seasrn, "seasrn");
    }
    public MainView()
    {
        InitializeUX();
    }
    internal void InitializeUX()
    {
        var temp = new Fuse.Reactive.Each();
        temp_Items_inst = new MainView.Fuse_Reactive_Each_object_Items_Property(temp);
        var temp1 = new Fuse.Controls.Text();
        temp1_Value_inst = new MainView.Fuse_Controls_TextControl_string_Value_Property(temp1);
        var temp2 = new Fuse.Reactive.Each();
        temp2_Items_inst = new MainView.Fuse_Reactive_Each_object_Items_Property(temp2);
        var temp3 = new Fuse.Controls.PageControl();
        temp3_Active_inst = new MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property(temp3);
        var temp4 = new Fuse.Reactive.JavaScript();
        Page1 = new Fuse.Controls.Page();
        loggedOutView = new Fuse.Controls.Grid();
        var temp5 = new Fuse.Controls.Rectangle();
        var temp6 = new Fuse.Drawing.ImageFill();
        var temp7 = new Fuse.Drawing.ImageFill();
        var temp8 = new Fuse.Controls.Grid();
        var temp9 = new AppName();
        var temp10 = new Fuse.Controls.Circle();
        var temp11 = new Fuse.Drawing.ImageFill();
        var temp12 = new AppName();
        var temp13 = new Fuse.Controls.Text();
        var temp14 = new Fuse.Controls.ScrollView();
        var temp15 = new Fuse.Controls.StackPanel();
        var temp16 = new Factory(this);
        var temp17 = new Fuse.Reactive.DataBinding<object>(temp_Items_inst, "categories");
        Page2 = new Fuse.Controls.Page();
        var temp18 = new Fuse.Controls.Grid();
        var temp19 = new Fuse.Controls.Rectangle();
        var temp20 = new Fuse.Drawing.ImageFill();
        var temp21 = new Fuse.Drawing.ImageFill();
        var temp22 = new Fuse.Controls.Grid();
        var temp23 = new Fuse.Controls.Rectangle();
        var temp24 = new Fuse.Drawing.StaticSolidColor(float4(0f, 0f, 0f, 1f));
        var temp25 = new Fuse.Controls.Image();
        temp_eb1 = new Fuse.Reactive.EventBinding("backButton");
        var temp26 = new Fuse.Reactive.DataBinding<string>(temp1_Value_inst, "selected");
        var temp27 = new Fuse.Controls.ScrollView();
        var temp28 = new Fuse.Controls.StackPanel();
        var temp29 = new Factory1(this);
        var temp30 = new Fuse.Reactive.DataBinding<object>(temp2_Items_inst, "contacts");
        var temp31 = new Fuse.Reactive.DataBinding<Fuse.Node>(temp3_Active_inst, "currentPage");
        temp4.LineNumber = 4;
        temp4.FileName = "MainView.ux";
        temp4.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../MainView.js"));
        temp3.Children.Add(Page1);
        temp3.Children.Add(Page2);
        temp3.Behaviors.Add(temp31);
        Page1.Name = "Page1";
        Page1.Children.Add(loggedOutView);
        loggedOutView.RowData = "1*,0.5*,2.5*";
        loggedOutView.Padding = float4(10f, 0f, 10f, 0f);
        loggedOutView.Name = "loggedOutView";
        loggedOutView.Children.Add(temp5);
        loggedOutView.Children.Add(temp8);
        loggedOutView.Children.Add(temp13);
        loggedOutView.Children.Add(temp14);
        temp5.Layer = Fuse.Layer.Background;
        temp5.Fills.Add(temp6);
        temp5.Fills.Add(temp7);
        temp6.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/phone.jpg"));
        temp7.Opacity = 0.3f;
        temp7.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/overlay.jpg"));
        temp8.RowCount = 1;
        temp8.ColumnCount = 3;
        global::Fuse.Controls.Grid.SetRow(temp8, 0);
        temp8.Children.Add(temp9);
        temp8.Children.Add(temp10);
        temp8.Children.Add(temp12);
        temp9.Value = "Call";
        temp9.TextColor = float4(1f, 1f, 1f, 1f);
        temp9.Margin = float4(0f, 35f, 0f, 0f);
        global::Fuse.Controls.Grid.SetColumn(temp9, 0);
        temp10.Width = 75f;
        temp10.Height = 75f;
        global::Fuse.Controls.Grid.SetColumn(temp10, 1);
        temp10.Fills.Add(temp11);
        temp11.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/CH-Icon.jpg"));
        temp12.Value = "Hero";
        temp12.Margin = float4(5f, 35f, 0f, 0f);
        global::Fuse.Controls.Grid.SetColumn(temp12, 2);
        temp13.Value = "Available Categories";
        temp13.FontSize = 20f;
        temp13.Alignment = Fuse.Elements.Alignment.Center;
        global::Fuse.Controls.Grid.SetRow(temp13, 1);
        temp14.Margin = float4(0f, 0f, 0f, 10f);
        global::Fuse.Controls.Grid.SetRow(temp14, 2);
        temp14.Content = temp15;
        temp15.Behaviors.Add(temp17);
        temp15.Behaviors.Add(temp);
        temp.Factories.Add(temp16);
        Page2.Name = "Page2";
        Page2.Children.Add(temp18);
        temp18.RowData = "0.3*,2.5*";
        temp18.Children.Add(temp19);
        temp18.Children.Add(temp22);
        temp18.Children.Add(temp27);
        temp19.Layer = Fuse.Layer.Background;
        temp19.Fills.Add(temp20);
        temp19.Fills.Add(temp21);
        temp20.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/background.jpg"));
        temp21.Opacity = 0.4f;
        temp21.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/overlay.jpg"));
        temp22.RowCount = 1;
        temp22.ColumnCount = 2;
        global::Fuse.Controls.Grid.SetRow(temp22, 0);
        temp22.Children.Add(temp23);
        temp22.Children.Add(temp25);
        temp22.Children.Add(temp1);
        temp23.Opacity = 0.5f;
        temp23.Layer = Fuse.Layer.Background;
        temp23.Fill = temp24;
        temp25.Width = 25f;
        temp25.Height = 25f;
        temp25.Margin = float4(-60f, 0f, 0f, 0f);
        global::Fuse.Controls.Grid.SetRow(temp25, 0);
        global::Fuse.Controls.Grid.SetColumn(temp25, 0);
        global::Fuse.Gestures.Clicked.AddHandler(temp25, temp_eb1.OnEvent);
        temp25.File = new global::Uno.UX.BundleFileSource(import global::Uno.BundleFile("../../../../../Assets/arrow.png"));
        temp25.Behaviors.Add(temp_eb1);
        temp1.TextColor = float4(1f, 1f, 1f, 1f);
        temp1.Alignment = Fuse.Elements.Alignment.Center;
        temp1.Margin = float4(-120f, 0f, 0f, 0f);
        temp1.Behaviors.Add(temp26);
        temp27.Margin = float4(0f, 10f, 0f, 10f);
        global::Fuse.Controls.Grid.SetRow(temp27, 1);
        temp27.Content = temp28;
        temp28.Behaviors.Add(temp30);
        temp28.Behaviors.Add(temp2);
        temp2.Factories.Add(temp29);
        this.RootNode = temp3;
        this.Theme = Fuse.BasicTheme.BasicTheme.Singleton;
        this.Behaviors.Add(temp4);
    }
}
