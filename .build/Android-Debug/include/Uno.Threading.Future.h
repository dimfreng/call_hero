// This file was generated based on 'C:\ProgramData\Uno\Packages\Uno.Threading\0.20.1\$.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Object.h>
namespace g{namespace Uno{namespace Threading{struct Future;}}}

namespace g{
namespace Uno{
namespace Threading{

// public abstract class Future :9
// {
uType* Future_typeof();
void Future__ctor__fn(Future* __this);
void Future__get_State_fn(Future* __this, int* __retval);
void Future__set_State_fn(Future* __this, int* value);

struct Future : uObject
{
    int _State;

    void ctor_();
    int State();
    void State(int value);
};
// }

}}} // ::g::Uno::Threading
