// This file was generated based on 'C:\ProgramData\Uno\Packages\Fuse.Controls\0.19.3\$.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Animations.IResize.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.IActualPlacement.h>
#include <Fuse.Navigation.INavigationPanel.h>
#include <Fuse.Node.h>
#include <Fuse.Scripting.INameScope.h>
#include <Fuse.Triggers.Actions.ICollapse.h>
#include <Fuse.Triggers.Actions.IHide.h>
#include <Fuse.Triggers.Actions.IShow.h>
#include <Fuse.Triggers.IAddRemove-1.h>
namespace g{namespace Fuse{namespace Controls{struct Grid;}}}
namespace g{namespace Fuse{namespace Elements{struct Element;}}}
namespace g{namespace Fuse{namespace Layouts{struct GridLayout;}}}
namespace g{namespace Fuse{struct StyleProperty1;}}

namespace g{
namespace Fuse{
namespace Controls{

// public sealed class Grid :842
// {
::g::Fuse::Controls::Panel_type* Grid_typeof();
void Grid__ctor_4_fn(Grid* __this);
void Grid__get_CellSpacing_fn(Grid* __this, float* __retval);
void Grid__set_CellSpacing_fn(Grid* __this, float* value);
void Grid__get_ColumnCount_fn(Grid* __this, int* __retval);
void Grid__set_ColumnCount_fn(Grid* __this, int* value);
void Grid__get_ContentAlignment_fn(Grid* __this, int* __retval);
void Grid__set_ContentAlignment_fn(Grid* __this, int* value);
void Grid__New2_fn(Grid** __retval);
void Grid__OnCellSpacingChanged_fn(Grid* p);
void Grid__OnContentAlignmentChanged_fn(Grid* p);
void Grid__get_RowCount_fn(Grid* __this, int* __retval);
void Grid__set_RowCount_fn(Grid* __this, int* value);
void Grid__get_RowData_fn(Grid* __this, uString** __retval);
void Grid__set_RowData_fn(Grid* __this, uString* value);
void Grid__SetColumn_fn(::g::Fuse::Elements::Element* elm, int* col);
void Grid__SetRow_fn(::g::Fuse::Elements::Element* elm, int* row);

struct Grid : ::g::Fuse::Controls::Panel
{
    uStrong< ::g::Fuse::Layouts::GridLayout*> _gridLayout;
    static uSStrong< ::g::Fuse::StyleProperty1*> CellSpacingProperty_;
    static uSStrong< ::g::Fuse::StyleProperty1*>& CellSpacingProperty() { return Grid_typeof()->Init(), CellSpacingProperty_; }
    static uSStrong< ::g::Fuse::StyleProperty1*> ContentAlignmentProperty_;
    static uSStrong< ::g::Fuse::StyleProperty1*>& ContentAlignmentProperty() { return Grid_typeof()->Init(), ContentAlignmentProperty_; }

    void ctor_4();
    float CellSpacing();
    void CellSpacing(float value);
    int ColumnCount();
    void ColumnCount(int value);
    int ContentAlignment();
    void ContentAlignment(int value);
    int RowCount();
    void RowCount(int value);
    uString* RowData();
    void RowData(uString* value);
    static Grid* New2();
    static void OnCellSpacingChanged(Grid* p);
    static void OnContentAlignmentChanged(Grid* p);
    static void SetColumn(::g::Fuse::Elements::Element* elm, int col);
    static void SetRow(::g::Fuse::Elements::Element* elm, int row);
};
// }

}}} // ::g::Fuse::Controls
