// This file was generated based on 'C:\ProgramData\Uno\Packages\Fuse.Themes\0.19.3\.upk\meta'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.h>
namespace g{namespace Uno{struct BundleFile;}}
namespace g{struct Fuse_Themes_bundle;}

namespace g{

// public static generated class Fuse_Themes_bundle :0
// {
uClassType* Fuse_Themes_bundle_typeof();

struct Fuse_Themes_bundle : uObject
{
    static uSStrong< ::g::Uno::BundleFile*> Roboto_Regularf1c3c1ef_;
    static uSStrong< ::g::Uno::BundleFile*>& Roboto_Regularf1c3c1ef() { return Fuse_Themes_bundle_typeof()->Init(), Roboto_Regularf1c3c1ef_; }
};
// }

} // ::g
