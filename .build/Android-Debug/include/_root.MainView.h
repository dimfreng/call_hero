// This file was generated based on 'C:\Users\Kevin\Desktop\callhero\CallHero\.cache\GeneratedCode\MainView.g.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.App.h>
namespace g{namespace Fuse{namespace Controls{struct Grid;}}}
namespace g{namespace Fuse{namespace Controls{struct Page;}}}
namespace g{namespace Fuse{namespace Reactive{struct EventBinding;}}}
namespace g{namespace Fuse{struct Font;}}
namespace g{struct MainView;}
namespace g{struct MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property;}
namespace g{struct MainView__Fuse_Controls_TextControl_string_Value_Property;}
namespace g{struct MainView__Fuse_Reactive_Each_object_Items_Property;}

namespace g{

// public partial sealed class MainView :16
// {
::g::Fuse::AppBase_type* MainView_typeof();
void MainView__ctor_3_fn(MainView* __this);
void MainView__InitializeUX_fn(MainView* __this);
void MainView__New1_fn(MainView** __retval);

struct MainView : ::g::Fuse::App
{
    uStrong< ::g::Fuse::Controls::Grid*> loggedOutView;
    uStrong< ::g::Fuse::Controls::Page*> Page1;
    uStrong< ::g::Fuse::Controls::Page*> Page2;
    static uSStrong< ::g::Fuse::Font*> seasrn_;
    static uSStrong< ::g::Fuse::Font*>& seasrn() { return MainView_typeof()->Init(), seasrn_; }
    uStrong< ::g::Fuse::Reactive::EventBinding*> temp_eb1;
    uStrong<MainView__Fuse_Reactive_Each_object_Items_Property*> temp_Items_inst;
    uStrong<MainView__Fuse_Controls_TextControl_string_Value_Property*> temp1_Value_inst;
    uStrong<MainView__Fuse_Reactive_Each_object_Items_Property*> temp2_Items_inst;
    uStrong<MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property*> temp3_Active_inst;

    void ctor_3();
    void InitializeUX();
    static MainView* New1();
};
// }

} // ::g
