// This file was generated based on 'C:\ProgramData\Uno\Packages\FuseCore\0.19.3\Input\$.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Fuse{
namespace Input{

// public delegate void PointerMovedHandler(object sender, Fuse.Input.PointerMovedArgs args) :788
uDelegateType* PointerMovedHandler_typeof();

}}} // ::g::Fuse::Input
