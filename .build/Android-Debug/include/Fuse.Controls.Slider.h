// This file was generated based on 'C:\ProgramData\Uno\Packages\Fuse.Controls\0.19.3\$.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Animations.IResize.h>
#include <Fuse.Controls.RangeControl.h>
#include <Fuse.IActualPlacement.h>
#include <Fuse.Navigation.INavigationPanel.h>
#include <Fuse.Node.h>
#include <Fuse.Scripting.INameScope.h>
#include <Fuse.Triggers.Actions.ICollapse.h>
#include <Fuse.Triggers.Actions.IHide.h>
#include <Fuse.Triggers.Actions.IShow.h>
#include <Fuse.Triggers.IAddRemove-1.h>
#include <Fuse.Triggers.IProgress.h>
#include <Fuse.Triggers.IValue-1.h>
#include <Uno.Double.h>
namespace g{namespace Fuse{namespace Controls{struct Slider;}}}

namespace g{
namespace Fuse{
namespace Controls{

// public sealed class Slider :2995
// {
::g::Fuse::Controls::RangeControl_type* Slider_typeof();

struct Slider : ::g::Fuse::Controls::RangeControl
{
};
// }

}}} // ::g::Fuse::Controls
