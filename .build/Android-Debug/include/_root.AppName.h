// This file was generated based on 'C:\Users\Kevin\Desktop\callhero\CallHero\.cache\GeneratedCode\MainView.g.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Animations.IResize.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.IActualPlacement.h>
#include <Fuse.Navigation.INavigationPanel.h>
#include <Fuse.Node.h>
#include <Fuse.Scripting.INameScope.h>
#include <Fuse.Triggers.Actions.ICollapse.h>
#include <Fuse.Triggers.Actions.IHide.h>
#include <Fuse.Triggers.Actions.IShow.h>
#include <Fuse.Triggers.IAddRemove-1.h>
#include <Fuse.Triggers.IValue-1.h>
#include <Uno.String.h>
namespace g{struct AppName;}

namespace g{

// public partial sealed class AppName :1
// {
::g::Fuse::Controls::TextControl_type* AppName_typeof();
void AppName__ctor_7_fn(AppName* __this);
void AppName__InitializeUX_fn(AppName* __this);
void AppName__New3_fn(AppName** __retval);

struct AppName : ::g::Fuse::Controls::Text
{
    void ctor_7();
    void InitializeUX();
    static AppName* New3();
};
// }

} // ::g
