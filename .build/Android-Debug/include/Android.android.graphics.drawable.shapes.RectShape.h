// This file was generated based on 'C:\ProgramData\Uno\Packages\Android\0.20.2\Android\android\graphics\drawable\shapes\$.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Android.android.graphics.drawable.shapes.Shape.h>
#include <Android.Base.Wrappers.IJWrapper.h>
#include <jni.h>
#include <Uno.IDisposable.h>
namespace g{namespace Android{namespace android{namespace graphics{namespace drawable{namespace shapes{struct RectShape;}}}}}}

namespace g{
namespace Android{
namespace android{
namespace graphics{
namespace drawable{
namespace shapes{

// public extern class RectShape :104
// {
::g::Android::java::lang::Object_type* RectShape_typeof();
void RectShape__ctor_7_fn(RectShape* __this, jobject* obj, uType* utype, bool* hasFallbackClass, bool* resolveType);

struct RectShape : ::g::Android::android::graphics::drawable::shapes::Shape
{
    void ctor_7(jobject obj, uType* utype, bool hasFallbackClass, bool resolveType);
};
// }

}}}}}} // ::g::Android::android::graphics::drawable::shapes
