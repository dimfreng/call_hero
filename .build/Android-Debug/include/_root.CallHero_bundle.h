// This file was generated based on 'C:\Users\Kevin\Desktop\callhero\CallHero\CallHero.unoproj'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.h>
namespace g{namespace Uno{struct BundleFile;}}
namespace g{struct CallHero_bundle;}

namespace g{

// public static generated class CallHero_bundle :0
// {
uClassType* CallHero_bundle_typeof();

struct CallHero_bundle : uObject
{
    static uSStrong< ::g::Uno::BundleFile*> arrowd98763bb_;
    static uSStrong< ::g::Uno::BundleFile*>& arrowd98763bb() { return CallHero_bundle_typeof()->Init(), arrowd98763bb_; }
    static uSStrong< ::g::Uno::BundleFile*> backgroundb072867a_;
    static uSStrong< ::g::Uno::BundleFile*>& backgroundb072867a() { return CallHero_bundle_typeof()->Init(), backgroundb072867a_; }
    static uSStrong< ::g::Uno::BundleFile*> CH_Icon96ea8a5e_;
    static uSStrong< ::g::Uno::BundleFile*>& CH_Icon96ea8a5e() { return CallHero_bundle_typeof()->Init(), CH_Icon96ea8a5e_; }
    static uSStrong< ::g::Uno::BundleFile*> MainView44d91759_;
    static uSStrong< ::g::Uno::BundleFile*>& MainView44d91759() { return CallHero_bundle_typeof()->Init(), MainView44d91759_; }
    static uSStrong< ::g::Uno::BundleFile*> overlaya0e0bdbe_;
    static uSStrong< ::g::Uno::BundleFile*>& overlaya0e0bdbe() { return CallHero_bundle_typeof()->Init(), overlaya0e0bdbe_; }
    static uSStrong< ::g::Uno::BundleFile*> Phone_icon86eee396_;
    static uSStrong< ::g::Uno::BundleFile*>& Phone_icon86eee396() { return CallHero_bundle_typeof()->Init(), Phone_icon86eee396_; }
    static uSStrong< ::g::Uno::BundleFile*> phone119a696f_;
    static uSStrong< ::g::Uno::BundleFile*>& phone119a696f() { return CallHero_bundle_typeof()->Init(), phone119a696f_; }
    static uSStrong< ::g::Uno::BundleFile*> SEASRN__9a1d02eb_;
    static uSStrong< ::g::Uno::BundleFile*>& SEASRN__9a1d02eb() { return CallHero_bundle_typeof()->Init(), SEASRN__9a1d02eb_; }
};
// }

} // ::g
