// This file was generated based on 'C:\ProgramData\Uno\Packages\Fuse.BasicTheme\0.19.3\.upk\meta'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.h>
namespace g{namespace Uno{struct BundleFile;}}
namespace g{struct Fuse_BasicTheme_bundle;}

namespace g{

// public static generated class Fuse_BasicTheme_bundle :0
// {
uClassType* Fuse_BasicTheme_bundle_typeof();

struct Fuse_BasicTheme_bundle : uObject
{
    static uSStrong< ::g::Uno::BundleFile*> Roboto_Blackf5d7198c_;
    static uSStrong< ::g::Uno::BundleFile*>& Roboto_Blackf5d7198c() { return Fuse_BasicTheme_bundle_typeof()->Init(), Roboto_Blackf5d7198c_; }
    static uSStrong< ::g::Uno::BundleFile*> Roboto_BlackItalicc5a41370_;
    static uSStrong< ::g::Uno::BundleFile*>& Roboto_BlackItalicc5a41370() { return Fuse_BasicTheme_bundle_typeof()->Init(), Roboto_BlackItalicc5a41370_; }
    static uSStrong< ::g::Uno::BundleFile*> Roboto_Bold7755803f_;
    static uSStrong< ::g::Uno::BundleFile*>& Roboto_Bold7755803f() { return Fuse_BasicTheme_bundle_typeof()->Init(), Roboto_Bold7755803f_; }
    static uSStrong< ::g::Uno::BundleFile*> Roboto_BoldItalic04d79bfd_;
    static uSStrong< ::g::Uno::BundleFile*>& Roboto_BoldItalic04d79bfd() { return Fuse_BasicTheme_bundle_typeof()->Init(), Roboto_BoldItalic04d79bfd_; }
    static uSStrong< ::g::Uno::BundleFile*> Roboto_Italic39cc53a1_;
    static uSStrong< ::g::Uno::BundleFile*>& Roboto_Italic39cc53a1() { return Fuse_BasicTheme_bundle_typeof()->Init(), Roboto_Italic39cc53a1_; }
    static uSStrong< ::g::Uno::BundleFile*> Roboto_Light6eba8813_;
    static uSStrong< ::g::Uno::BundleFile*>& Roboto_Light6eba8813() { return Fuse_BasicTheme_bundle_typeof()->Init(), Roboto_Light6eba8813_; }
    static uSStrong< ::g::Uno::BundleFile*> Roboto_LightItalic33f1b993_;
    static uSStrong< ::g::Uno::BundleFile*>& Roboto_LightItalic33f1b993() { return Fuse_BasicTheme_bundle_typeof()->Init(), Roboto_LightItalic33f1b993_; }
    static uSStrong< ::g::Uno::BundleFile*> Roboto_Mediume4e88fae_;
    static uSStrong< ::g::Uno::BundleFile*>& Roboto_Mediume4e88fae() { return Fuse_BasicTheme_bundle_typeof()->Init(), Roboto_Mediume4e88fae_; }
    static uSStrong< ::g::Uno::BundleFile*> Roboto_MediumItalic4a35f4dc_;
    static uSStrong< ::g::Uno::BundleFile*>& Roboto_MediumItalic4a35f4dc() { return Fuse_BasicTheme_bundle_typeof()->Init(), Roboto_MediumItalic4a35f4dc_; }
    static uSStrong< ::g::Uno::BundleFile*> Roboto_Regularc35195f4_;
    static uSStrong< ::g::Uno::BundleFile*>& Roboto_Regularc35195f4() { return Fuse_BasicTheme_bundle_typeof()->Init(), Roboto_Regularc35195f4_; }
    static uSStrong< ::g::Uno::BundleFile*> Roboto_Thin6a587ab8_;
    static uSStrong< ::g::Uno::BundleFile*>& Roboto_Thin6a587ab8() { return Fuse_BasicTheme_bundle_typeof()->Init(), Roboto_Thin6a587ab8_; }
    static uSStrong< ::g::Uno::BundleFile*> Roboto_ThinItalice8b656e6_;
    static uSStrong< ::g::Uno::BundleFile*>& Roboto_ThinItalice8b656e6() { return Fuse_BasicTheme_bundle_typeof()->Init(), Roboto_ThinItalice8b656e6_; }
    static uSStrong< ::g::Uno::BundleFile*> RobotoCondensed_Boldb51af5b9_;
    static uSStrong< ::g::Uno::BundleFile*>& RobotoCondensed_Boldb51af5b9() { return Fuse_BasicTheme_bundle_typeof()->Init(), RobotoCondensed_Boldb51af5b9_; }
    static uSStrong< ::g::Uno::BundleFile*> RobotoCondensed_BoldItalic300c0ef3_;
    static uSStrong< ::g::Uno::BundleFile*>& RobotoCondensed_BoldItalic300c0ef3() { return Fuse_BasicTheme_bundle_typeof()->Init(), RobotoCondensed_BoldItalic300c0ef3_; }
    static uSStrong< ::g::Uno::BundleFile*> RobotoCondensed_Italic3437663c_;
    static uSStrong< ::g::Uno::BundleFile*>& RobotoCondensed_Italic3437663c() { return Fuse_BasicTheme_bundle_typeof()->Init(), RobotoCondensed_Italic3437663c_; }
    static uSStrong< ::g::Uno::BundleFile*> RobotoCondensed_Lightbfb1e7a3_;
    static uSStrong< ::g::Uno::BundleFile*>& RobotoCondensed_Lightbfb1e7a3() { return Fuse_BasicTheme_bundle_typeof()->Init(), RobotoCondensed_Lightbfb1e7a3_; }
    static uSStrong< ::g::Uno::BundleFile*> RobotoCondensed_LightItalic7329a69e_;
    static uSStrong< ::g::Uno::BundleFile*>& RobotoCondensed_LightItalic7329a69e() { return Fuse_BasicTheme_bundle_typeof()->Init(), RobotoCondensed_LightItalic7329a69e_; }
    static uSStrong< ::g::Uno::BundleFile*> RobotoCondensed_Regular85d27024_;
    static uSStrong< ::g::Uno::BundleFile*>& RobotoCondensed_Regular85d27024() { return Fuse_BasicTheme_bundle_typeof()->Init(), RobotoCondensed_Regular85d27024_; }
};
// }

} // ::g
