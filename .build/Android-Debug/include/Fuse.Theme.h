// This file was generated based on 'C:\ProgramData\Uno\Packages\FuseCore\0.19.3\$.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Style.h>
#include <Uno.UX.ITemplate.h>
namespace g{namespace Fuse{struct Theme;}}

namespace g{
namespace Fuse{

// public abstract class Theme :5873
// {
::g::Fuse::Style_type* Theme_typeof();
void Theme__ctor_1_fn(Theme* __this);

struct Theme : ::g::Fuse::Style
{
    void ctor_1();
};
// }

}} // ::g::Fuse
