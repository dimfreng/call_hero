// This file was generated based on 'C:\ProgramData\Uno\Packages\Fuse.UserEvents\0.19.3\$.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Fuse{

// public delegate void UserEventHandler(object sender, Fuse.UserEventArgs args) :213
uDelegateType* UserEventHandler_typeof();

}} // ::g::Fuse
