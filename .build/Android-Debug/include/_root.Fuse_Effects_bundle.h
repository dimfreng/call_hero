// This file was generated based on 'C:\ProgramData\Uno\Packages\Fuse.Effects\0.19.3\.upk\meta'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.h>
namespace g{namespace Uno{namespace Runtime{namespace Implementation{namespace ShaderBackends{namespace OpenGL{struct GLProgram;}}}}}}
namespace g{struct Fuse_Effects_bundle;}

namespace g{

// public static generated class Fuse_Effects_bundle :0
// {
uClassType* Fuse_Effects_bundle_typeof();

struct Fuse_Effects_bundle : uObject
{
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> Blitterec98f148_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& Blitterec98f148() { return Fuse_Effects_bundle_typeof()->Init(), Blitterec98f148_; }
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> Blur289a5370_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& Blur289a5370() { return Fuse_Effects_bundle_typeof()->Init(), Blur289a5370_; }
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> Desaturate1f514108_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& Desaturate1f514108() { return Fuse_Effects_bundle_typeof()->Init(), Desaturate1f514108_; }
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> EffectHelpers0ae20243_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& EffectHelpers0ae20243() { return Fuse_Effects_bundle_typeof()->Init(), EffectHelpers0ae20243_; }
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> EffectHelpersa4726cf5_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& EffectHelpersa4726cf5() { return Fuse_Effects_bundle_typeof()->Init(), EffectHelpersa4726cf5_; }
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> EffectHelpersf4e3f746_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& EffectHelpersf4e3f746() { return Fuse_Effects_bundle_typeof()->Init(), EffectHelpersf4e3f746_; }
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> EffectHelpersf791727a_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& EffectHelpersf791727a() { return Fuse_Effects_bundle_typeof()->Init(), EffectHelpersf791727a_; }
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> Halftone1ff68c3c_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& Halftone1ff68c3c() { return Fuse_Effects_bundle_typeof()->Init(), Halftone1ff68c3c_; }
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> Mask85b813a2_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& Mask85b813a2() { return Fuse_Effects_bundle_typeof()->Init(), Mask85b813a2_; }
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> Maske38913a2_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& Maske38913a2() { return Fuse_Effects_bundle_typeof()->Init(), Maske38913a2_; }
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*> Maskfa5313a2_;
    static uSStrong< ::g::Uno::Runtime::Implementation::ShaderBackends::OpenGL::GLProgram*>& Maskfa5313a2() { return Fuse_Effects_bundle_typeof()->Init(), Maskfa5313a2_; }
};
// }

} // ::g
