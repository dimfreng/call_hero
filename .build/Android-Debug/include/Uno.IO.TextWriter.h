// This file was generated based on 'C:\ProgramData\Uno\Packages\UnoCore\0.20.1\Source\Uno\IO\$.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.IDisposable.h>
#include <Uno.Object.h>
namespace g{namespace Uno{namespace IO{struct TextWriter;}}}

namespace g{
namespace Uno{
namespace IO{

// public abstract class TextWriter :2324
// {
struct TextWriter_type : uType
{
    ::g::Uno::IDisposable interface0;
    void(*fp_Dispose1)(::g::Uno::IO::TextWriter*, bool*);
    void(*fp_Write3)(::g::Uno::IO::TextWriter*, uArray*, int*, int*);
    void(*fp_Write9)(::g::Uno::IO::TextWriter*, uString*);
};

TextWriter_type* TextWriter_typeof();
void TextWriter__ctor__fn(TextWriter* __this);
void TextWriter__Dispose_fn(TextWriter* __this);
void TextWriter__Dispose1_fn(TextWriter* __this, bool* disposing);
void TextWriter__Write2_fn(TextWriter* __this, uArray* buffer);
void TextWriter__Write3_fn(TextWriter* __this, uArray* buffer, int* index, int* count);
void TextWriter__Write9_fn(TextWriter* __this, uString* value);

struct TextWriter : uObject
{
    void ctor_();
    void Dispose();
    void Dispose1(bool disposing) { (((TextWriter_type*)__type)->fp_Dispose1)(this, &disposing); }
    void Write2(uArray* buffer);
    void Write3(uArray* buffer, int index, int count) { (((TextWriter_type*)__type)->fp_Write3)(this, buffer, &index, &count); }
    void Write9(uString* value) { (((TextWriter_type*)__type)->fp_Write9)(this, value); }
    static void Dispose1(TextWriter* __this, bool disposing) { TextWriter__Dispose1_fn(__this, &disposing); }
    static void Write3(TextWriter* __this, uArray* buffer, int index, int count) { TextWriter__Write3_fn(__this, buffer, &index, &count); }
    static void Write9(TextWriter* __this, uString* value) { TextWriter__Write9_fn(__this, value); }
};
// }

}}} // ::g::Uno::IO
