// This file was generated based on 'C:\ProgramData\Uno\Packages\FuseCore\0.19.3\Input\$.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Input.PointerEventArgs.h>
#include <Fuse.Scripting.IScriptEvent.h>
namespace g{namespace Fuse{namespace Input{struct PointerWheelMovedArgs;}}}

namespace g{
namespace Fuse{
namespace Input{

// public sealed class PointerWheelMovedArgs :860
// {
::g::Fuse::NodeEventArgs_type* PointerWheelMovedArgs_typeof();

struct PointerWheelMovedArgs : ::g::Fuse::Input::PointerEventArgs
{
};
// }

}}} // ::g::Fuse::Input
