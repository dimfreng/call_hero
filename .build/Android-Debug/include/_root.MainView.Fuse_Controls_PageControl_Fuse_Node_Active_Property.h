// This file was generated based on 'C:\Users\Kevin\Desktop\callhero\CallHero\.cache\GeneratedCode\MainView.g.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Node.h>
#include <Uno.UX.Property-1.h>
namespace g{namespace Fuse{namespace Controls{struct PageControl;}}}
namespace g{struct MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property;}

namespace g{

// public sealed class MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property :18
// {
::g::Uno::UX::Property_type* MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property_typeof();
void MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property__ctor_1_fn(MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property* __this, ::g::Fuse::Controls::PageControl* obj);
void MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property__New1_fn(::g::Fuse::Controls::PageControl* obj, MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property** __retval);
void MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property__OnGet_fn(MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property* __this, ::g::Fuse::Node** __retval);
void MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property__OnSet_fn(MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property* __this, ::g::Fuse::Node* v, uObject* origin);

struct MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property : ::g::Uno::UX::Property
{
    uStrong< ::g::Fuse::Controls::PageControl*> _obj;

    void ctor_1(::g::Fuse::Controls::PageControl* obj);
    static MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property* New1(::g::Fuse::Controls::PageControl* obj);
};
// }

} // ::g
