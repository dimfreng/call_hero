// This file was generated based on 'C:\ProgramData\Uno\Packages\Fuse.Controls\0.19.3\$.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Fuse{
namespace Controls{

// public delegate void TextInputActionHandler(object sender, Fuse.Controls.TextInputActionArgs args) :4039
uDelegateType* TextInputActionHandler_typeof();

}}} // ::g::Fuse::Controls
