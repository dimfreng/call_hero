// This file was generated based on 'C:\Users\Kevin\Desktop\callhero\CallHero\CallHero.unoproj'.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.CallHero_bundle.h>
#include <Uno.BundleFile.h>
#include <Uno.String.h>
static uString* STRINGS[8];

namespace g{

// public static generated class CallHero_bundle :0
// {
// static CallHero_bundle() :0
static void CallHero_bundle__cctor__fn(uType* __type)
{
    CallHero_bundle::arrowd98763bb_ = ::g::Uno::BundleFile::New1(::STRINGS[0/*"arrow-ffec2...*/]);
    CallHero_bundle::backgroundb072867a_ = ::g::Uno::BundleFile::New1(::STRINGS[1/*"background-...*/]);
    CallHero_bundle::CH_Icon96ea8a5e_ = ::g::Uno::BundleFile::New1(::STRINGS[2/*"ch-icon-273...*/]);
    CallHero_bundle::MainView44d91759_ = ::g::Uno::BundleFile::New1(::STRINGS[3/*"mainview-51...*/]);
    CallHero_bundle::overlaya0e0bdbe_ = ::g::Uno::BundleFile::New1(::STRINGS[4/*"overlay-132...*/]);
    CallHero_bundle::Phone_icon86eee396_ = ::g::Uno::BundleFile::New1(::STRINGS[5/*"phone-icon-...*/]);
    CallHero_bundle::phone119a696f_ = ::g::Uno::BundleFile::New1(::STRINGS[6/*"phone-118b1...*/]);
    CallHero_bundle::SEASRN__9a1d02eb_ = ::g::Uno::BundleFile::New1(::STRINGS[7/*"seasrn__-de...*/]);
}

uClassType* CallHero_bundle_typeof()
{
    static uSStrong<uClassType*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.FieldCount = 8;
    options.TypeSize = sizeof(uClassType);
    type = uClassType::New("CallHero_bundle", options);
    type->fp_cctor_ = CallHero_bundle__cctor__fn;
    ::STRINGS[0] = uString::Const("arrow-ffec248d.png");
    ::STRINGS[1] = uString::Const("background-c9186ac9.jpg");
    ::STRINGS[2] = uString::Const("ch-icon-273fb448.jpg");
    ::STRINGS[3] = uString::Const("mainview-51cc8ebe.js");
    ::STRINGS[4] = uString::Const("overlay-1322bee8.jpg");
    ::STRINGS[5] = uString::Const("phone-icon-56e466b5.png");
    ::STRINGS[6] = uString::Const("phone-118b1e16.jpg");
    ::STRINGS[7] = uString::Const("seasrn__-de2cf990.ttf");
    type->SetFields(0,
        ::g::Uno::BundleFile_typeof(), (uintptr_t)&::g::CallHero_bundle::arrowd98763bb_, uFieldFlagsStatic,
        ::g::Uno::BundleFile_typeof(), (uintptr_t)&::g::CallHero_bundle::backgroundb072867a_, uFieldFlagsStatic,
        ::g::Uno::BundleFile_typeof(), (uintptr_t)&::g::CallHero_bundle::CH_Icon96ea8a5e_, uFieldFlagsStatic,
        ::g::Uno::BundleFile_typeof(), (uintptr_t)&::g::CallHero_bundle::MainView44d91759_, uFieldFlagsStatic,
        ::g::Uno::BundleFile_typeof(), (uintptr_t)&::g::CallHero_bundle::overlaya0e0bdbe_, uFieldFlagsStatic,
        ::g::Uno::BundleFile_typeof(), (uintptr_t)&::g::CallHero_bundle::Phone_icon86eee396_, uFieldFlagsStatic,
        ::g::Uno::BundleFile_typeof(), (uintptr_t)&::g::CallHero_bundle::phone119a696f_, uFieldFlagsStatic,
        ::g::Uno::BundleFile_typeof(), (uintptr_t)&::g::CallHero_bundle::SEASRN__9a1d02eb_, uFieldFlagsStatic);
    return type;
}

uSStrong< ::g::Uno::BundleFile*> CallHero_bundle::arrowd98763bb_;
uSStrong< ::g::Uno::BundleFile*> CallHero_bundle::backgroundb072867a_;
uSStrong< ::g::Uno::BundleFile*> CallHero_bundle::CH_Icon96ea8a5e_;
uSStrong< ::g::Uno::BundleFile*> CallHero_bundle::MainView44d91759_;
uSStrong< ::g::Uno::BundleFile*> CallHero_bundle::overlaya0e0bdbe_;
uSStrong< ::g::Uno::BundleFile*> CallHero_bundle::Phone_icon86eee396_;
uSStrong< ::g::Uno::BundleFile*> CallHero_bundle::phone119a696f_;
uSStrong< ::g::Uno::BundleFile*> CallHero_bundle::SEASRN__9a1d02eb_;
// }

} // ::g
