// This file was generated based on 'C:\Users\Kevin\Desktop\callhero\CallHero\.cache\GeneratedCode\MainView.g.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.CallHero_bundle.h>
#include <_root.MainView.Factory1.h>
#include <_root.MainView.Fuse_Controls_TextControl_string_Value_Property.h>
#include <_root.MainView.h>
#include <Fuse.Behavior.h>
#include <Fuse.Controls.Grid.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.Controls.Rectangle.h>
#include <Fuse.Controls.ScrollView.h>
#include <Fuse.Controls.Shape.h>
#include <Fuse.Controls.StackPanel.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Drawing.Brush.h>
#include <Fuse.Drawing.ImageFill.h>
#include <Fuse.Drawing.StaticSolidColor.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Gestures.Clicked.h>
#include <Fuse.Gestures.ClickedHandler.h>
#include <Fuse.Gestures.ScrollDirections.h>
#include <Fuse.Node.h>
#include <Fuse.Reactive.DataBinding-1.h>
#include <Fuse.Reactive.EventBinding.h>
#include <Uno.BundleFile.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Float.h>
#include <Uno.Float4.h>
#include <Uno.Int.h>
#include <Uno.String.h>
#include <Uno.UX.BundleFileSource.h>
#include <Uno.UX.FileSource.h>
#include <Uno.UX.Property-1.h>
static uString* STRINGS[4];
static uType* TYPES[16];

namespace g{

// public partial sealed class MainView.Factory1 :76
// {
// static Factory1() :87
static void MainView__Factory1__cctor__fn(uType* __type)
{
}

MainView__Factory1_type* MainView__Factory1_typeof()
{
    static uSStrong<MainView__Factory1_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.FieldCount = 4;
    options.InterfaceCount = 1;
    options.ObjectSize = sizeof(MainView__Factory1);
    options.TypeSize = sizeof(MainView__Factory1_type);
    type = (MainView__Factory1_type*)uClassType::New("MainView.Factory1", options);
    type->fp_cctor_ = MainView__Factory1__cctor__fn;
    type->interface0.fp_New1 = (void(*)(uObject*, uObject**))MainView__Factory1__New1_fn;
    ::STRINGS[0] = uString::Const("name");
    ::STRINGS[1] = uString::Const("address");
    ::STRINGS[2] = uString::Const("number");
    ::STRINGS[3] = uString::Const("callNumber");
    ::TYPES[0] = ::g::Fuse::Controls::TextControl_typeof();
    ::TYPES[1] = ::g::Fuse::Reactive::DataBinding_typeof()->MakeType(::g::Uno::String_typeof());
    ::TYPES[2] = ::g::Uno::UX::Property_typeof()->MakeType(::g::Uno::String_typeof());
    ::TYPES[3] = ::g::Fuse::Elements::Element_typeof();
    ::TYPES[4] = ::g::Fuse::Controls::Grid_typeof();
    ::TYPES[5] = ::g::Fuse::Controls::Panel_typeof();
    ::TYPES[6] = ::g::Fuse::Node_typeof();
    ::TYPES[7] = ::g::Fuse::Controls::ScrollView_typeof();
    ::TYPES[8] = ::g::Fuse::Behavior_typeof();
    ::TYPES[9] = ::g::Fuse::Gestures::ClickedHandler_typeof();
    ::TYPES[10] = ::g::Fuse::Controls::Shape_typeof();
    ::TYPES[11] = ::g::Fuse::Drawing::Brush_typeof();
    ::TYPES[12] = ::g::Fuse::Drawing::ImageFill_typeof();
    ::TYPES[13] = ::g::Uno::UX::FileSource_typeof();
    ::TYPES[14] = ::g::CallHero_bundle_typeof();
    ::TYPES[15] = uObject_typeof();
    type->SetInterfaces(
        ::g::Uno::UX::IFactory_typeof(), offsetof(MainView__Factory1_type, interface0));
    type->SetFields(0,
        ::g::MainView_typeof(), offsetof(::g::MainView__Factory1, __parent1), 0,
        ::g::MainView__Fuse_Controls_TextControl_string_Value_Property_typeof(), offsetof(::g::MainView__Factory1, temp_Value_inst), 0,
        ::g::MainView__Fuse_Controls_TextControl_string_Value_Property_typeof(), offsetof(::g::MainView__Factory1, temp1_Value_inst), 0,
        ::g::MainView__Fuse_Controls_TextControl_string_Value_Property_typeof(), offsetof(::g::MainView__Factory1, temp2_Value_inst), 0);
    return type;
}

// public Factory1(MainView parent) :79
void MainView__Factory1__ctor__fn(MainView__Factory1* __this, ::g::MainView* parent)
{
    __this->ctor_(parent);
}

// public object New() :90
void MainView__Factory1__New1_fn(MainView__Factory1* __this, uObject** __retval)
{
    *__retval = __this->New1();
}

// public Factory1 New(MainView parent) :79
void MainView__Factory1__New2_fn(::g::MainView* parent, MainView__Factory1** __retval)
{
    *__retval = MainView__Factory1::New2(parent);
}

// public Factory1(MainView parent) [instance] :79
void MainView__Factory1::ctor_(::g::MainView* parent)
{
    __parent1 = parent;
}

// public object New() [instance] :90
uObject* MainView__Factory1::New1()
{
    ::g::Fuse::Controls::Rectangle* self = ::g::Fuse::Controls::Rectangle::New2();
    ::g::Fuse::Controls::Text* temp = ::g::Fuse::Controls::Text::New2();
    temp_Value_inst = ::g::MainView__Fuse_Controls_TextControl_string_Value_Property::New1(temp);
    ::g::Fuse::Controls::Text* temp1 = ::g::Fuse::Controls::Text::New2();
    temp1_Value_inst = ::g::MainView__Fuse_Controls_TextControl_string_Value_Property::New1(temp1);
    ::g::Fuse::Controls::Text* temp2 = ::g::Fuse::Controls::Text::New2();
    temp2_Value_inst = ::g::MainView__Fuse_Controls_TextControl_string_Value_Property::New1(temp2);
    ::g::Fuse::Controls::Grid* temp3 = ::g::Fuse::Controls::Grid::New2();
    ::g::Fuse::Controls::StackPanel* temp4 = ::g::Fuse::Controls::StackPanel::New2();
    ::g::Fuse::Controls::ScrollView* temp5 = ::g::Fuse::Controls::ScrollView::New2();
    ::g::Fuse::Reactive::DataBinding* temp6 = (::g::Fuse::Reactive::DataBinding*)::g::Fuse::Reactive::DataBinding::New1(::TYPES[1/*Fuse.Reactive.DataBinding<string>*/], temp_Value_inst, ::STRINGS[0/*"name"*/]);
    ::g::Fuse::Controls::ScrollView* temp7 = ::g::Fuse::Controls::ScrollView::New2();
    ::g::Fuse::Reactive::DataBinding* temp8 = (::g::Fuse::Reactive::DataBinding*)::g::Fuse::Reactive::DataBinding::New1(::TYPES[1/*Fuse.Reactive.DataBinding<string>*/], temp1_Value_inst, ::STRINGS[1/*"address"*/]);
    ::g::Fuse::Controls::ScrollView* temp9 = ::g::Fuse::Controls::ScrollView::New2();
    ::g::Fuse::Reactive::DataBinding* temp10 = (::g::Fuse::Reactive::DataBinding*)::g::Fuse::Reactive::DataBinding::New1(::TYPES[1/*Fuse.Reactive.DataBinding<string>*/], temp2_Value_inst, ::STRINGS[2/*"number"*/]);
    ::g::Fuse::Controls::StackPanel* temp11 = ::g::Fuse::Controls::StackPanel::New2();
    ::g::Fuse::Controls::Rectangle* temp12 = ::g::Fuse::Controls::Rectangle::New2();
    ::g::Fuse::Drawing::ImageFill* temp13 = ::g::Fuse::Drawing::ImageFill::New1();
    ::g::Fuse::Reactive::EventBinding* temp_eb21 = ::g::Fuse::Reactive::EventBinding::New1(::STRINGS[3/*"callNumber"*/]);
    ::g::Fuse::Drawing::StaticSolidColor* temp14 = ::g::Fuse::Drawing::StaticSolidColor::New1(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    self->Width(300.0f);
    self->Height(80.0f);
    self->Margin(::g::Uno::Float4__New2(0.0f, 0.0f, 0.0f, 10.0f));
    self->Opacity(0.7f);
    temp3->RowCount(1);
    temp3->ColumnCount(1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[6/*Fuse.Node*/])), temp4);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[6/*Fuse.Node*/])), temp11);
    temp4->Width(180.0f);
    temp4->Margin(::g::Uno::Float4__New2(50.0f, 5.0f, -5.0f, 7.0f));
    ::g::Fuse::Controls::Grid::SetRow(temp4, 0);
    ::g::Fuse::Controls::Grid::SetColumn(temp4, 0);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp4->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[6/*Fuse.Node*/])), temp5);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp4->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[6/*Fuse.Node*/])), temp7);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp4->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[6/*Fuse.Node*/])), temp9);
    temp5->AllowedScrollDirections(3);
    temp5->Content1(temp);
    temp->FontSize(20.0f);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Behaviors()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[8/*Fuse.Behavior*/])), temp6);
    temp7->AllowedScrollDirections(3);
    temp7->Content1(temp1);
    temp1->FontSize(18.0f);
    temp1->TextColor(::g::Uno::Float4__New2(0.4666667f, 0.4666667f, 0.4666667f, 1.0f));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Behaviors()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[8/*Fuse.Behavior*/])), temp8);
    temp9->AllowedScrollDirections(3);
    temp9->Content1(temp2);
    temp2->FontSize(18.0f);
    temp2->TextColor(::g::Uno::Float4__New2(0.0f, 0.0f, 0.0f, 1.0f));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Behaviors()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[8/*Fuse.Behavior*/])), temp10);
    temp11->Width(10.0f);
    ::g::Fuse::Controls::Grid::SetRow(temp11, 0);
    ::g::Fuse::Controls::Grid::SetColumn(temp11, 1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp11->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[6/*Fuse.Node*/])), temp12);
    temp12->Width(50.0f);
    temp12->Height(50.0f);
    temp12->Alignment(10);
    temp12->Margin(::g::Uno::Float4__New2(30.0f, 15.0f, 0.0f, 0.0f));
    ::g::Fuse::Gestures::Clicked::AddHandler(temp12, uDelegate::New(::TYPES[9/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, temp_eb21));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp12->Fills()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[11/*Fuse.Drawing.Brush*/])), temp13);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp12->Behaviors()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[8/*Fuse.Behavior*/])), temp_eb21);
    temp13->File(::g::Uno::UX::BundleFileSource::New1(::g::CallHero_bundle::Phone_icon86eee396()));
    self->Fill(temp14);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(self->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[6/*Fuse.Node*/])), temp3);
    return self;
}

// public Factory1 New(MainView parent) [static] :79
MainView__Factory1* MainView__Factory1::New2(::g::MainView* parent)
{
    MainView__Factory1* obj1 = (MainView__Factory1*)uNew(MainView__Factory1_typeof());
    obj1->ctor_(parent);
    return obj1;
}
// }

} // ::g
