// This file was generated based on 'C:\Users\Kevin\Desktop\callhero\CallHero\.cache\GeneratedCode\MainView.g.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.AppName.h>
#include <_root.CallHero_bundle.h>
#include <_root.MainView.Factory.h>
#include <_root.MainView.Factory1.h>
#include <_root.MainView.Fuse_Controls_PageControl_Fuse_Node_Active_Property.h>
#include <_root.MainView.Fuse_Controls_TextControl_string_Value_Property.h>
#include <_root.MainView.Fuse_Reactive_Each_object_Items_Property.h>
#include <_root.MainView.h>
#include <Fuse.AppBase.h>
#include <Fuse.BasicTheme.BasicTheme.h>
#include <Fuse.Behavior.h>
#include <Fuse.Controls.Circle.h>
#include <Fuse.Controls.Grid.h>
#include <Fuse.Controls.Image.h>
#include <Fuse.Controls.Page.h>
#include <Fuse.Controls.PageControl.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.Controls.Rectangle.h>
#include <Fuse.Controls.ScrollView.h>
#include <Fuse.Controls.Shape.h>
#include <Fuse.Controls.StackPanel.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Drawing.Brush.h>
#include <Fuse.Drawing.DynamicBrush.h>
#include <Fuse.Drawing.ImageFill.h>
#include <Fuse.Drawing.StaticSolidColor.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Font.h>
#include <Fuse.Gestures.Clicked.h>
#include <Fuse.Gestures.ClickedHandler.h>
#include <Fuse.Layer.h>
#include <Fuse.Node.h>
#include <Fuse.Reactive.DataBinding-1.h>
#include <Fuse.Reactive.Each.h>
#include <Fuse.Reactive.EventBinding.h>
#include <Fuse.Reactive.JavaScript.h>
#include <Fuse.Theme.h>
#include <Uno.BundleFile.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Float.h>
#include <Uno.Float4.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.BundleFileSource.h>
#include <Uno.UX.FileSource.h>
#include <Uno.UX.IFactory.h>
#include <Uno.UX.Property-1.h>
#include <Uno.UX.Resource.h>
static uString* STRINGS[15];
static uType* TYPES[27];

namespace g{

// public partial sealed class MainView :16
// {
// static MainView() :167
static void MainView__cctor__fn(uType* __type)
{
    MainView::seasrn_ = ::g::Fuse::Font::New1(::g::Uno::UX::BundleFileSource::New1(::g::CallHero_bundle::SEASRN__9a1d02eb()));
    ::g::Uno::UX::Resource::SetGlobalKey(MainView::seasrn_, ::STRINGS[0/*"seasrn"*/]);
}

::g::Fuse::AppBase_type* MainView_typeof()
{
    static uSStrong< ::g::Fuse::AppBase_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.FieldCount = 25;
    options.ObjectSize = sizeof(MainView);
    options.TypeSize = sizeof(::g::Fuse::AppBase_type);
    type = (::g::Fuse::AppBase_type*)uClassType::New("MainView", options);
    type->SetBase(::g::Fuse::App_typeof());
    type->fp_ctor_ = (void*)MainView__New1_fn;
    type->fp_cctor_ = MainView__cctor__fn;
    ::STRINGS[0] = uString::Const("seasrn");
    ::STRINGS[1] = uString::Const("categories");
    ::STRINGS[2] = uString::Const("backButton");
    ::STRINGS[3] = uString::Const("selected");
    ::STRINGS[4] = uString::Const("contacts");
    ::STRINGS[5] = uString::Const("currentPage");
    ::STRINGS[6] = uString::Const("MainView.ux");
    ::STRINGS[7] = uString::Const("Page1");
    ::STRINGS[8] = uString::Const("1*,0.5*,2.5*");
    ::STRINGS[9] = uString::Const("loggedOutView");
    ::STRINGS[10] = uString::Const("Call");
    ::STRINGS[11] = uString::Const("Hero");
    ::STRINGS[12] = uString::Const("Available Categories");
    ::STRINGS[13] = uString::Const("Page2");
    ::STRINGS[14] = uString::Const("0.4*,2.5*");
    ::TYPES[0] = ::g::Uno::UX::FileSource_typeof();
    ::TYPES[1] = ::g::CallHero_bundle_typeof();
    ::TYPES[2] = uObject_typeof();
    ::TYPES[3] = ::g::Fuse::Controls::TextControl_typeof();
    ::TYPES[4] = ::g::Fuse::Reactive::DataBinding_typeof()->MakeType(uObject_typeof());
    ::TYPES[5] = ::g::Uno::UX::Property_typeof()->MakeType(uObject_typeof());
    ::TYPES[6] = ::g::Fuse::Reactive::DataBinding_typeof()->MakeType(::g::Uno::String_typeof());
    ::TYPES[7] = ::g::Uno::UX::Property_typeof()->MakeType(::g::Uno::String_typeof());
    ::TYPES[8] = ::g::Fuse::Reactive::DataBinding_typeof()->MakeType(::g::Fuse::Node_typeof());
    ::TYPES[9] = ::g::Uno::UX::Property_typeof()->MakeType(::g::Fuse::Node_typeof());
    ::TYPES[10] = ::g::Fuse::Reactive::JavaScript_typeof();
    ::TYPES[11] = ::g::Fuse::Controls::Panel_typeof();
    ::TYPES[12] = ::g::Fuse::Node_typeof();
    ::TYPES[13] = ::g::Fuse::Behavior_typeof();
    ::TYPES[14] = ::g::Fuse::Controls::Grid_typeof();
    ::TYPES[15] = ::g::Fuse::Controls::Shape_typeof();
    ::TYPES[16] = ::g::Fuse::Drawing::Brush_typeof();
    ::TYPES[17] = ::g::Fuse::Drawing::ImageFill_typeof();
    ::TYPES[18] = ::g::Fuse::Drawing::DynamicBrush_typeof();
    ::TYPES[19] = ::g::Fuse::Elements::Element_typeof();
    ::TYPES[20] = ::g::Fuse::Controls::ScrollView_typeof();
    ::TYPES[21] = ::g::Fuse::Reactive::Each_typeof();
    ::TYPES[22] = ::g::Uno::UX::IFactory_typeof();
    ::TYPES[23] = ::g::Fuse::Gestures::ClickedHandler_typeof();
    ::TYPES[24] = ::g::Fuse::Controls::Image_typeof();
    ::TYPES[25] = ::g::Fuse::AppBase_typeof();
    ::TYPES[26] = ::g::Fuse::BasicTheme::BasicTheme_typeof();
    type->SetFields(16,
        ::g::Fuse::Controls::Grid_typeof(), offsetof(::g::MainView, loggedOutView), 0,
        ::g::Fuse::Controls::Page_typeof(), offsetof(::g::MainView, Page1), 0,
        ::g::Fuse::Controls::Page_typeof(), offsetof(::g::MainView, Page2), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(::g::MainView, temp_eb1), 0,
        MainView__Fuse_Reactive_Each_object_Items_Property_typeof(), offsetof(::g::MainView, temp_Items_inst), 0,
        MainView__Fuse_Controls_TextControl_string_Value_Property_typeof(), offsetof(::g::MainView, temp1_Value_inst), 0,
        MainView__Fuse_Reactive_Each_object_Items_Property_typeof(), offsetof(::g::MainView, temp2_Items_inst), 0,
        MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property_typeof(), offsetof(::g::MainView, temp3_Active_inst), 0,
        ::g::Fuse::Font_typeof(), (uintptr_t)&::g::MainView::seasrn_, uFieldFlagsStatic);
    return type;
}

// public MainView() :172
void MainView__ctor_3_fn(MainView* __this)
{
    __this->ctor_3();
}

// internal void InitializeUX() :176
void MainView__InitializeUX_fn(MainView* __this)
{
    __this->InitializeUX();
}

// public MainView New() :172
void MainView__New1_fn(MainView** __retval)
{
    *__retval = MainView::New1();
}

uSStrong< ::g::Fuse::Font*> MainView::seasrn_;

// public MainView() [instance] :172
void MainView::ctor_3()
{
    ctor_2();
    InitializeUX();
}

// internal void InitializeUX() [instance] :176
void MainView::InitializeUX()
{
    ::g::Fuse::Reactive::Each* temp = ::g::Fuse::Reactive::Each::New1();
    temp_Items_inst = MainView__Fuse_Reactive_Each_object_Items_Property::New1(temp);
    ::g::Fuse::Controls::Text* temp1 = ::g::Fuse::Controls::Text::New2();
    temp1_Value_inst = MainView__Fuse_Controls_TextControl_string_Value_Property::New1(temp1);
    ::g::Fuse::Reactive::Each* temp2 = ::g::Fuse::Reactive::Each::New1();
    temp2_Items_inst = MainView__Fuse_Reactive_Each_object_Items_Property::New1(temp2);
    ::g::Fuse::Controls::PageControl* temp3 = ::g::Fuse::Controls::PageControl::New2();
    temp3_Active_inst = MainView__Fuse_Controls_PageControl_Fuse_Node_Active_Property::New1(temp3);
    ::g::Fuse::Reactive::JavaScript* temp4 = ::g::Fuse::Reactive::JavaScript::New1();
    Page1 = ::g::Fuse::Controls::Page::New2();
    loggedOutView = ::g::Fuse::Controls::Grid::New2();
    ::g::Fuse::Controls::Rectangle* temp5 = ::g::Fuse::Controls::Rectangle::New2();
    ::g::Fuse::Drawing::ImageFill* temp6 = ::g::Fuse::Drawing::ImageFill::New1();
    ::g::Fuse::Drawing::ImageFill* temp7 = ::g::Fuse::Drawing::ImageFill::New1();
    ::g::Fuse::Controls::Grid* temp8 = ::g::Fuse::Controls::Grid::New2();
    ::g::AppName* temp9 = ::g::AppName::New3();
    ::g::Fuse::Controls::Circle* temp10 = ::g::Fuse::Controls::Circle::New2();
    ::g::Fuse::Drawing::ImageFill* temp11 = ::g::Fuse::Drawing::ImageFill::New1();
    ::g::AppName* temp12 = ::g::AppName::New3();
    ::g::Fuse::Controls::Panel* temp13 = ::g::Fuse::Controls::Panel::New1();
    ::g::Fuse::Controls::Text* temp14 = ::g::Fuse::Controls::Text::New2();
    ::g::Fuse::Controls::Rectangle* temp15 = ::g::Fuse::Controls::Rectangle::New2();
    ::g::Fuse::Drawing::StaticSolidColor* temp16 = ::g::Fuse::Drawing::StaticSolidColor::New1(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    ::g::Fuse::Controls::ScrollView* temp17 = ::g::Fuse::Controls::ScrollView::New2();
    ::g::Fuse::Controls::StackPanel* temp18 = ::g::Fuse::Controls::StackPanel::New2();
    MainView__Factory* temp19 = MainView__Factory::New2(this);
    ::g::Fuse::Reactive::DataBinding* temp20 = (::g::Fuse::Reactive::DataBinding*)::g::Fuse::Reactive::DataBinding::New1(::TYPES[4/*Fuse.Reactive.DataBinding<object>*/], temp_Items_inst, ::STRINGS[1/*"categories"*/]);
    Page2 = ::g::Fuse::Controls::Page::New2();
    ::g::Fuse::Controls::Grid* temp21 = ::g::Fuse::Controls::Grid::New2();
    ::g::Fuse::Controls::Rectangle* temp22 = ::g::Fuse::Controls::Rectangle::New2();
    ::g::Fuse::Drawing::ImageFill* temp23 = ::g::Fuse::Drawing::ImageFill::New1();
    ::g::Fuse::Drawing::ImageFill* temp24 = ::g::Fuse::Drawing::ImageFill::New1();
    ::g::Fuse::Controls::Panel* temp25 = ::g::Fuse::Controls::Panel::New1();
    ::g::Fuse::Controls::Rectangle* temp26 = ::g::Fuse::Controls::Rectangle::New2();
    ::g::Fuse::Drawing::StaticSolidColor* temp27 = ::g::Fuse::Drawing::StaticSolidColor::New1(::g::Uno::Float4__New2(0.0f, 0.0f, 0.0f, 1.0f));
    ::g::Fuse::Controls::Rectangle* temp28 = ::g::Fuse::Controls::Rectangle::New2();
    ::g::Fuse::Controls::Image* temp29 = ::g::Fuse::Controls::Image::New2();
    temp_eb1 = ::g::Fuse::Reactive::EventBinding::New1(::STRINGS[2/*"backButton"*/]);
    ::g::Fuse::Reactive::DataBinding* temp30 = (::g::Fuse::Reactive::DataBinding*)::g::Fuse::Reactive::DataBinding::New1(::TYPES[6/*Fuse.Reactive.DataBinding<string>*/], temp1_Value_inst, ::STRINGS[3/*"selected"*/]);
    ::g::Fuse::Controls::ScrollView* temp31 = ::g::Fuse::Controls::ScrollView::New2();
    ::g::Fuse::Controls::StackPanel* temp32 = ::g::Fuse::Controls::StackPanel::New2();
    MainView__Factory1* temp33 = MainView__Factory1::New2(this);
    ::g::Fuse::Reactive::DataBinding* temp34 = (::g::Fuse::Reactive::DataBinding*)::g::Fuse::Reactive::DataBinding::New1(::TYPES[4/*Fuse.Reactive.DataBinding<object>*/], temp2_Items_inst, ::STRINGS[4/*"contacts"*/]);
    ::g::Fuse::Reactive::DataBinding* temp35 = (::g::Fuse::Reactive::DataBinding*)::g::Fuse::Reactive::DataBinding::New1(::TYPES[8/*Fuse.Reactive.DataBinding<Fuse.Node>*/], temp3_Active_inst, ::STRINGS[5/*"currentPage"*/]);
    temp4->LineNumber(4);
    temp4->FileName(::STRINGS[6/*"MainView.ux"*/]);
    temp4->File(::g::Uno::UX::BundleFileSource::New1(::g::CallHero_bundle::MainView44d91759()));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), Page1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), Page2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Behaviors()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[13/*Fuse.Behavior*/])), temp35);
    uPtr(Page1)->Name(::STRINGS[7/*"Page1"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(Page1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), loggedOutView);
    uPtr(loggedOutView)->RowData(::STRINGS[8/*"1*,0.5*,2.5*"*/]);
    uPtr(loggedOutView)->Name(::STRINGS[9/*"loggedOutView"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loggedOutView)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp5);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loggedOutView)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp8);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loggedOutView)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp13);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loggedOutView)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp17);
    temp5->Layer(0);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp5->Fills()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[16/*Fuse.Drawing.Brush*/])), temp6);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp5->Fills()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[16/*Fuse.Drawing.Brush*/])), temp7);
    temp6->File(::g::Uno::UX::BundleFileSource::New1(::g::CallHero_bundle::phone119a696f()));
    temp7->Opacity(0.2f);
    temp7->File(::g::Uno::UX::BundleFileSource::New1(::g::CallHero_bundle::overlaya0e0bdbe()));
    temp8->RowCount(1);
    temp8->ColumnCount(3);
    temp8->Margin(::g::Uno::Float4__New2(0.0f, 20.0f, 0.0f, 0.0f));
    temp8->Padding(::g::Uno::Float4__New2(10.0f, 0.0f, 10.0f, 0.0f));
    ::g::Fuse::Controls::Grid::SetRow(temp8, 0);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp8->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp9);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp8->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp10);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp8->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp12);
    temp9->Value(::STRINGS[10/*"Call"*/]);
    temp9->TextColor(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    temp9->Margin(::g::Uno::Float4__New2(15.0f, 45.0f, 0.0f, 0.0f));
    ::g::Fuse::Controls::Grid::SetColumn(temp9, 0);
    temp10->Width(75.0f);
    temp10->Height(75.0f);
    ::g::Fuse::Controls::Grid::SetColumn(temp10, 1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp10->Fills()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[16/*Fuse.Drawing.Brush*/])), temp11);
    temp11->File(::g::Uno::UX::BundleFileSource::New1(::g::CallHero_bundle::CH_Icon96ea8a5e()));
    temp12->Value(::STRINGS[11/*"Hero"*/]);
    temp12->TextColor(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    temp12->Margin(::g::Uno::Float4__New2(5.0f, 45.0f, 0.0f, 0.0f));
    ::g::Fuse::Controls::Grid::SetColumn(temp12, 2);
    temp13->Margin(::g::Uno::Float4__New2(0.0f, 0.0f, 0.0f, 15.0f));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp13->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp14);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp13->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp15);
    temp14->Value(::STRINGS[12/*"Available C...*/]);
    temp14->FontSize(30.0f);
    temp14->Alignment(10);
    ::g::Fuse::Controls::Grid::SetRow(temp14, 1);
    temp15->Opacity(0.5f);
    temp15->Layer(0);
    temp15->Fill(temp16);
    temp17->Margin(::g::Uno::Float4__New2(0.0f, 0.0f, 0.0f, 10.0f));
    ::g::Fuse::Controls::Grid::SetRow(temp17, 2);
    temp17->Content1(temp18);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp18->Behaviors()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[13/*Fuse.Behavior*/])), temp20);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp18->Behaviors()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[13/*Fuse.Behavior*/])), temp);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Factories()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[22/*Uno.UX.IFactory*/])), (uObject*)temp19);
    uPtr(Page2)->Name(::STRINGS[13/*"Page2"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(Page2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp21);
    temp21->RowData(::STRINGS[14/*"0.4*,2.5*"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp21->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp22);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp21->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp25);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp21->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp31);
    temp22->Layer(0);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp22->Fills()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[16/*Fuse.Drawing.Brush*/])), temp23);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp22->Fills()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[16/*Fuse.Drawing.Brush*/])), temp24);
    temp23->File(::g::Uno::UX::BundleFileSource::New1(::g::CallHero_bundle::backgroundb072867a()));
    temp24->Opacity(0.4f);
    temp24->File(::g::Uno::UX::BundleFileSource::New1(::g::CallHero_bundle::overlaya0e0bdbe()));
    ::g::Fuse::Controls::Grid::SetRow(temp25, 0);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp25->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp26);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp25->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp28);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp25->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp1);
    temp26->Opacity(0.5f);
    temp26->Layer(0);
    temp26->Fill(temp27);
    temp28->Width(25.0f);
    temp28->Height(25.0f);
    temp28->Alignment(1);
    temp28->Margin(::g::Uno::Float4__New2(25.0f, 20.0f, 0.0f, 0.0f));
    ::g::Fuse::Gestures::Clicked::AddHandler(temp28, uDelegate::New(::TYPES[23/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb1)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp28->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[12/*Fuse.Node*/])), temp29);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp28->Behaviors()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[13/*Fuse.Behavior*/])), temp_eb1);
    temp29->File(::g::Uno::UX::BundleFileSource::New1(::g::CallHero_bundle::arrowd98763bb()));
    temp1->FontSize(24.0f);
    temp1->TextColor(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    temp1->Alignment(10);
    temp1->Margin(::g::Uno::Float4__New2(0.0f, 20.0f, 0.0f, 0.0f));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Behaviors()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[13/*Fuse.Behavior*/])), temp30);
    temp31->Margin(::g::Uno::Float4__New2(0.0f, 10.0f, 0.0f, 10.0f));
    ::g::Fuse::Controls::Grid::SetRow(temp31, 1);
    temp31->Content1(temp32);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp32->Behaviors()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[13/*Fuse.Behavior*/])), temp34);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp32->Behaviors()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[13/*Fuse.Behavior*/])), temp2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Factories()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[22/*Uno.UX.IFactory*/])), (uObject*)temp33);
    RootNode(temp3);
    Theme(::g::Fuse::BasicTheme::BasicTheme::Singleton1());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Behaviors()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::TYPES[13/*Fuse.Behavior*/])), temp4);
}

// public MainView New() [static] :172
MainView* MainView::New1()
{
    MainView* obj1 = (MainView*)uNew(MainView_typeof());
    obj1->ctor_3();
    return obj1;
}
// }

} // ::g
